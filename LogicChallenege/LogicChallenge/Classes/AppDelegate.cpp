#include "AppDelegate.h"
#include "InicioScene.h"
#include "SimpleAudioEngine.h"
#ifdef SDKBOX_ENABLED
#include "PluginGoogleAnalytics/PluginGoogleAnalytics.h"
#include "PluginIAP/PluginIAP.h"
#include "PluginReview/PluginReview.h"
#endif
#include "layers/LayerShop.h"
#include "tutorial/TutorialScene.h"
#include "localnotifications/LocalNotification.h"
#include "helpers/LogicSQLHelper.h"
#include "Levels.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace CocosDenshion;


AppDelegate::AppDelegate()
{
}

AppDelegate::~AppDelegate() 
{
}

// if you want a different context, modify the value of glContextAttrs
// it will affect all platforms
void AppDelegate::initGLContextAttrs()
{
    // set OpenGL context attributes: red,green,blue,alpha,depth,stencil
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};

    GLView::setGLContextAttrs(glContextAttrs);
}

// if you want to use the package manager to install more packages,  
// don't modify or remove this function
static int register_all_packages()
{
    return 0; //flag for packages manager
}

bool AppDelegate::applicationDidFinishLaunching() {
#ifdef SDKBOX_ENABLED
    sdkbox::PluginGoogleAnalytics::init();
    sdkbox::IAP::init();
    sdkbox::PluginReview::init();
#endif
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32) || (CC_TARGET_PLATFORM == CC_PLATFORM_MAC) || (CC_TARGET_PLATFORM == CC_PLATFORM_LINUX)
        glview = GLViewImpl::createWithRect("LogicChallenge", cocos2d::Rect(0, 0, 960, 640));
#else
        glview = GLViewImpl::create("LogicChallenge");
#endif
        director->setOpenGLView(glview);
    }

    // turn on display FPS
    director->setDisplayStats(false);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0f / 60);

    // Set the design resolution
    glview->setDesignResolutionSize(960, 640, ResolutionPolicy::FIXED_HEIGHT);
    auto frameSize = glview->getFrameSize();
    // if the frame's height is larger than the height of medium size.
    
    
    iniciarVariables();
    
    register_all_packages();

    // create a scene. it's an autorelease object
    Scene *pScene;
    if(!LogicSQLHelper::getInstance().getTutorialSuperado() ){
        pScene= TutorialScene::scene();
    }else{
        pScene= Inicio::createScene();
        
    }


    // run
    director->runWithScene(pScene);

    return true;
}

// This function will be called when the app is inactive. Note, when receiving a phone call it is invoked.
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();
    LayerShop::releaseLayer();
    // if you use SimpleAudioEngine, it must be paused
    SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
    cargarNotificaciones();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
     SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
    
    
    if(LogicSQLHelper::getInstance().getRegaloVoto() && !LogicSQLHelper::getInstance().getRegaloGastado()){
        LogicSQLHelper::getInstance().setRegaloGastado(true);
        int numeroRegalo=LogicSQLHelper::getInstance().getRegaloNumero();
        LogicSQLHelper::getInstance().addElixiresUsuario(numeroRegalo);
        LogicSQLHelper::getInstance().setRegaloVoto(false, -1);
        MessageBox(CCLocalizedString("RATE_PREMIO_CONGRATS_CONTENT", "RATE_PREMIO_CONGRATS_CONTENT"), CCLocalizedString("RATE_PREMIO_CONGRATS_TITLE", "RATE_PREMIO_CONGRATS_TITLE"));
        try{
            
            Director *pDirector = Director::getInstance();
            pDirector->replaceScene(TransitionCrossFade::create(0.5f, Levels::createScene()));
        }catch(...){}
        //vuelve de votar
    }
    LocalNotification::cancel(LocalNotification::NOTIFICATION_GIFT);
    LocalNotification::cancel(LocalNotification::NOTIFICATION_REMEMBER);
    
}



void AppDelegate::showGifNotification(){
    if( LogicSQLHelper::getInstance().getRegaloNotifGastado()){
        return;
    }
    MessageBox (LanguageManager::getInstance()->getStringForKey("NOTIFICATION_GIFT_OWNED").c_str(),LanguageManager::getInstance()->getStringForKey("NOTIFICATION_GIFT_OWNED_TITLE").c_str());
    LogicSQLHelper::getInstance().setRegaloNotif(true);
    LogicSQLHelper::getInstance().addHintsUsuario(1);
    
}

void AppDelegate::cargarNotificaciones(){
    if(!LogicSQLHelper::getInstance().getRegaloNotifGastado()){
        LocalNotification::show(LanguageManager::getInstance()->getStringForKey("LOCAL_NOTIFICATION_GIFT"), 3600*24*10, LocalNotification::NOTIFICATION_GIFT);
     }
    
    LocalNotification::show(LanguageManager::getInstance()->getStringForKey("LOCAL_NOTIFICATION_REMMEBER"), 3600*24*6, LocalNotification::NOTIFICATION_REMEMBER);
    
    
    
}


void AppDelegate::iniciarVariables()
{
    
    
    bool iniciado=UserDefault::getInstance()->getBoolForKey("INICIADO", false);
    if(!iniciado){
        
        
        UserDefault::getInstance()->setBoolForKey(LayerShop::COMPRA_REMOVE_ADS.c_str(), false);
        UserDefault::getInstance()->setBoolForKey(LayerShop::COMPRA_COMPLETE_GAME.c_str(), false);
        LogicSQLHelper::getInstance().addElixiresUsuario(0);
        UserDefault::getInstance()->setBoolForKey("INICIADO", true);
        UserDefault::getInstance()->flush();
    }
    
    bool migration130=UserDefault::getInstance()->getBoolForKey("VERSION_1.30", false);
    if(!migration130){
        bool updateado=LogicSQLHelper::getInstance().updateVersions();
        UserDefault::getInstance()->setBoolForKey("VERSION_1.30",updateado );
       UserDefault::getInstance()->flush();
        
        
    }
    
    bool migration131=UserDefault::getInstance()->getBoolForKey("VERSION_1.31", false);
    if(!migration131){
        bool updateado=LogicSQLHelper::getInstance().updateVersions131();
        UserDefault::getInstance()->setBoolForKey("VERSION_1.31",updateado );
        UserDefault::getInstance()->flush();
        
        
    }
    
    
    
    
    
    
    
}
