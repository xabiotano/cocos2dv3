#ifndef __LevelsBase_SCENE_H__
#define __LevelsBase_SCENE_H__

#include "cocos2d.h"
#include "cocos-ext.h"
#include "layers/LayerExplicarItems.h"
#include "PluginIAP/PluginIAP.h"
#include "layers/ILayerShop.h"
USING_NS_CC;
USING_NS_CC_EXT;

class LevelsBase : public LayerColor, public ILayerShop
{
public:
    virtual ~LevelsBase(void);
    

    bool initWithColor(const Color4B& color);
  
    void customInit();

    
    void onExit() override;
    
   

    
    
    //ILayerShop
    void onElixirComprado(int numero) override;
    virtual void onHintsComprados(int numero) override {};
    void onCompleteGameComprado() override;
    void onRemoveAdsComprado() override;

    virtual void Back(Ref* sender);
    virtual void Shop(Ref* sender);
   
protected :
    void  onEnterTransitionDidFinish() override;
    virtual void compradoItem(sdkbox::Product  const& p){};
    template <typename T> std::string tostr(const T& t);
    Label* mLabelNumeroItems;
private:
    void recargarShop();
    Boton* mFondoItems;
    Size mVisibleSize;
    
};

#endif // __LevelsBase_SCENE_H__
