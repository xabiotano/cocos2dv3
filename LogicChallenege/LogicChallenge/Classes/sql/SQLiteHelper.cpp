//
//  SQLiteHelper.cpp
//  logicmaster2
//
//  Created by Xabier on 14/11/13.
//
//

#include "SQLiteHelper.h"
#include "fstream"


sqlite3 *SQLiteHelper::_db;
int SQLiteHelper::rc;
char* SQLiteHelper::zErrMsg;


void SQLiteHelper::openDB(){
    if(SQLiteHelper::_db!=NULL){
        return;
    }
    SQLiteHelper::initDB();
    CCLOG("openDB");
    ssize_t filesize = 0;
    unsigned char* fileData = NULL;
    std::string content, fullPath;
    
    fullPath=FileUtils::getInstance()->getWritablePath();
    fullPath += "mundiales.pngs.sqlite";
    
    
   fileData = FileUtils::getInstance()->getFileData(fullPath.c_str(), "rw", &filesize);
   // fileData= FileUtils::getInstance()->getFileData(fullPath.c_str()).;
    
    CCLOG("openDB %s",fullPath.c_str());
    content.append((char*)fileData);
    CCLOG("CONTENIDO %s,PATH %s,filesize:%zu",content.c_str(),fullPath.c_str(),filesize);
    
    SQLiteHelper::zErrMsg = 0;
    
    // Open the test.db file
    SQLiteHelper::rc = sqlite3_open(fullPath.c_str(),&SQLiteHelper::_db);
    
    if(SQLiteHelper::rc)
    {
        // failed
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(SQLiteHelper::_db));
    }
    else
    {
        // success
        fprintf(stderr, "Open database successfully\n");
    }

   
}

void SQLiteHelper::closeDB(){
    if(_db!=NULL){
        sqlite3_close(_db);
        sqlite3_db_release_memory(_db);

    }
   }


void SQLiteHelper::initDB(){
    
    if( UserDefault::getInstance()->getBoolForKey("bd_cargada",false)){
        CCLOG("bd_cargada");
        return;
    }
    CCLOG("NO EXISTE bd_cargada");
        ssize_t filesize = 0;
        unsigned char* fileData = NULL;
        std::string content, fullPath;
        fullPath = FileUtils::getInstance()->fullPathForFilename("ui/mundiales.pngs.sqlite");
        fileData = FileUtils::getInstance()->getFileData(fullPath.c_str(), "rwx", &filesize);
        
        //copio la base de datos en cache
        std::string path;
        
        path=CCFileUtils::getInstance()->getWritablePath();
        path += "mundiales.pngs.sqlite";
        CCLOG("PATH %s",path.c_str());
        
        char buffer[500];
        sprintf(buffer,"PATH: %s\n",path.c_str());
        std::fstream outfile(path.c_str(),std::fstream::out);
        outfile.write((const char*)fileData,filesize-1);
        outfile.close();
        UserDefault::getInstance()->setBoolForKey("bd_cargada",true);
        UserDefault::getInstance()->flush();
    
}


