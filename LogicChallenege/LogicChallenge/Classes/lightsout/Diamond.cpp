#include "Diamond.h"
#include "SimpleAudioEngine.h"
#include "LightsOutScene.h"
#include <stdio.h>





Diamond::~Diamond(void){}


Diamond* Diamond::create(Node* parent,int x,int y, bool active)
{
    Diamond *devolver= new Diamond();
    devolver->autorelease();
    devolver->initDiamond(parent,x, y, active);
   
    return devolver;
}


bool Diamond::getLuzActive(){
    return mLuzActive;
}

void Diamond::initDiamond(Node* parent,int x,int y, bool active)
{
    mX=x;
    mY=y;
    mParent=parent;
    mLuzActive=active;
    mInitialLuzActive=mLuzActive;
    if(mLuzActive){
        this->initBoton(this,menu_selector(Diamond::click),"lightsout/light.png");
    }else {
        this->initBoton(this,menu_selector(Diamond::click),"lightsout/light_black.png");
    }
}





void Diamond::click(Ref* sender){
    LightsOut::getInstance()->clickDiamond(mX,mY);
    this->runAction(Sequence::createWithTwoActions(ScaleBy::create(0.1f, 1.1f), ScaleBy::create(0.1f, 0.9f)));    
};



void Diamond::timeout(){
    this->setScale(1.f);
}

void Diamond::establecerEstadoInicial(){
    if(mInitialLuzActive){
        if(!getLuzActive()){
           this->swapLuz();
        }
    }else{
        if(getLuzActive()){
            this->swapLuz();
            
        }
    }
}

void Diamond::reset(){
    if(mInitialLuzActive){
        if(!getLuzActive()){
            CallFunc *finReset = CallFunc::create([this]() { this->establecerEstadoInicial(); });
            this->runAction(Sequence::createWithTwoActions(EaseBounceIn::create(RotateBy::create(1.f, 360)),finReset ));
        }
    }else{
        if(getLuzActive()){
            CallFunc *finReset =CallFunc::create([this]() { this->establecerEstadoInicial(); });
            this->runAction(Sequence::createWithTwoActions(EaseBounceIn::create(RotateBy::create(1.f, 360)),finReset ));
            
        }
    }
}


void Diamond::swapLuz(){
    
    mLuzActive=!mLuzActive;
    if(mLuzActive){
         CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/lightsout/pop.mp3");
        this->initWithTexture(Director::getInstance()->getTextureCache()->addImage("lightsout/light.png"));
    }else {
         CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/lightsout/pop2.mp3");
          this->initWithTexture(Director::getInstance()->getTextureCache()->addImage("lightsout/light_black.png"));
    }
}

int Diamond::getX(){
    return mX;
}
int Diamond::getY(){
    return mY;
}


