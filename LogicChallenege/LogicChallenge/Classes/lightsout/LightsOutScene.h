#ifndef __LightsOut_SCENE_H__
#define __LightsOut_SCENE_H__

#include "cocos2d.h"
#include "../BaseScene.h"
#include "../widgets/Boton.h"
#include "Diamond.fwd.h"
#include <stack>

USING_NS_CC;


class LightsOut : public BaseScene
{
public:
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init() override;
     virtual ~LightsOut(void);
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static Scene* scene();
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(LightsOut);
    static LightsOut* getInstance();
    
     void checkFinish(Ref* sender) override;
     void Hint(Ref* sender) override;
     void HintCallback(bool accepted) override;
     void HintStart(Ref* sender) override;
     void HintStop(Ref* sender) override;
     void Retry(Ref* sender) override;
    
    void clickDiamond(int x,int y);
    Dictionary* mLuces;
    std::string getStringFromInt(int uno,int dos);
    bool StringToBool(std::string cadena);
    virtual void TutorialStart(){};
    void onEnterTransitionDidFinish() override;
    
private:
    CCArray* mTableros;
    Dictionary* mSoluciones;
    int mNumeroTablero;
    int NUMERO_CASILLAS;
    void resetNivel();
    std::stack<Diamond*> mSerieSolucion;
    void SugerirMove();
    bool mSituacionHint;
    Diamond* mDiamanteParpadeando;
    float mScaleBase;
    Label* mLabelCounter;
    int mNumeroMovimientos;
    void updateCounter();
    void HintStartTemp(float dt);
    
    
};


#endif // __LightsOut_SCENE_H__
