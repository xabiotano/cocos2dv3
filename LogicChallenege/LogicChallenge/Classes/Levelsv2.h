#ifndef __Levelsv2_SCENE_H__
#define __Levelsv2_SCENE_H__

#include "cocos2d.h"
#include "cocos-ext.h"
#include "BaseScene.h"
#include "TipoNivelTypes.h"
#include "LevelsBase.h"
#include "layers/LayerBlock.h"
#include "widgets/LanguageManager.h"

USING_NS_CC;
USING_NS_CC_EXT;

class Levelsv2 : public LevelsBase
{
public:
     virtual ~Levelsv2(void);
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init();

    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static cocos2d::Scene* scene();
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(Levelsv2);
    static int cargarNiveltexto(void *NotUsed, int argc, char **argv, char **azColName);
    static int cargarNumeroNiveles(void *NotUsed, int argc, char **argv, char **azColName);

    
    
    
    

    
    
  //  static Levelsv2* GetInstance();
   
    
    SpriteBatchNode* item_madera_tex_bacth;
    SpriteBatchNode* item_elixir_cerebro_tex_bacth;
    SpriteBatchNode* item_elixir_fresa_tex_bacth;
    SpriteBatchNode* item_elixir_energia_tex_bacth;
    SpriteBatchNode* item_elixir_locked_tex_bacth;
    SpriteBatchNode* item_elixir_item_wood_tex_bacth;
    SpriteBatchNode* item_elixir_item_wood_hole_tex_bacth;
    SpriteBatchNode* item_elixir_item_tex_bacth;
    
    
private:
    
    
    Label* mNumeroElixiresParaSiguienteNivel;
    Size mVisibleSize;
    int mNumeroItem;
    void pintarNiveles();
    void  crearScroll();
    LayerColor* mFondo;
    void onClickNivel(Ref* sender);
    ScrollView* mSlidingLayer;
    void creaItemNivel(int idnivel,int tiponivel,bool superado,bool locked,int numeroestrellas);
    int tope;
    int minimo;
    float mPaddingY;
    
   // void dibujarCirculo(int tag);
    int RandomInt2(int min, int max);
    

    void pintarContenido();
    Label* labelLoading;
    void onKeyBackClicked();
    int mFila;
    int mColumna;
    int mHeightFila;
    bool mEsFilaJugable;
    int mNumeroItems;
    int mNumeroElixiresUsuario;
    int mElixiresParaSiguienteNivel;
    Boton* mMadero;
    Sprite* mPanel_izquierdo;
    Sprite* fondo ;
    
    /*Texture2D * item_madera_tex;
    Texture2D * item_elixir_cerebro_tex;
    Texture2D * item_elixir_fresa_tex;
    Texture2D * item_elixir_energia_tex;
    Texture2D * item_elixir_locked_tex;
    Texture2D * item_elixir_item_wood_tex;
    Texture2D * item_elixir_item_wood_hole_tex;
    Texture2D * item_elixir_item_tex;*/
    
    void pintarElixir(LevelButton* item,int idnivel,int tipo,bool superado,bool locked,int numeroestrellas);
    void pintarElixiresGanados(int numeroestrellas);
    void animarContadoresElixirRecienSuperado(int numEstrellas,int idnivel);
    
    void setNumeroNiveles(int numero);
    void contarNiveles();
    
    /*metodos que actualizan la BD de niveles*/
    
    Dictionary* mCol1,*mCol2,*mCol3,*mNivelesJugables;
    void cargarAnimacionNivelSuperado(float dt);
    int mPasoActualizacionBotella;
    void sumarElixirContadores();
    void mostrarDesbloqueoNivel();
    Sprite* mDestinoUnlock;
    void hacerScrollUnlock();
    void ponerEnSuSitioOffset(float dt);
  
    bool superadoAnterior;
    float mOffsetYInicial;
    void nothing(Ref* sender);
    void playSonidoUnlock();
   
  
    void onExit();
 
    
protected:
    void compradoItem(const sdkbox::Product &p);
    
};

#endif // __Levelsv2_SCENE_H__
