#include "FiveFrogs.h"
#include "SimpleAudioEngine.h"

using namespace cocos2d;
using namespace CocosDenshion;

FiveFrogs* instancia_frogs;

FiveFrogs::~FiveFrogs(void){
    CC_SAFE_RELEASE(mArrayOfFrogs);
}


FiveFrogs* FiveFrogs::getInstance(){
    return instancia_frogs;
}






Scene* FiveFrogs::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    
    // 'layer' is an autorelease object
    FiveFrogs *layer = FiveFrogs::create();

    // add layer as a child to scene
    scene->addChild(layer);

    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool FiveFrogs::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !BaseScene::init() )
    {
        return false;
    }
    instancia_frogs=this;
    mNombrePantalla="FiveFrogs";
    mTipoNivel=kNivelLogica;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    Sprite* mFondo = Sprite::create("frogs/frogs_fondo.png");
    // position the sprite on the center of the screen
    mFondo->setPosition(Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    
    this->addChild(mFondo, 0);
    float scalex=Director::getInstance()->getVisibleSize().width/mFondo->getContentSize().width;
    float scaley=Director::getInstance()->getVisibleSize().height/mFondo->getContentSize().height;
    if(scalex>scaley){
        scaley=scalex;
    }
    mFondo->setScale(scaley);
    Director::getInstance()->setContentScaleFactor(960/visibleSize.width);
    mArrayOfFrogs= Dictionary::create();
    mArrayOfFrogs->retain();
    someFrogMoving=false;
    colocarRanas();
    mTextoSuccess=LanguageManager::getInstance()->getStringForKey("SUCCESS_MESSAGE", "SUCCESS_MESSAGE");
    
    mTextoHelp=std::string(LanguageManager::getInstance()->getStringForKey("HELP_FIVE_FROGS","HELP_FIVE_FROGS"));
    mRetryVisibleDuringGame=true;
   
    return true;
}



void FiveFrogs::colocarRanas()
{
    
    if(mArrayOfFrogs->count()>0){
        mArrayOfFrogs->removeAllObjects();
    }
    if(rana1!=NULL){
        rana1->removeFromParent();
    }
    if(rana2!=NULL){
        rana2->removeFromParent();
    }
    if(rana3!=NULL){
        rana3->removeFromParent();
    }
    if(rana4!=NULL){
        rana4->removeFromParent();
    }
    if(rana5!=NULL){
        rana5->removeFromParent();
    }
    if(rana6!=NULL){
        rana6->removeFromParent();
    }
    if(rana7!=NULL){
        rana7->removeFromParent();
    }
    if(rana8!=NULL){
        rana8->removeFromParent();
    }
    
    rana5=Frog::create(this, "frogs/frog_brown.png", false, 5);
    this->addChild(rana5);
    rana6=Frog::create(this, "frogs/frog_brown.png", false, 6);
    this->addChild(rana6);
    rana7=Frog::create(this, "frogs/frog_brown.png", false, 7);
    this->addChild(rana7);
    rana8=Frog::create(this, "frogs/frog_brown.png", false, 8);
    this->addChild(rana8);
    
    
    rana1=Frog::create(this, "frogs/frog_green.png", true, 0);
    this->addChild(rana1);
    rana2=Frog::create(this, "frogs/frog_green.png", true, 1);
    this->addChild(rana2);
    rana3=Frog::create(this, "frogs/frog_green.png", true, 2);
    this->addChild(rana3);
    rana4=Frog::create(this, "frogs/frog_green.png", true, 3);
    this->addChild(rana4);
    
    mArrayOfFrogs->setObject(rana1, 0);
    mArrayOfFrogs->setObject(rana2, 1);
    mArrayOfFrogs->setObject(rana3, 2);
    mArrayOfFrogs->setObject(rana4, 3);
   
    mArrayOfFrogs->setObject(rana5, 5);
    mArrayOfFrogs->setObject(rana6, 6);
    mArrayOfFrogs->setObject(rana7, 7);
    mArrayOfFrogs->setObject(rana8, 8);
  
    

}


void FiveFrogs::checkFinish(){
    if(mArrayOfFrogs->objectForKey(0)!=NULL &&
       mArrayOfFrogs->objectForKey(1)!=NULL &&
       mArrayOfFrogs->objectForKey(2)!=NULL &&
       mArrayOfFrogs->objectForKey(3)!=NULL &&
       mArrayOfFrogs->objectForKey(4)==NULL &&
       mArrayOfFrogs->objectForKey(5)!=NULL &&
       mArrayOfFrogs->objectForKey(6)!=NULL &&
       mArrayOfFrogs->objectForKey(7)!=NULL &&
       mArrayOfFrogs->objectForKey(8)!=NULL )
    {
        
        for(int i=0;i<=3;i++){
            CCLOG("i");
            Frog* frog=(Frog*)mArrayOfFrogs->objectForKey(i);
            if(frog->esverde()){
                mSuccess=false;
                break;
            }else{
               
                mSuccess=true;
            }
            
        }
        if(mSuccess){
            for(int i=5;i<=8;i++){
                 Frog* frog=(Frog*)mArrayOfFrogs->objectForKey(i);
                if(!frog->esverde()){
                    mSuccess=false;
                    break;
                }else{
                    mSuccess=true;
                }
            }
            
        }
        BaseScene::checkFinish(NULL);
    }

}



void FiveFrogs::Retry(Ref* sender){
    Scene* scene=FiveFrogs::scene();
    Director *pDirector = Director::getInstance();
     pDirector->replaceScene(TransitionCrossFade::create(0.5f, scene));
    
}
void FiveFrogs::HintStartTemp(float dt)
{
    HintStart(NULL);
}

void FiveFrogs::HintStart(Ref* sender){
      // 5 4 3 5 6 7 4 3 2 1 5 6 7 8 4 3 2 1 6 7 8 2 1 8
    
    switch(mHintStep){
        case 1:
            rana5->moveToStone(NULL);
            break;
        case 2:
             rana4->moveToStone(NULL);
            break;
        case 3:
            rana3->moveToStone(NULL);
            break;
        case 4:
            rana5->moveToStone(NULL);
            break;
        case 5:
            rana6->moveToStone(NULL);
            break;
        case 6:
            rana7->moveToStone(NULL);
            break;
        case 7:
            rana4->moveToStone(NULL);
            break;
        case 8:
            rana3->moveToStone(NULL);
            break;
        case 9:
            rana2->moveToStone(NULL);
            break;
        case 10:
            rana1->moveToStone(NULL);
            break;
        case 11:
            rana5->moveToStone(NULL);
            break;
        case 12:
            rana6->moveToStone(NULL);
            break;
        case 13:
            rana7->moveToStone(NULL);
            break;
        case 14:
            rana8->moveToStone(NULL);
            break;
        case 15:
            rana4->moveToStone(NULL);
            break;
        case 16:
            rana3->moveToStone(NULL);
            break;
        case 17:
            rana2->moveToStone(NULL);
            break;
        case 18:
            rana1->moveToStone(NULL);
            break;
        case 19:
            rana6->moveToStone(NULL);
            break;
        case 20:
            rana7->moveToStone(NULL);
            break;
        case 21:
            rana8->moveToStone(NULL);
            break;
        case 22:
            rana2->moveToStone(NULL);
            break;
        case 23:
            rana1->moveToStone(NULL);
            break;
        case 24:
            rana8->moveToStone(NULL);
            break;
    }
    CCLOG("%i",mHintStep);
    mHintStep++;
  
}


void FiveFrogs::Hint(Ref* sender){
    BaseScene::Hint(NULL);
}

void FiveFrogs::HintStop(Ref* sender)
{
    this->unschedule(mScheudleHint);
    mScheudleHint=NULL;
    BaseScene::HintStop(sender);
}



void FiveFrogs::HintCallback(bool accepted){
    if(accepted){
        BaseScene::HintCallback(accepted);
        colocarRanas();
        mHintStep=1;
        mSituacionHint=true;
        LayerHintOk::mostrar(true);
        mScheudleHint=schedule_selector(FiveFrogs::HintStartTemp);
        this->schedule(mScheudleHint,2.f);
    }
}


void FiveFrogs::onEnterTransitionDidFinish(){
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sounds/fivefrogs/jungle.mp3", true);
    BaseScene::onEnterTransitionDidFinish();
}
