//
//  Frog.h
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#ifndef __logicmaster2__Frog__
#define __logicmaster2__Frog__

#include <iostream>
#include "../widgets/Boton.h"
#include "Frog.fwd.h"
#include "FiveFrogs.h"

class Frog: public Boton
{
    
public:
    //typedef std::map< int, Frog*> PiedrasMap;
    
    Frog(bool esVerde,int posicion);
    virtual ~Frog(void);
    
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static cocos2d::Scene* scene();
    
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    
    static Frog* create(Node* parent,std::string imagen,bool esVerde,int posicion);
 
    void moveToStone(Ref* sender);
    
    static Point posicionPiedra(int index);
    bool esverde();
private:
    bool mIsMoving;
    bool mEsVerde;
    int mPosicion;
    void finMovement();
    
};


#endif /* defined(__logicmaster2__Frog__) */
