#include "LogicSQLHelper.h"
#include "fstream"
#include "../layers/LayerShop.h"
#include "../Variables.h"
#include "../localnotifications/LocalNotification.h"


//strings
std::string LogicSQLHelper::MEMORIA_NUMERO_PISTAS="NUMERO_PISTAS";
std::string LogicSQLHelper::LEVEL_INTENTOS_PREFIX_LOGICA="NUMERO_PISTAS";

std::string LogicSQLHelper::LEVEL_SUPERADO_PREFIX_LOGICA="SUP_LOGICA_";


std::string LogicSQLHelper::LEVEL_STARS_PREFIX_LOGICA="STARS_LOGICA_";


std::string LogicSQLHelper::LEVEL_LOGICA_JUGADO="LEVEL_JUGADO_";


std::string LogicSQLHelper::TUTORIAL_SUPERADO="TUTORIAL_SUPERADO";

std::string LogicSQLHelper::FLAG_REGALO_VOTO_USADO="FLAG_REGALO_VOTO_USADO";
std::string LogicSQLHelper::FLAG_REGALO_VOTO="RATE_REGALO";
std::string LogicSQLHelper::FLAG_REGALO_VOTO_NUMERO="FLAG_REGALO_VOTO_NUMERO";


bool LogicSQLHelper::getNivelSuperadoLogica(int nivel){
    if(estaElJuegoCompleto())
    {
        return true;
    }
    if(Variables::ES_DEBUG){
        return true;
    }
    std::string prefijo;
    prefijo=LogicSQLHelper::LEVEL_SUPERADO_PREFIX_LOGICA;
    std::ostringstream os;
    os<<nivel;
    std::string numero_nivel=os.str();
    
    std::string memoria=prefijo +"_"+ numero_nivel;
    return UserDefault::getInstance()->getBoolForKey(memoria.c_str(), false);
}

void LogicSQLHelper::setNivelSuperadoLogica(int nivel,int estrellas){
    std::string prefijo;
    prefijo=LogicSQLHelper::LEVEL_SUPERADO_PREFIX_LOGICA;
    std::ostringstream os;
    os<<nivel;
    std::string numero_nivel=os.str();
    std::string memoria=prefijo +"_"+ numero_nivel;
    UserDefault::getInstance()->setBoolForKey(memoria.c_str(), true);
    UserDefault::getInstance()->flush();
    
    std::ostringstream os1;
    os1<<nivel;
    numero_nivel=os1.str();
    prefijo=LogicSQLHelper::LEVEL_STARS_PREFIX_LOGICA;
    memoria=prefijo+"_"+ numero_nivel;
    UserDefault::getInstance()->setIntegerForKey(memoria.c_str(), estrellas);
    UserDefault::getInstance()->flush();
    
    
    
    
}







void LogicSQLHelper::setNivelSuperadoElixir(int idnivel,int numerogalardones){
    mtipoAccionSql=AccionSqlType(kUpdateSuperado);
    SQLiteHelper::openDB();
    char sqlgenerado [400];
    sprintf (sqlgenerado, "UPDATE NIVELES SET superado=1,estrellas=%i WHERE idnivel=%i",numerogalardones,idnivel);
    CCLOG("%s",sqlgenerado);
    SQLiteHelper::rc = sqlite3_exec(SQLiteHelper::_db, sqlgenerado, respuesta,this, &SQLiteHelper::zErrMsg);
        
    if( SQLiteHelper::rc!=SQLITE_OK ){
        fprintf(stderr, "SQL error: %s\n", SQLiteHelper::zErrMsg);
        sqlite3_free(SQLiteHelper::zErrMsg);
    }
    setNivelSuperadoElixirenMemoria(idnivel);
        
    
}


void LogicSQLHelper::setNivelSuperadoElixirenMemoria(int idnivel){
    UserDefault::getInstance()->setIntegerForKey("nivel_elixir_superado",idnivel);
    UserDefault::getInstance()->flush();
}


int LogicSQLHelper::getNivelSuperadoElixirenMemoria(){
   return  UserDefault::getInstance()->getIntegerForKey("nivel_elixir_superado",0);
}

void LogicSQLHelper::setNivelDesbloqueado(int idnivel){
    mtipoAccionSql=AccionSqlType(kUpdateUnlock);
    SQLiteHelper::openDB();
    char sqlgenerado [400];
    sprintf (sqlgenerado, "UPDATE NIVELES SET locked=0 WHERE idnivel=%i",idnivel);
    CCLOG("%s",sqlgenerado);
    SQLiteHelper::rc = sqlite3_exec(SQLiteHelper::_db, sqlgenerado, respuesta,this, &SQLiteHelper::zErrMsg);
    
    if( SQLiteHelper::rc!=SQLITE_OK ){
        fprintf(stderr, "SQL error: %s\n", SQLiteHelper::zErrMsg);
        sqlite3_free(SQLiteHelper::zErrMsg);
    }
}



void LogicSQLHelper::setAnimacionDesbloqueoPendiente(bool enabled,int idnivel,int estrellas){
    UserDefault::getInstance()->setBoolForKey("animacion_desbloqueo_pendiente",enabled);
    UserDefault::getInstance()->setIntegerForKey("animacion_desbloqueo_pendiente_idnivel",idnivel);
    UserDefault::getInstance()->setIntegerForKey("animacion_desbloqueo_pendiente_estrellas",estrellas);
    UserDefault::getInstance()->flush();
}



bool LogicSQLHelper::getAnimacionDesbloqueoPendiente(){
     return UserDefault::getInstance()->getBoolForKey("animacion_desbloqueo_pendiente",false);
}
int LogicSQLHelper::getAnimacionDesbloqueoPendienteIdNivel(){
     return UserDefault::getInstance()->getIntegerForKey("animacion_desbloqueo_pendiente_idnivel",0);
}
int LogicSQLHelper::getAnimacionDesbloqueoPendienteEstrellas(){
     return UserDefault::getInstance()->getIntegerForKey("animacion_desbloqueo_pendiente_estrellas",0);
}


int LogicSQLHelper::respuesta(void *NotUsed, int argc, char **argv, char **azColName){
    LogicSQLHelper* logicHelper= (LogicSQLHelper*) NotUsed;
    if(logicHelper->mtipoAccionSql==AccionSqlType(kUpdateSuperado) ){
         CCLOG("respuesta: OK superado");
    }else if(logicHelper->mtipoAccionSql==AccionSqlType(kUpdateUnlock) ){
         CCLOG("respuesta: OK unlock");
    }
    CCLOG("respuesta: OK");
    //return ?
}



int LogicSQLHelper::getElixiresGanados(){
    if(Variables::ES_DEBUG){
        return 1000;
    }
    return UserDefault::getInstance()->getIntegerForKey("elixires_disponibles",0);
}

void LogicSQLHelper::addElixiresUsuario(int numero){
    int ganados=getElixiresGanados()+ numero;
    UserDefault::getInstance()->setIntegerForKey("elixires_disponibles",ganados);
    UserDefault::getInstance()->flush();
}


int LogicSQLHelper::getElixiresParaSiguienteNivel(){
    int ganados=getElixiresGanados();
    int buscado=-1;
    for(int i=2;i<=14;i++){
        if(getElixiresGanados()< getElixiresRequeridosNivelLogica(i)){
            buscado=i;
            break;
        }
    }
    if(buscado==-1){//si tiene muchisimos elixires es el caso especial de pedirle 10000 por que si! jeje
        buscado=1000;
    }
    int siguienteNivel=getElixiresRequeridosNivelLogica(buscado);
    return siguienteNivel- ganados;
}





int LogicSQLHelper::addHintsUsuario(int numero){
    int pistas=getHintsUsuario();
    pistas=pistas+numero;
    UserDefault::getInstance()->setIntegerForKey(LogicSQLHelper::MEMORIA_NUMERO_PISTAS.c_str(), pistas);
    return pistas;
}
int LogicSQLHelper::getHintsUsuario(){
    return  UserDefault::getInstance()->getIntegerForKey(LogicSQLHelper::MEMORIA_NUMERO_PISTAS.c_str(),1);
}

void LogicSQLHelper::gastarPista(int level){
    bool comprado=true;
    char memoria[200];
    sprintf(memoria,"nivel_%i_hint",level);
    UserDefault::getInstance()->setBoolForKey(memoria, true);
    LogicSQLHelper::getInstance().addHintsUsuario(-1);
}







void LogicSQLHelper::sumarIntento(int nivel,TipoNivel tipoNivel){
    std::string prefijo;
    if(tipoNivel==TipoNivel(kNivelLogica)){
        prefijo=LogicSQLHelper::LEVEL_INTENTOS_PREFIX_LOGICA;
    }else if(tipoNivel==TipoNivel(kNivelElixir)){
        sumarIntentoSQL(nivel);
    }
    std::string numero_nivel= tostr(nivel);
    std::string memoria=prefijo+ numero_nivel;
    int num_intentos=UserDefault::getInstance()->getIntegerForKey(memoria.c_str(), 0);
    num_intentos++;
    CCLOG("Sumando intento: %s se queda en %i",memoria.c_str(),num_intentos);
    UserDefault::getInstance()->setIntegerForKey(memoria.c_str(), num_intentos);
    UserDefault::getInstance()->flush();
}

void LogicSQLHelper::sumarIntentoSQL(int idnivel){
    mtipoAccionSql=AccionSqlType(kUpdateUnlock);
    SQLiteHelper::openDB();
    char sqlgenerado [400];
    sprintf (sqlgenerado, "UPDATE NIVELES SET intentos=intentos+1 WHERE idnivel=%i",idnivel);
    CCLOG("%s",sqlgenerado);
    SQLiteHelper::rc = sqlite3_exec(SQLiteHelper::_db, sqlgenerado, respuesta,this, &SQLiteHelper::zErrMsg);
    
    if( SQLiteHelper::rc!=SQLITE_OK ){
        fprintf(stderr, "SQL error: %s\n", SQLiteHelper::zErrMsg);
        sqlite3_free(SQLiteHelper::zErrMsg);
    }
}


int LogicSQLHelper::getIntentos(int nivel,TipoNivel tipoNivel){
    std::string prefijo;
    if(tipoNivel==TipoNivel(kNivelLogica)){
        prefijo=LogicSQLHelper::LEVEL_INTENTOS_PREFIX_LOGICA;
    }else{
        CCAssert(false, "solo se obtienen  intentos para niveles de logica");
    }
    
    std::ostringstream os;
    os<<nivel;
    std::string numero_nivel=os.str();
    std::string memoria=prefijo+ numero_nivel;
    int intentos= UserDefault::getInstance()->getIntegerForKey(memoria.c_str(), 0);
    CCLOG("getIntentos %s intentos:%i",memoria.c_str(),intentos);
    return intentos;
}







bool LogicSQLHelper::estaElJuegoCompleto(){
    if(Variables::ES_DEBUG){
        return true;
    }
    return UserDefault::getInstance()->getBoolForKey(LayerShop::COMPRA_COMPLETE_GAME.c_str(), false);
}
bool LogicSQLHelper::estaSinPublicidadComprado(){
    if(Variables::ES_DEBUG){
        return true;
    }
    return (UserDefault::getInstance()->getBoolForKey(LayerShop::COMPRA_REMOVE_ADS.c_str(), false) ||
            UserDefault::getInstance()->getBoolForKey(LayerShop::COMPRA_COMPLETE_GAME.c_str(), false));
}

bool LogicSQLHelper::estaSinPublicidadCompradoCompra(){
    return (UserDefault::getInstance()->getBoolForKey(LayerShop::COMPRA_REMOVE_ADS.c_str(), false) );
    
}



int LogicSQLHelper::getNivelStarsLogica(int nivel)
{
    if(LogicSQLHelper::getInstance().estaElJuegoCompleto())
    {
        return 3;
    }
    std::ostringstream os;
    os<<nivel;
    std::string numero_nivel=os.str();
    std::string prefijo;
    prefijo=LogicSQLHelper::LEVEL_STARS_PREFIX_LOGICA;
    std::string memoria=prefijo+"_"+ numero_nivel;
    return UserDefault::getInstance()->getIntegerForKey(memoria.c_str(), 0);
}



bool LogicSQLHelper::userOwnsHint(int level){
    CCLOG("userOwnsHint:%i",level);
    if( LogicSQLHelper::getInstance().estaElJuegoCompleto()){
         return true;
    }
    if(level==Variables::SERIES_LEVEL || level== Variables::PSICO_LEVEL || level== Variables::FRUTAS_LEVEL){
        return false;
    }
     
    
    bool comprado=false;
    char memoria[200];
    sprintf(memoria,"nivel_%i_hint",level);
    bool pista_concreta=UserDefault::getInstance()->getBoolForKey(memoria, false);
    bool juego_completo=estaElJuegoCompleto();
    comprado= (pista_concreta || juego_completo);
    return comprado;
}

int LogicSQLHelper::getElixiresRequeridosNivelLogica(int idnivel) {
    int devolver = -1;
    switch (idnivel) {
        case 1:
            devolver = 0;
            break;
        case 2:
            devolver = 6;
            break;
        case 3:
            devolver = 12;
            break;
        case 4:
            devolver = 18;
            break;
        case 5:
            devolver = 24;
            break;
        case 6:
            devolver = 30;
            break;
        case 7:
            devolver = 34;
            break;
        case 8:
            devolver = 38;
            break;
        case 9:
            devolver = 40;
            break;
        case 10:
            devolver = 45;
            break;
        case 11:
            devolver = 50;
            break;
        case 12:
            devolver = 55;
            break;
        case 13:
            devolver = 60;
            break;
        case 14:
            devolver = 75;
            break;
        case 1000:
            devolver=1000;
            break;
            
    }
    return devolver;
}


bool LogicSQLHelper::getNivelJugadoAlgunaVezLogica(int nivel){
    std::string nivelJugado=LEVEL_LOGICA_JUGADO + tostr(nivel);
    return UserDefault::getInstance()->getBoolForKey(nivelJugado.c_str(), false);
}
void LogicSQLHelper::setNivelJugadoAlgunaVezLogica(int nivel)
{
    std::string nivelJugado=LEVEL_LOGICA_JUGADO + tostr(nivel);
    UserDefault::getInstance()->setBoolForKey(nivelJugado.c_str(), true);
    UserDefault::getInstance()->flush();
}

void LogicSQLHelper::setTutorialSuperado(){
    UserDefault::getInstance()->setBoolForKey(TUTORIAL_SUPERADO.c_str(), true);
    UserDefault::getInstance()->flush();
}
bool LogicSQLHelper::getTutorialSuperado()
{
     return UserDefault::getInstance()->getBoolForKey(TUTORIAL_SUPERADO.c_str() , false);
}



bool LogicSQLHelper::updateVersions(){
    SQLiteHelper::openDB();
    char sqlgenerado [1000];
    sprintf (sqlgenerado, "UPDATE niveles SET textonivel='41, 37, 33, ?, 25, 21, 17, 13' WHERE idnivel=4;UPDATE niveles SET textonivel='15, 29, 56, 108, ?' WHERE idnivel=88;UPDATE niveles SET solucion='208' WHERE idnivel=88;UPDATE niveles SET textonivel='4, 1, 5, 6, 11, 17, 28, ?' WHERE idnivel=115;UPDATE niveles SET textonivel='?, 16, 80, 480, 3360' WHERE idnivel=145;UPDATE niveles SET solucion='4' WHERE idnivel=145;UPDATE niveles SET solucion='C' WHERE idnivel=137;UPDATE niveles SET solucion='D' WHERE idnivel=122;");
    
    
       CCLOG("%s",sqlgenerado);
    SQLiteHelper::rc = sqlite3_exec(SQLiteHelper::_db, sqlgenerado, respuesta,this, &SQLiteHelper::zErrMsg);
    if( SQLiteHelper::rc!=SQLITE_OK ){
        fprintf(stderr, "SQL error: %s\n", SQLiteHelper::zErrMsg);
        sqlite3_free(SQLiteHelper::zErrMsg);
        return false;
    }
    return true;
   

}






bool LogicSQLHelper::updateVersions131(){
    SQLiteHelper::openDB();
    char sqlgenerado [1000];
    sprintf (sqlgenerado, "UPDATE niveles SET locked=0 WHERE 1");
    
    CCLOG("%s",sqlgenerado);
    SQLiteHelper::rc = sqlite3_exec(SQLiteHelper::_db, sqlgenerado, respuesta,this, &SQLiteHelper::zErrMsg);
    if( SQLiteHelper::rc!=SQLITE_OK ){
        fprintf(stderr, "SQL error: %s\n", SQLiteHelper::zErrMsg);
        sqlite3_free(SQLiteHelper::zErrMsg);
        return false;
    }
    return true;
}



void LogicSQLHelper::setRegaloGastado(bool activo){
    return  UserDefault::getInstance()->setBoolForKey(FLAG_REGALO_VOTO_USADO.c_str(), activo);
    UserDefault::getInstance()->flush();
}
bool LogicSQLHelper::getRegaloGastado(){
    return  UserDefault::getInstance()->getBoolForKey(FLAG_REGALO_VOTO_USADO.c_str(), false);
}

void LogicSQLHelper::setRegaloVoto(bool activo,int numero){
    UserDefault::getInstance()->setBoolForKey(FLAG_REGALO_VOTO.c_str(), activo);
    UserDefault::getInstance()->setIntegerForKey(FLAG_REGALO_VOTO_NUMERO.c_str(), numero);
    UserDefault::getInstance()->flush();
}

bool LogicSQLHelper::getRegaloVoto(){
   return  UserDefault::getInstance()->getBoolForKey(FLAG_REGALO_VOTO.c_str(), false);
}

int LogicSQLHelper::getRegaloNumero()
{
    return  UserDefault::getInstance()->getIntegerForKey(FLAG_REGALO_VOTO_NUMERO.c_str(), false);
}


void LogicSQLHelper::setSinPublicidadComprado(){
    UserDefault::getInstance()->setBoolForKey(LayerShop::COMPRA_REMOVE_ADS.c_str(), true);
    UserDefault::getInstance()->flush();
}



bool LogicSQLHelper::getRegaloNotifGastado(){
    return UserDefault::getInstance()->getBoolForKey(LocalNotification::NOTIFICATIONS_GIFT_KEY().c_str(),false);
  
}
void LogicSQLHelper::setRegaloNotif(bool gastado)
{
    UserDefault::getInstance()->setBoolForKey(LocalNotification::NOTIFICATIONS_GIFT_KEY().c_str(),gastado);
    UserDefault::getInstance()->flush();
}


template <typename T> std::string LogicSQLHelper::tostr(const T& t) { std::ostringstream os; os<<t; return os.str(); }



