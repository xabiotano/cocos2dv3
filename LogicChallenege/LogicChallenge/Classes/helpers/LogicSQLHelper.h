
#ifndef __logicmaster2__LogicSQLHelper__
#define __logicmaster2__LogicSQLHelper__
#include "cocos2d.h"
#include "../sql/SQLiteHelper.h"
#include <iostream>
#include "../TipoNivelTypes.h"

USING_NS_CC;

class LogicSQLHelper{
    
    typedef enum AccionSqlType
    {
        kUpdateSuperado,
        kUpdateUnlock
        
    } AccionSqlType;
    
public:

    static int getElixiresRequeridosNivelLogica(int idnivel);
    void setNivelSuperadoElixir(int idnivel,int numerogalardones);
    void setNivelSuperadoElixirenMemoria(int idnivel);
    int getNivelSuperadoElixirenMemoria();
    void setNivelDesbloqueado(int idnivel);
    void setAnimacionDesbloqueoPendiente(bool enabled,int idnivel,int estrellas);
    bool getAnimacionDesbloqueoPendiente();
    int getAnimacionDesbloqueoPendienteIdNivel();
    int getAnimacionDesbloqueoPendienteEstrellas();
    //logic
    bool getNivelSuperadoLogica(int nivel);
    void setNivelSuperadoLogica(int nivel,int estrellas);
    int getNivelStarsLogica(int nivel);
    bool getNivelJugadoAlgunaVezLogica(int nivel);
    void setNivelJugadoAlgunaVezLogica(int nivel);
    
    void setSinPublicidadComprado();
    
    
    
    int getElixiresGanados();
    int getElixiresParaSiguienteNivel();
    void addElixiresUsuario(int numero);
    
    //HINTS
    int addHintsUsuario(int numero);
    int getHintsUsuario();
    static void gastarPista(int level);
    bool userOwnsHint(int level);
    
    //INTENTOS
    void sumarIntento(int nivel,TipoNivel tipoNivel);
    int getIntentos(int idnivel, TipoNivel tipoNivel);
    void sumarIntentoSQL(int idnivel);

    
    
    
    
    //strings
    static std::string MEMORIA_NUMERO_PISTAS;
    static std::string LEVEL_STARS_PREFIX_LOGICA;

    static std::string LEVEL_INTENTOS_PREFIX_LOGICA;

    static std::string LEVEL_SUPERADO_PREFIX_LOGICA;
    
     static std::string LEVEL_LOGICA_JUGADO;
    
    
    static std::string TUTORIAL_SUPERADO;

    static std::string FLAG_REGALO_VOTO_USADO;
    static std::string FLAG_REGALO_VOTO;
    static std::string FLAG_REGALO_VOTO_NUMERO;

    
    
    bool estaElJuegoCompleto();//para las compras
    bool estaSinPublicidadComprado();//para las compras
    bool estaSinPublicidadCompradoCompra();

    
    
    //tutorial
    void setTutorialSuperado();
    bool getTutorialSuperado();
    
    bool updateVersions();
    bool updateVersions131();
    
    void setRegaloGastado(bool activo);
    bool getRegaloGastado();
    void setRegaloVoto(bool activo, int numero);
    bool getRegaloVoto();
    int getRegaloNumero();
  
    bool getRegaloNotifGastado();
    void setRegaloNotif(bool gastado);
       
    
    static int respuesta(void *NotUsed, int argc, char **argv, char **azColName);

    
public:
    static LogicSQLHelper& getInstance()
    {
        static LogicSQLHelper    instance; // Guaranteed to be destroyed.
        // Instantiated on first use.
        return instance;
    }
private:
    template <typename T> std::string tostr(const T& t);
    AccionSqlType mtipoAccionSql;
    LogicSQLHelper() {}                    // Constructor? (the {} brackets) are needed here.
    
    // C++ 03
    // ========
    // Dont forget to declare these two. You want to make sure they
    // are unacceptable otherwise you may accidentally get copies of
    // your singleton appearing.
    LogicSQLHelper(LogicSQLHelper const&);              // Don't Implement
    void operator=(LogicSQLHelper const&); // Don't implement
    
    // C++ 11
    // =======
    // We can use the better technique of deleting the methods
    // we don't want.
/*public:
    LogicSQLHelper(LogicSQLHelper const&)               = delete;
    void operator=(LogicSQLHelper const&)  = delete;*/
    
    // Note: Scott Meyers mentions in his Effective Modern
    //       C++ book, that deleted functions should generally
    //       be public as it results in better error messages
    //       due to the compilers behavior to check accessibility
    //
    
    
};


#endif /* defined(__logicmaster2__LogicSQLHelper__) */
