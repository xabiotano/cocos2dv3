#ifndef __TwoFrogs_SCENE_H__
#define __TwoFrogs_SCENE_H__

#include "cocos2d.h"
#include "../BaseScene.h"
#include "Frogtwo.fwd.h"
#include "../widgets/imports.h"

USING_NS_CC;

class TwoFrogs : public BaseScene
{
public:
    
    virtual ~TwoFrogs(void);
    virtual void Retry(Ref* sender) override;
    virtual void Hint(Ref* sender) override;
    virtual void HintCallback(bool accepted) override;
    virtual void HintStart(Ref* sender) override;
    void HintStop(Ref* sender) override;
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init() override;

    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static Scene* scene();
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(TwoFrogs);
    static TwoFrogs* getInstance();
    Dictionary* mArrayOfFrogs;
    virtual void checkFinish();
    void onEnterTransitionDidFinish() override;
    bool someFrogMoving;
    void HintStartTemp(float dt);

    
    
private:
    Frogtwo* rana1,*rana2,*rana3,*rana4;
    void colocarRanas();
    
   
};

#endif // __TwoFrogs_SCENE_H__
