
#ifndef __logicmaster2__Baloon__
#define __logicmaster2__Baloon__

#include <iostream>
#include "../widgets/Boton.h"
#include "Baloon.fwd.h"
#include "Bear.fwd.h"

typedef enum baloonState
{
    kBaloonIzquierda,
    kBaloonDerecha
    
} BaloonState;

typedef std::map< int, Bear*> BearMap;

class Baloon: public Boton
{
    public:
        Baloon(Point izquierda,Point derecha);
        virtual ~Baloon(void);
        // there's no 'id' in cpp, so we recommend to return the class instance pointer
        static cocos2d::Scene* scene();
        // preprocessor macro for "static create()" constructor ( node() deprecated )
        static Baloon* create(Node* parent,Point izquierda,Point derecha);
        void move(Ref* sender);
        bool estaEnIzquierda();
        bool mountBear(Bear* animal,Point positionBarca);
        void mountBearLogic(Bear* animal);
        bool unMountBear(Bear* animal);
       
        BaloonState mState;
        BearMap bearsMontados;
        int getPassengers();
        Point getSeat();
        bool isMoving();
        virtual bool containsTouchLocation(Touch* touch);
        void dibujarTexto();
    private:
        bool mEstaIzquierda;
        Point mPosicionIzquierda,mPosicionDerecha;
        void finMove();
        bool mIsMoving;
        unsigned int mSoundEffect;
        int numero_osos;
        Point getSeatBaloon();
        Point mSeat1;
        Point mSeat2;
    
};


#endif 
