//
//  Bear.cpp
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#include "Bear.h"
#include "SimpleAudioEngine.h"
#include "Baloon.h"

Bear::Bear(Point mPointIzq,Point mPointDch,std::string name,int z_index){
    mEstaIzquierda=true;
    mIsMoving=false;
    mPosicionIzquierda=mPointIzq;
    mPosicionDerecha=mPointDch;
    mEstado=kLadoIzquierdo;
    mName=name;
    setPosition(mPointIzq);
    mZIndex=z_index;
    mPositionBarca=Point(0,0);
    mSwallowTouches=true;
}

Bear::~Bear(void){}


Bear* Bear::create(Node* parent,std::string name,Point mPointIzq,Point mPointDch,std::string imagen,int z_index)
{
    Bear *devolver= new Bear(mPointIzq,mPointDch,name,z_index);
    devolver->retain();
    devolver->setPosition(devolver->mPosicionIzquierda);
    devolver->initBoton(devolver, menu_selector(Bear::click), imagen);
    return devolver;
}


void Bear::click(Ref* sender)
{
    if(mIsMoving){
        return;
    }
    if(VolcanScene::getInstance()->getBaloon()->isMoving()){
        CCLOG("CANOA MOVING");
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevator_badselected.mp3",false);
        return;
    }
    if(mEstado==BearState(kEmbarcado)){
        if(!VolcanScene::getInstance()->getBaloon()-> unMountBear(this)){
            CCLOG("SIN SENTIDO");
            return;
        }
        mIsMoving=true;
        Point destino;
        if(VolcanScene::getInstance()->getBaloon()->estaEnIzquierda()){
            destino=mPosicionIzquierda;
            this->mEstado=BearState(kLadoIzquierdo);
            this->mEstaIzquierda=true;
        }
        else{
            destino=mPosicionDerecha;
            this->mEstado=BearState(kLadoDerecho);
             this->mEstaIzquierda=false;
        }
        CallFunc *finUnmount =CallFunc::create([this]() { this->finUnmount(); });
        this->runAction(Sequence::createWithTwoActions(JumpTo::create(0.5f, destino, Director::getInstance()->getWinSize().height*0.1f, 1),finUnmount));
        this->runAction(ScaleTo::create(0.5f, 1.f));
        return;
    }
    else{
        if(VolcanScene::getInstance()->getBaloon()->getPassengers()==2){
            CCLOG("BARCA LLENA");
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevator_badselected.mp3",false);
            return;
        }
        if(VolcanScene::getInstance()->getBaloon()->estaEnIzquierda())
        {
            CCLOG("Canoa izq");
            if(this->mEstado==BearState(kLadoIzquierdo)){
                mPositionBarca=VolcanScene::getInstance()->getBaloon()->getSeat();
                VolcanScene::getInstance()->getBaloon()->mountBearLogic(this);
                CallFunc *finMove =CallFunc::create([this]() { this->finMount(); });
                this->runAction(Sequence::createWithTwoActions(JumpTo::create(0.5f,mPositionBarca , 100, 1),finMove));
                this->mEstado=BearState(kEmbarcado);
                mIsMoving=true;

            }
            
        }
        else{
            CCLOG("Canoa dch");
            if(this->mEstado==BearState(kLadoDerecho)){
                mPositionBarca=VolcanScene::getInstance()->getBaloon()->getSeat();
                VolcanScene::getInstance()->getBaloon()->mountBearLogic(this);
                CallFunc *finMove = CallFunc::create([this]() { this->finMount(); });
                this->runAction(Sequence::createWithTwoActions(JumpTo::create(0.5f,mPositionBarca , 100, 1),finMove));
                this->mEstado=BearState(kEmbarcado);
                mIsMoving=true;

            }
        }
    }
    
}

void Bear::finUnmount(){
    mIsMoving=false;
    std::string baby= std::string("baby");
    if (this->mName.find(baby) != std::string::npos) {
        CCLOG("BABY UNMOUNT");
        setTouchPriority(-10);
    }else{
        setTouchPriority(-9);
    }
    this->getParent()->reorderChild(this, mZIndex);
    this->mPositionBarca=Point(0,0);
    
}
void Bear::finMount(){
    VolcanScene::getInstance()->getBaloon()->mountBear(this,mPositionBarca);
    mIsMoving=false;
}
                    
bool Bear::estaEnIzquierda(){
    return mEstaIzquierda;
}

void Bear::setMoving(bool moving){
    this->mIsMoving=moving;
}


std::string Bear::getName(){
    return mName;
}









