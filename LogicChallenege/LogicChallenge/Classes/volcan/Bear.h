//
//  Bear.h
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#ifndef __logicmaster2__Bear__
#define __logicmaster2__Bear__

#include <iostream>
#include "../widgets/Boton.h"
#include "Bear.fwd.h"
#include "VolcanScene.h"


typedef enum BearState
{
    kLadoIzquierdo,
    kEmbarcado,
    kLadoDerecho
    
} BearState;

class Bear: public Boton
{
    
    
    public:
    
        Bear(Point mPointIzq,Point mPointDch,std::string name,int z_index);
        virtual ~Bear(void);
        static cocos2d::Scene* scene();
    
   
        static Bear* create(Node* parent,std::string imagen,Point mPointIzq,Point mPointDch,std::string name,int z_index);
        void click(Ref* sender);
        bool estaEnIzquierda();
        void setMoving(bool moving);
        std::string mName;
        std::string getName();
        BearState mEstado;
        int mZIndex;
        bool mEstaIzquierda;
       
    private:
    
        Point mPosicionIzquierda,mPosicionDerecha;
        void finMount();
        void finUnmount();
        bool mIsMoving;
        unsigned int mSoundEffect;
        Point mPositionBarca; 
   
    
    
        
    
};


#endif /* defined(__logicmaster2__Bear__) */
