#include "VolcanScene.h"
#include "SimpleAudioEngine.h"
#include "../layers/LayerGameOver.h"
#include "../layers/LayerSuccess.h"
#include "../Variables.h"
#include "../Levels.h"
#include "Baloon.h"
#include "Bear.h"
using namespace cocos2d;
using namespace CocosDenshion;


VolcanScene* instancia_volcan;


VolcanScene::~VolcanScene(void){
    CC_SAFE_RELEASE(mBear1);
    CC_SAFE_RELEASE(mBear2);
    CC_SAFE_RELEASE(mBear3);
    CC_SAFE_RELEASE(mBaby1);
    CC_SAFE_RELEASE(mBaby2);
    CC_SAFE_RELEASE(mBaby3);

  

}


Scene* VolcanScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    // 'layer' is an autorelease object
    VolcanScene *layer = VolcanScene::create();
    // add layer as a child to scene
    scene->addChild(layer);
     // return the scene
    return scene;
}

bool VolcanScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !BaseScene::init() )
    {
        return false;
    }
    instancia_volcan=this;
    mNombrePantalla="VolcanScene";
    mTipoNivel=kNivelLogica;
    
    mVisibleSize = Director::getInstance()->getVisibleSize();
    
    
    iniciarEscena();
    
    
    mTextoHelp=std::string(LanguageManager::getInstance()->getStringForKey("HELP_VOLCAN", "HELP_VOLCAN"));
    //
    return true;
}



void VolcanScene::iniciarEscena(){
    
    
    if(mBear1!=NULL){
        mBear1->removeFromParent();
        CC_SAFE_RELEASE(mBear1);
    }if(mBear2!=NULL){
        mBear2->removeFromParent();
        CC_SAFE_RELEASE(mBear2);
    }if(mBear3!=NULL){
        mBear3->removeFromParent();
        CC_SAFE_RELEASE(mBear3);

    }if(mBaby1!=NULL){
        mBaby1->removeFromParent();
        CC_SAFE_RELEASE(mBaby1);
    }if(mBaby2!=NULL){
        mBaby2->removeFromParent();
        CC_SAFE_RELEASE(mBaby2);
    }if(mBaby3!=NULL){
        mBaby3->removeFromParent();
        CC_SAFE_RELEASE(mBaby3);
    }
    if(mBaloon!=NULL){
        mBaloon->removeFromParent();
    }
   
    Point origin = Director::getInstance()->getVisibleOrigin();
    mFondo = Sprite::create("volcan/background.png");
    // position the sprite on the center of the screen
    mFondo->setPosition(Point(mVisibleSize.width/2 + origin.x, mVisibleSize.height/2 + origin.y));
    
    this->addChild(mFondo, 0);
    float scalex=Director::getInstance()->getVisibleSize().width/mFondo->getContentSize().width;
    float scaley=Director::getInstance()->getVisibleSize().height/mFondo->getContentSize().height;
    if(scalex>scaley){
        scaley=scalex;
    }
    mFondo->setScale(scaley);
    
    Director::getInstance()->setContentScaleFactor(960/mVisibleSize.width);
    Point izquierda=Point(mVisibleSize.width/2,mVisibleSize.height*0.9f);
    Point derecha=Point(mVisibleSize.width*0.6f,mVisibleSize.height*0.95f);
    
    mBaloon=Baloon::create(this,izquierda,derecha);
    mBaloon->setPosition(izquierda);
    this->addChild(mBaloon,Z_INDEX_GLOBO);
    mBaloon->setOpacity(200.f);
    
    mBear1=Bear::create(this,std::string("bear1"), Point(0.05f*mVisibleSize.width,0.4f *mVisibleSize.height), Point(0.95f*mVisibleSize.width,0.4f*mVisibleSize.height), std::string("volcan/bear_r.png"), Z_INDEX_OSOS);
    mBaby1=Bear::create(this,std::string("baby1"),  Point(0.07f *mVisibleSize.width,0.28f*mVisibleSize.height), Point(0.93f*mVisibleSize.width,0.28*mVisibleSize.height), std::string("volcan/baby_r.png"), Z_INDEX_BUBUS);
    
    
    mBear2=Bear::create(this,std::string("bear2"), Point(0.2f*mVisibleSize.width,0.45f*mVisibleSize.height), Point(0.83f*mVisibleSize.width,0.46f*mVisibleSize.height), std::string("volcan/bear_m.png"), Z_INDEX_OSOS);
    mBaby2=Bear::create(this,std::string("baby2"),  Point(0.20f*mVisibleSize.width,0.29f*mVisibleSize.height), Point(0.85f*mVisibleSize.width,0.35f*mVisibleSize.height), std::string("volcan/baby_m.png"),Z_INDEX_BUBUS);
    
    mBear3=Bear::create(this,std::string("bear3"),  Point(0.33f*mVisibleSize.width,0.37f*mVisibleSize.height), Point(0.67f*mVisibleSize.width,0.41f*mVisibleSize.height),std::string("volcan/bear_b.png"), Z_INDEX_OSOS);
    mBaby3=Bear::create(this,std::string("baby3"),  Point(0.30f*mVisibleSize.width,0.25f*mVisibleSize.height), Point(0.66f*mVisibleSize.width,0.3f*mVisibleSize.height), std::string("volcan/baby_b.png"), Z_INDEX_BUBUS);
    
    
    mBaloon->setSound("sounds/volcan/baloon.mp3");
    mBear1->setSound("sounds/volcan/montar.mp3");
    mBear2->setSound("sounds/volcan/montar.mp3");
    mBear3->setSound("sounds/volcan/montar.mp3");
    mBaby1->setSound("sounds/volcan/montar.mp3");
    mBaby2->setSound("sounds/volcan/montar.mp3");
    mBaby3->setSound("sounds/volcan/montar.mp3");
    
    this->addChild(mBear1,VolcanScene::Z_INDEX_OSOS);
    this->addChild(mBear2,VolcanScene::Z_INDEX_OSOS);
    this->addChild(mBear3,VolcanScene::Z_INDEX_OSOS);
    this->addChild(mBaby1,VolcanScene::Z_INDEX_BUBUS);
    this->addChild(mBaby2,VolcanScene::Z_INDEX_BUBUS);
    this->addChild(mBaby3,VolcanScene::Z_INDEX_BUBUS);
}




VolcanScene* VolcanScene::getInstance(){
    return instancia_volcan;
};

void VolcanScene::checkFinish(Ref* sender){
    
    //algun baby esta solo  en la derecha
    if(mBear1->estaEnIzquierda() && !mBaby1->estaEnIzquierda()){
        if(!mBear2->estaEnIzquierda() || !mBear2->estaEnIzquierda()){
            mGameover=true;
        }
    }
    if(mBear2->estaEnIzquierda() && !mBaby2->estaEnIzquierda()){
        if(!mBear1->estaEnIzquierda() || !mBear3->estaEnIzquierda()){
            mGameover=true;
        }
    }
    if(mBear3->estaEnIzquierda() && !mBaby3->estaEnIzquierda()){
        if(!mBear1->estaEnIzquierda() || !mBear2->estaEnIzquierda()){
            mGameover=true;
        }
    }
    //algun baby esta solo en la izquierda
    if(!mBear1->estaEnIzquierda() && mBaby1->estaEnIzquierda()){
        if(mBear2->estaEnIzquierda() || mBear3->estaEnIzquierda()){
            mGameover=true;
        }
    }
    if(!mBear2->estaEnIzquierda() && mBaby2->estaEnIzquierda()){
        if(mBear1->estaEnIzquierda() || mBear3->estaEnIzquierda()){
            mGameover=true;
        }
    }
    if(!mBear3->estaEnIzquierda() && mBaby3->estaEnIzquierda()){
        if(mBear1->estaEnIzquierda() || mBear2->estaEnIzquierda()){
            mGameover=true;
        }
    }
    if(!mBear1->estaEnIzquierda() && !mBear2->estaEnIzquierda() && !mBear3->estaEnIzquierda()
       && !mBaby1->estaEnIzquierda() && !mBaby2->estaEnIzquierda() && !mBaby3->estaEnIzquierda() ){
        mStars=3;
        mSuccess=true;
    }
    mSmallGameOver=true;
    mTextoGameOver=LanguageManager::getInstance()->getStringForKey("LEFT_BABY");
    BaseScene::checkFinish(NULL);
    
};


Baloon* VolcanScene::getBaloon(){
    return mBaloon;
}


void VolcanScene::HintStart(float dt){
    if(!mSituacionHint){
        return;
    }
    mHintStep++;
    CCLOG("PASO %i",mHintStep);
    switch (mHintStep) {
        case 1:
            mBaby1->click(NULL);
            mBaby2->click(NULL);
            break;
        case 2:
            mBaloon->move(NULL);
            break;
        case 3:
            mBaby1->click(NULL);
            break;
        case 4:
            mBaloon->move(NULL);
            break;
        case 5:
            mBaby3->click(NULL);
            mBaby1->click(NULL);
            break;
        case 6:
            mBaloon->move(NULL);
            break;
        case 7:
            mBaby3->click(NULL);
            break;
        case 8:
            mBaloon->move(NULL);
            break;
        case 9:
            //mBaby1->click();
            mBear1->click(NULL);
            mBear2->click(NULL);
            break;
        case 10:
             mBaloon->move(NULL);
            break;
        case 11:
            mBear1->click(NULL);
            mBaby1->click(NULL);
            break;
       case 12:
             mBaloon->move(NULL);
            break;
        case 13:
            mBear1->click(NULL);
            mBear3->click(NULL);
            break;
        case 14:
            mBaloon->move(NULL);
            break;
        case 15:
            mBaby2->click(NULL);
            break;
        case 16:
            mBaloon->move(NULL);
            break;
        case 17:
            mBaby1->click(NULL);
            mBaby2->click(NULL);
            break;
        case 18:
             mBaloon->move(NULL);
            break;
        case 19:
            mBaby1->click(NULL);
            break;
        case 20:
            mBaloon->move(NULL);
            break;
        case 21:
            mBaby1->click(NULL);
             mBaby3->click(NULL);
            break;
        case 22:
            mBaloon->move(NULL);
            break;
       
    }
    if(mHintStep==22){
        BaseScene::HintStop(NULL);
    }

}

void VolcanScene::Hint(Ref* sender){
    BaseScene::Hint(NULL);
}


void VolcanScene::HintCallback(bool accepted){
    if(accepted){
        CCLOG("MOSTRAR");
        BaseScene::HintCallback(accepted);
        iniciarEscena();
        mSituacionHint=true;
        mHintStep=0;
        LayerHintOk::mostrar(true);
        mScheudleHint=schedule_selector(VolcanScene::HintStart);
        this->schedule(mScheudleHint,2.f);
        
        
    }
}



void VolcanScene::Retry(Ref* sender){
    Director::getInstance()->replaceScene(TransitionCrossFade::create(0.5f, VolcanScene::scene()));
}


void VolcanScene::onEnterTransitionDidFinish(){
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sounds/volcan/forest.mp3", true);
    BaseScene::onEnterTransitionDidFinish();
}


