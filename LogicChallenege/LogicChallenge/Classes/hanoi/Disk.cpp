//
//  Disk.cpp
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#include "Disk.h"
#include "SimpleAudioEngine.h"
#include "DisksScene.h"
#include "Stick.h"


Disk::Disk(Stick* stick_origen,float scale){
    mScale=scale;
    mStickOrigen=stick_origen;
    this->setScaleX(scale);
    mImagenOutStick =Director::getInstance()->getTextureCache()->addImage("hanoi/disk_normal.png");
    mImagenInStick=Director::getInstance()->getTextureCache()->addImage("hanoi/disk_in_stick.png");
    initWithTexture(mImagenInStick);
    _colorfijado=false;
    mListener = EventListenerTouchOneByOne::create();
    mListener->setSwallowTouches(true);
    mListener->onTouchBegan = CC_CALLBACK_2(Disk::onTouchBegan, this);
    mListener->onTouchMoved = CC_CALLBACK_2(Disk::onTouchMoved, this);
    mListener->onTouchEnded = CC_CALLBACK_2(Disk::onTouchEnded, this);
    
    
   this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(mListener, this);
    
}
Disk::~Disk(void)
{
 this->getEventDispatcher()->removeEventListener(mListener);
   
    
}





Disk* Disk::create(Node* parent,std::string imagen,Stick* stick_origen,float scale)
{
    Disk *devolver= new Disk(stick_origen,scale);
    devolver->autorelease();
    devolver->initBoton(parent, menu_selector(Disk::clickDisk), imagen);
    return devolver;
}


void Disk::clickDisk(Ref* sender)
{
 
}


bool Disk::onTouchBegan(Touch* touch, Event* event){
    if ( !containsTouchLocation(touch) )
    {
        return false;
    }
    if(DisksScene::getInstance()->touchingDisk!=NULL){
        CCLOG("YA HAS COGIDO UNO!");
        return false;
    }
   
    if(this->mStickOrigen->getLastDisk()!=this){
        CCLOG("NOT LAST DISK");
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevator_badselected.mp3",false);
        return false;
    }
    removeFromStick();
    DisksScene::getInstance()->touchingDisk=this;
    return Boton::onTouchBegan(touch, event);
    
}

void Disk::onTouchMoved(Touch* touch, Event* event)
{
   
    Point puntonuevo=Director::getInstance()->convertToGL(touch->getLocationInView());
    this->setPosition(puntonuevo);
}


void Disk::onTouchEnded(Touch* touch, Event* event){
    Stick* stick=getTouchingStick(touch);
    setStick(stick);
    Boton::onTouchEnded(touch, event);
    DisksScene::getInstance()->touchingDisk=NULL;
}



Stick* Disk::getTouchingStick(Touch* touch){
    
     Point puntonuevo=Director::getInstance()->convertToGL(touch->getLocationInView());
       Size sizeOfScreen=Director::getInstance()->getWinSize();
    if(puntonuevo.x < sizeOfScreen.width*0.33f ){
        CCLOG("stick1 destination");
        return DisksScene::getInstance()->mStick1;
    }else if(puntonuevo.x > sizeOfScreen.width*0.33f && puntonuevo.x<=sizeOfScreen.width*0.66f){
        CCLOG("stick2 destination");
        return DisksScene::getInstance()->mStick2;
    }
    else if(puntonuevo.x>sizeOfScreen.width*0.66f){
        CCLOG("stick3 destination");
        return DisksScene::getInstance()->mStick3;
    }
}


void Disk::removeFromStick()
{
    mStickOrigen->removeDisk(this);
    this->initWithTexture(mImagenOutStick);
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/pop.mp3",false);
    this->getParent()->reorderChild(this, 10);
}

void Disk::setStick(Stick* stick){
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/pop_grave.mp3",false);
    this->initWithTexture(mImagenInStick);
    
    bool esElMismo=false;
    if(stick==mStickOrigen){
        esElMismo=true;
    }
    //intento anadirlo al destino, sino al origen
    bool anadido= stick->addDisk(this);
    if(anadido ){
        if(!esElMismo){
            CCLOG("Descender counter");
            DisksScene::getInstance()->descenderCounter();
        }else{
            CCLOG("Anadido en origen");
            
        }
        mStickOrigen=stick;
    }else{
        mStickOrigen->addDisk(this);
    }
}





