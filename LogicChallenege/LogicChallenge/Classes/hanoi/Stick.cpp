//
//  Stick.cpp
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#include "Stick.h"
#include "SimpleAudioEngine.h"
#include "DisksScene.h"

Stick::Stick(int posicion)
{
    mPosicion=posicion;
    mArrayOfDisks=Array::create();
    mArrayOfDisks->retain();

}
Stick::~Stick(void)
{
    CC_SAFE_RELEASE(mArrayOfDisks);
}





Stick* Stick::create(Node* parent,std::string imagen,int posicion)
{
    Stick *devolver= new Stick(posicion);
    devolver->autorelease();
    devolver->initWithFile(imagen);
    return devolver;
}


void Stick::clickStick(Ref* sender)
{
   
}


bool Stick::addDisk(Disk* disk){
    
   
    if(mArrayOfDisks->count() >0 && getLastDisk()->mScale< disk->mScale  ){
        return false;
    }
    if(mArrayOfDisks->count() >0 ){
         CCLOG("addDisk Sticklast:%f currentMove:%f ", getLastDisk()->mScale, disk->mScale);
    }
    
    disk->mStickOrigen=this;
    
    Point destino= Point(this->getPositionX(),getYOfDisk(disk));
    disk->setPosition(destino);

    //lo anadimos al stick
    CCLOG("Adddisk %i",(unsigned int)mArrayOfDisks->count());
    mArrayOfDisks->addObject(disk);
    for(int i=0;i<mArrayOfDisks->count();i++){
        DisksScene::getInstance()->reorderChild((Disk*)mArrayOfDisks->getObjectAtIndex(i), i);
    }
    ssize_t lastDisk=mArrayOfDisks->count();
    CCLOG("Adddisk later %i",(unsigned int)lastDisk);
    return true;
}

void Stick::removeDisk(Disk* disk){
    CCLOG("removeDisk mArrayOfDisks size before %i",(unsigned int)mArrayOfDisks->count());
    mArrayOfDisks->removeLastObject();
    CCLOG("removeDisk mArrayOfDisks size after %i",(unsigned int)mArrayOfDisks->count());

}

float Stick::getYOfDisk(Disk* disk){
  
    float paddingBottom=this->getContentSize().height*0.15f;
    if(mArrayOfDisks->count()>0 && (Disk*)mArrayOfDisks->getLastObject() !=NULL){
        Disk* ultimo= (Disk*)mArrayOfDisks->getLastObject();
        return ultimo->getPositionY() + disk->getBoundingBox().size.height*0.6f;
    }else{
        return paddingBottom+disk->getBoundingBox().size.height/2;
    }
   
}

Disk* Stick::getLastDisk(){
    return (Disk*)mArrayOfDisks->getLastObject();
}



void Stick::removeAllDisks(){
    if(mArrayOfDisks->count()>0){
        mArrayOfDisks->removeAllObjects();
    }
    removeAllChildren();
}











