#ifndef __DisksScene_SCENE_H__
#define __DisksScene_SCENE_H__

#include "cocos2d.h"
#include "../BaseScene.h"
#include "Disk.fwd.h"
#include "Stick.fwd.h"


USING_NS_CC;

class DisksScene : public BaseScene
{
public:
    virtual void Retry(Ref* sender);
    virtual void Hint(Ref* sender);
    virtual void HintCallback(bool accepted);
    virtual void HintStart(Ref* sender);
     virtual void TutorialStart(){};
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init();

    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static Scene* scene();
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(DisksScene);
    static DisksScene* getInstance();
    
    virtual void checkFinish();
    Stick* mStick1,*mStick2,*mStick3;
    void descenderCounter();
    void onEnterTransitionDidFinish();
    Disk* touchingDisk;
private:
    Disk* disk1,*disk2,*disk3,*disk4,*disk5;
    void colocarDisks();
    void crearPanel();
    int mMovements;
    Label* mCounter;
    void HintStartTemp(float dt);
    
    
    
   
};

#endif // __DisksScene_SCENE_H__
