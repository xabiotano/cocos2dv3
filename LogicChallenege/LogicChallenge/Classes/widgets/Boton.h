//
//  Boton.h
//  trivial
//
//  Created by Xabier on 11/04/13.
//
//

#ifndef __trivial__Boton__
#define __trivial__Boton__

#include <iostream>
#include "cocos2d.h"



USING_NS_CC;
class Boton: public Sprite
{
public:
    static Boton* create(Texture2D *pTexture);
    Boton(void);
    virtual ~Boton(void);
    static Boton* createBoton(Node* parent, SEL_MenuHandler accion,std::string imagen);
    static Boton* createBotonWithPriority(Node* _parent,SEL_MenuHandler accion,std::string url_imagen,int priority);
    
    static Boton* createBoton(SEL_MenuHandler accion,std::string url_imagen);
    static Boton* createBotonWithPriority(SEL_MenuHandler accion,std::string url_imagen,int priority);
    static Boton* createBotonWithPriorityAndSwallow(Node* _parent,SEL_MenuHandler accion,std::string url_imagen,int priority,bool swallow);
    
    
    bool initBoton(Node* _parent,SEL_MenuHandler accion,std::string url_imagen);
    bool initBotonWithPriority(Node* _parent,SEL_MenuHandler accion,std::string url_imagen,int priority);
    
    void onEnter() override;
    void onExit() override;
    void onExitTransitionDidStart() override;
    
    bool _movido;
    Node* _parent;
    
    virtual void activo();
    virtual void inactivo();
    void setSound(std::string soundname);
    void setColorBoton(Color3B color);
    void setTexto(std::string texto);
    void setColorSroke(Color3B color);
    Color3B _color;
    bool _colorfijado;
    Color3B mColorStroke;
    int _numero_nivel;
    void setEnabled(bool activo);
    void setTouchPriority(int priority);
    void addEventListener();
    void addEventListenerSelfPriority();
    virtual bool containsTouchLocation(Touch* touch);
   
    void setSwallowTouches(bool swallowTouches);
    bool mSwallowTouches;
    int mPriority;
   void customInit(Node* _parent,SEL_MenuHandler accion);
    
    
protected:
    EventListenerTouchOneByOne* mListener;
    virtual bool onTouchBegan(Touch* touch, Event* event);
    virtual void onTouchMoved(Touch* touch, Event* event);
    virtual void onTouchEnded(Touch* touch, Event* event);
    std::string _sound;
    int GetApproxDistance(Point pt1, Point pt2);
     SEL_MenuHandler _accion;
    void noActionLaunch();
 
private:
    bool _enabled;
    bool _noActionLaunch;
    Point _puntotoque;
  
    Label* mLabel;
    
};




#endif /* defined(__trivial__Boton__) */
