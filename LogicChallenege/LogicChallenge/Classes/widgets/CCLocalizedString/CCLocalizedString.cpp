//
//  CCLocalizedString.cpp
//  SkeletonX
//
//  Created by mac on 11-12-1.
//  Copyright (c) 2011年 GeekStudio. All rights reserved.
//



#include "cocos2d.h"
USING_NS_CC;

#include <iostream>
#include <sstream>
#include <string>
#include <map>
using namespace std;

#include "CCLocalizedString.h"
#include "../LanguageManager.h"


static map<std::string,std::string> localizedStrings;

void PurgeCCLocalizedStringCached()
{
    localizedStrings.clear();
}

const char * CCLocalizedString(const char * mKey,const char * mComment)
{
    return LanguageManager::getInstance()->getStringForKey(mKey).c_str();
}
