//
//  LevelButton.h
//  trivial
//
//  Created by Xabier on 11/04/13.
//
//

#ifndef __trivial__LevelButton__
#define __trivial__LevelButton__

#include <iostream>
#include "cocos2d.h"

#include "Levelsv2.fwd.h"


USING_NS_CC;
class LevelButton: public Sprite
{
public:
    LevelButton(void);
    virtual ~LevelButton(void);
    
    static LevelButton* create(Texture2D* pobTexture);

    //static LevelButton* createLevelButton(Node* parent, SEL_MenuHandler accion,std::string imagen);
    static LevelButton* createLevelButtonWithTexture(SEL_MenuHandler accion,Texture2D* pobTexture,int tipo,int idnivel);
   // static LevelButton* createLevelButtonItem(SEL_MenuHandler accion,std::string url_imagen);
   
 
    
    virtual void onEnter();
    virtual void onExit();
    bool _movido;
        
    virtual void activo();
    virtual void inactivo();
    void setSound(std::string soundname);
    void setColorLevelButton(Color3B color);
    void setTexto(std::string texto);
    Color3B _color;
    bool _colorfijado;
    int _numero_nivel;
    void setEnabled(bool activo);
    void setTouchPriority(int priority);
    virtual bool containsTouchLocation(Touch* touch);
    //void initLevelButton(Node* _parent,SEL_MenuHandler accion,std::string url_imagen);
    SEL_MenuHandler _accion;
    
    
    /*IMPLEMENTACION DE METODOS*/
    
    /*SpriteBatchNode * item_madera_tex;
    SpriteBatchNode * item_elixir_cerebro_tex;
    SpriteBatchNode * item_elixir_fresa_tex;
    SpriteBatchNode * item_elixir_energia_tex;
    SpriteBatchNode * item_elixir_locked_tex;
    SpriteBatchNode * item_elixir_item_wood_tex;
    SpriteBatchNode * item_elixir_item_wood_hole_tex;
    SpriteBatchNode * item_elixir_item_tex;*/
    int mTipo;
    int mIdNivel;
    
    void unlock();
    void pintarSuperado();
    void pintarTipo(Levelsv2* parent,bool superado,bool locked,int tipo);
    void pintarElixiresGanados(Levelsv2* parent,int numeroestrellas);
    void animarElixirRecienSuperado(Levelsv2* parent,int numeroBotellitasActualizar);

protected:
    EventListenerTouchOneByOne* mListener;
    virtual bool onTouchBegan(Touch* touch, Event* event);
    virtual void onTouchMoved(Touch* touch, Event* event);
    virtual void onTouchEnded(Touch* touch, Event* event);
    std::string _sound;
    int GetApproxDistance(Point pt1, Point pt2);
    
    void noActionLaunch();
private:
   
    bool _noActionLaunch;
    Point _puntotoque;
   
    Label* mLabel;
    
};




#endif /* defined(__trivial__LevelButton__) */
