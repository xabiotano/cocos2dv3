//
//  StarLocked.h
//  logicmaster2
//
//  Created by Xabier on 23/11/13.
//
//
#ifndef __logicmaster2__StarLocked__
#define __logicmaster2__StarLocked__

#include <iostream>
#include "cocos2d.h"


USING_NS_CC;
class StarLocked: public Sprite
{
public:
    StarLocked(void);
    virtual ~StarLocked(void);
    static StarLocked* create(bool locked);
   
    void unlock();
   
};




#endif /* defined(__logicmaster2__StarLocked__) */
