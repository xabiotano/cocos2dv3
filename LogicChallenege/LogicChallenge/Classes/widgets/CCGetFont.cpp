
#include "cocos2d.h"
#include "CCGetFont.h"
USING_NS_CC;

#include <iostream>
#include <sstream>
#include <string>
#include <map>
using namespace std;




const char * CCGetFont()
{
    
    LanguageType curLanguage = Application::getInstance()->getCurrentLanguage();
    const char * fileName;
    switch (curLanguage) {
        case LanguageType::RUSSIAN:
            fileName = "fonts/Days.otf";
            break;
        case LanguageType::KOREAN:
            fileName = "fonts/BMYEONSUNG.otf";
        break;
        default:
            fileName = "fonts/JungleFever.ttf";
            break;
            
    }
    
    return fileName;
}




