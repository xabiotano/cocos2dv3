#include "NivelItem.h"
#include "../Variables.h"
#include "../helpers/LogicSQLHelper.h"

template <typename T> std::string NivelItem::tostr(const T& t) { std::ostringstream os; os<<t; return os.str(); }

NivelItem::~NivelItem(void){};

NivelItem* NivelItem::create(Node* parent, SEL_MenuHandler accion,int numNivel,TipoNivel tipoNivel)
{
    CCLOG("numNivel %i",numNivel);
    NivelItem* nivelitem= new NivelItem(numNivel,tipoNivel);
    if (nivelitem && nivelitem->initBoton(parent, accion, NivelItem::getImagenNivel(numNivel)))
    {
        nivelitem->autorelease();
        nivelitem->initNivelItem();
        nivelitem->setPosition(nivelitem->getPositionNivel(numNivel));
        nivelitem->setTag(numNivel);
        return nivelitem;
    }
    CC_SAFE_DELETE(nivelitem);
    return NULL;
}


NivelItem::NivelItem(int numNivel,TipoNivel tipoNivel){
    mNumNivel=numNivel;
    mTipoNivel=tipoNivel;
};

void  NivelItem::initNivelItem(){

    char temp[100];
    sprintf(temp,"%i",mNumNivel);
    /*CCLabelStroke* numeroNivelLabel=CCLabelStroke::create(this, temp, "fonts/JungleFever.ttf", 60, Size(getContentSize().width, getContentSize().height), kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter);
    numeroNivelLabel->setPosition(Point(getContentSize().width/2,getHeightPositionTexto(mNumNivel)));
    numeroNivelLabel->setColor(Color3B(255,255,255));
    addChild(numeroNivelLabel,3);
    numeroNivelLabel->setString(temp, Color3B((0,0,0));*/
    Label* numeroNivelLabel=Label::createWithTTF(temp, "fonts/JungleFever.ttf", 60);
    numeroNivelLabel->setWidth(getContentSize().width);
    numeroNivelLabel->setHeight(getContentSize().height);
    numeroNivelLabel->setHorizontalAlignment(TextHAlignment::CENTER);
    numeroNivelLabel->setVerticalAlignment(TextVAlignment::CENTER);
    numeroNivelLabel->enableOutline(Color4B(0,0,0,255),4);
    numeroNivelLabel->setPosition(Point(getContentSize().width/2,getHeightPositionTexto(mNumNivel)));
    addChild(numeroNivelLabel,3);
    
    
    drawStars(LogicSQLHelper::getInstance().getNivelStarsLogica(mNumNivel));
    mJugable=BaseScene::getNivelJugable(mNumNivel,mTipoNivel);
    if(!mJugable){
        this->setColor(Color3B(100,100,100));
    }
    bool superado= LogicSQLHelper::getInstance().getNivelSuperadoLogica(mNumNivel);
    if(!superado){
        dibujarElixiresRequeridos();
    }
   
    
    

}
void NivelItem::dibujarElixiresRequeridos(){
    int elixiresReq=NivelItem::getElixiresRequeridos(mNumNivel);
    if(elixiresReq>0  ){
        Sprite* elixirIcono=Sprite::create("elixircomun/elixir_s.png");
        elixirIcono->setPosition(Point(this->getContentSize().width*0.5f,this->getContentSize().height*0.2f));
        elixirIcono->setScale(0.9f);
        this->addChild(elixirIcono);
        if(elixiresReq <= LogicSQLHelper::getInstance().getElixiresGanados()){
            Sprite* check=Sprite::create("ui/check.png");
            check->setPosition(Point(elixirIcono->getContentSize().width*0.6f,elixirIcono->getContentSize().height*0.2f));
            elixirIcono->addChild(check,3);
        }else{
            std::string granos=std::string("x")+ tostr(elixiresReq);
            Label* label=Label::createWithTTF(granos.c_str(), CCGetFont(), 40);
            label->setWidth(elixirIcono->getContentSize().width*2);
            label->setHeight(elixirIcono->getContentSize().height);
            label->setHorizontalAlignment(TextHAlignment::RIGHT);
            label->setVerticalAlignment(TextVAlignment::CENTER);
            label->enableOutline(Color4B(0,0,0,255),4);
            label->setPosition(Point(elixirIcono->getContentSize().width*0.6f,elixirIcono->getContentSize().height*0.2f));
            elixirIcono->addChild(label,3);

        }
    }
}





int NivelItem::getNivel()
{
    return mNumNivel;
}






bool NivelItem::isLocked(int numNivel)
{
    return true;
}

std::string NivelItem::getImagenNivel(int numNivel)
{
    std::string devolver;
    //CCLOG("numNivel %i",numNivel);
    switch(numNivel)
    {
        case 1:
            devolver=std::string("ui/levels/items/1.png");
            break;
        case 2:
            devolver=std::string("ui/levels/items/2.png");
            break;
        case 3:
            devolver=std::string("ui/levels/items/3.png");
            break;
        case 4:
           devolver=std::string("ui/levels/items/4.png");
            break;
        case 5:
            devolver=std::string("ui/levels/items/5.png");
            break;
        case 6:
            devolver=std::string("ui/levels/items/6.png");
            break;
        case 7:
            devolver=std::string("ui/levels/items/7.png");
            break;
        case 8:
            devolver=std::string("ui/levels/items/3.png");
            break;
        case 9:
            devolver=std::string("ui/levels/items/8.png");
            break;
        case 10:
            devolver=std::string("ui/levels/items/9.png");
            break;
        case 11:
            devolver=std::string("ui/levels/items/2.png");
            break;
        case 12:
            devolver=std::string("ui/levels/items/5.png");
            break;
        case 13:
            devolver=std::string("ui/levels/items/10.png");
            break;
        case 14:
            devolver=std::string("ui/levels/items/11.png");
            break;
            
            
    }
    return devolver;
    
}

float NivelItem::getHeightPositionTexto(int i){
   
    Point posicion;
    switch(i)
    {
        case 1:
            return getContentSize().height*0.65f;
            break;
        case 2:
             return getContentSize().height*0.54f;
            break;
        case 3:
            return getContentSize().height*0.4f;
            break;
        case 4:
             return getContentSize().height*0.55;
            break;
        case 5:
            return getContentSize().height*0.7f;
            break;
        case 6:
            return getContentSize().height*0.7f;
            break;
        case 7:
             return getContentSize().height*0.65;
            break;
        case 8:
            return getContentSize().height*0.4f;
            break;
        case 9:
             return getContentSize().height*0.69f;
            break;
        case 10:
            return getContentSize().height*0.7f;
            break;
        case 11:
            return getContentSize().height*0.54f;
            break;
        case 12:
             return getContentSize().height*0.65f;
            break;
        case 13:
             return getContentSize().height*0.6;
            break;
        case 14:
             return getContentSize().height/2;
            break;
    }
    return getContentSize().height/2;;
}

Point NivelItem::getPositionNivel(int i)
{
    float tamanoY= ((Node*)_parent)->getContentSize().height;
    float tamanoX= ((Node*)_parent)->getContentSize().width;
    
    //CCLOG("width:%f height:%f",tamanoX, tamanoY);
    Point posicion;
    switch(i)
    {
        case 1:
            posicion=Point(tamanoX*0.23f,tamanoY*0.12f);
            break;
        case 2:
            posicion=Point(tamanoX*0.6f,tamanoY*0.18f);
            break;
        case 3:
             posicion=Point(tamanoX*0.92f,tamanoY*0.25f);
            break;
        case 4:
            posicion=Point(tamanoX*0.74f,tamanoY*0.32f);
            break;
        case 5:
            posicion=Point(tamanoX*0.5f,tamanoY*0.38f);
            break;
        case 6:
            posicion=Point(tamanoX*0.25f,tamanoY*0.42f);
            break;
        case 7:
            posicion=Point(tamanoX*0.1f,tamanoY*0.51f);
            break;
        case 8:
            posicion=Point(tamanoX*0.35f,tamanoY*0.57f);
            break;
        case 9:
            posicion=Point(tamanoX*0.74f,tamanoY*0.57f);
            break;
        case 10:
            posicion=Point(tamanoX*0.9f,tamanoY*0.66f);
            break;
        case 11:
            posicion=Point(tamanoX*0.84f,tamanoY*0.785f);
            break;
        case 12:
            posicion=Point(tamanoX*0.4f,tamanoY*0.8f);
            break;
        case 13:
            posicion=Point(tamanoX*0.23f,tamanoY*0.9f);
            break;
        case 14:
            posicion=Point(tamanoX*0.64f,tamanoY*0.87f);
            break;
    }
    return posicion;
    //Boton* nivel=
    
}


void NivelItem::drawStars(int stars){
    
    Texture2D * estrellaTexture=Director::getInstance()->getTextureCache()->addImage("ui/levels/star.png");
    
    Point estrella1;
    Point estrella2;
    Point estrella3;
    float scale=1.f;
    
    float height=getContentSize().height;
    float width=getContentSize().width;
    
    float widthEstrella=estrellaTexture->getContentSize().width;
    
    switch(mNumNivel)
    {
        case 1:
            scale=1.f;
            estrella2=Point(width*0.5f,height*0.3f);
            estrella1=Point((width*0.5f-widthEstrella*scale*0.8f) ,height*0.35f);
            estrella3=Point((width*0.5f+widthEstrella*scale*0.8f) ,height*0.35f);
            break;
        case 2:
            scale=0.6f;
            estrella2=Point(width*0.5f,height*0.25f);
            estrella1=Point((width*0.5f-widthEstrella*scale*0.9f) ,height*0.25f);
            estrella3=Point((width*0.5f+widthEstrella*scale*0.9f) ,height*0.25f);
            break;
        case 3:
            scale=0.8f;
            estrella2=Point(width*0.5f,height*0.9f);
            estrella1=Point((width*0.5f-widthEstrella*scale*0.8f) ,height*0.82f);
            estrella3=Point((width*0.5f+widthEstrella*scale*0.8f) ,height*0.82f);
            break;
        case 4:
            scale=0.8f;
            estrella2=Point(width*0.5f,height*0.2f);
            estrella1=Point((width*0.5f-widthEstrella*scale*0.8f) ,height*0.25f);
            estrella3=Point((width*0.5f+widthEstrella*scale*0.8f) ,height*0.25f);
            break;
        case 5:
            scale=0.9f;
            estrella2=Point(width*0.5f,height*1.10f);
            estrella1=Point((width*0.5f-widthEstrella*scale*0.9f) ,height*1.0f);
            estrella3=Point((width*0.5f+widthEstrella*scale*0.9f) ,height*1.0f);
            break;
            
        case 6:
            scale=0.7f;
            estrella2=Point(width*0.5f,height*0.2f);
            estrella1=Point((width*0.5f-widthEstrella*scale*0.8f) ,height*0.25f);
            estrella3=Point((width*0.5f+widthEstrella*scale*0.8f) ,height*0.25f);
            break;
        case 7:
            scale=0.9f;
            estrella2=Point(width*0.5f,height);
            estrella1=Point((width*0.5f-widthEstrella*scale*0.8f) ,height*0.9f);
            estrella3=Point((width*0.5f+widthEstrella*scale*0.8f) ,height*0.9f);
            break;
        case 8:
            scale=0.8f;
            estrella2=Point(width*0.5f,height*0.9f);
            estrella1=Point((width*0.5f-widthEstrella*scale*0.8f) ,height*0.82f);
            estrella3=Point((width*0.5f+widthEstrella*scale*0.8f) ,height*0.82f);
            break;
        case 9:
            scale=0.7f;
            estrella2=Point(width*0.5f,height*0.20f);
            estrella1=Point((width*0.5f-widthEstrella*scale*0.9f) ,height*0.25f);
            estrella3=Point((width*0.5f+widthEstrella*scale*0.9f) ,height*0.25f);
            break;
        case 10:
            scale=0.9f;
            estrella2=Point(width*0.5f,height*1.10f);
            estrella1=Point((width*0.5f-widthEstrella*scale*0.9f) ,height*1.0f);
            estrella3=Point((width*0.5f+widthEstrella*scale*0.9f) ,height*1.0f);
            break;
        case 11:
            scale=0.6f;
            estrella2=Point(width*0.5f,height*0.25f);
            estrella1=Point((width*0.5f-widthEstrella*scale*0.9f) ,height*0.25f);
            estrella3=Point((width*0.5f+widthEstrella*scale*0.9f) ,height*0.25f);
            break;

        case 12:
            scale=0.9f;
            estrella2=Point(width*0.5f,height);
            estrella1=Point((width*0.5f-widthEstrella*scale*0.8f) ,height*0.9f);
            estrella3=Point((width*0.5f+widthEstrella*scale*0.8f) ,height*0.9f);
            break;
        case 13:
            scale=0.7f;
            estrella2=Point(width*0.5f,height*0.2f);
            estrella1=Point((width*0.5f-widthEstrella*scale*0.7f) ,height*0.25f);
            estrella3=Point((width*0.5f+widthEstrella*scale*0.7f) ,height*0.25f);
            break;
        case 14:
            scale=0.9f;
            estrella2=Point(width*0.5f,height*1.f);
            estrella1=Point((width*0.5f-widthEstrella*scale*0.9f) ,height*0.9f);
            estrella3=Point((width*0.5f+widthEstrella*scale*0.9f) ,height*0.9f);
            break;
            
    }
    Sprite* estrella1Sprite=Sprite::createWithTexture(estrellaTexture);
    estrella1Sprite->setPosition(estrella1);
    Sprite* estrella2Sprite=Sprite::createWithTexture(estrellaTexture);
    estrella2Sprite->setPosition(estrella2);
    Sprite* estrella3Sprite=Sprite::createWithTexture(estrellaTexture);
    estrella3Sprite->setPosition(estrella3);

    estrella1Sprite->setScale(scale);
    estrella2Sprite->setScale(scale);
    estrella3Sprite->setScale(scale);
    
    if(stars>=1){
         this->addChild(estrella1Sprite);
    }
    if(stars>=2){
         this->addChild(estrella2Sprite,2);
    }
    if(stars==3){
        this->addChild(estrella3Sprite);
    }
}




void NivelItem::activo(){
    if(!mJugable){
        this->setColor(Color3B(80,80,80));
    }else{
        Boton::activo();
    }
    
}


void NivelItem::inactivo()
{
    if(!mJugable){
        this->setColor(Color3B(80,80,80));
    }else{
        this->setColor(Color3B(255,255,255));
    }
}

void NivelItem::onTouchEnded(Touch* touch, Event* event)
{
    CCLOG("NivelItem %i",mNumNivel);
    Variables::LEVELS_NIVEL_CLICKADO=this->mNumNivel;
    if(!mJugable){
        this->runAction(Sequence::create(RotateTo::create(0.2f, -10), RotateTo::create(0.2f, 10),RotateTo::create(0.2f,0),NULL));
    }
    Boton::onTouchEnded(touch, event);
    if(!mJugable){
        this->setColor(Color3B(100,100,100));
    }
    
    
    
    
}



int NivelItem::getElixiresRequeridos(int nivel){
    return LogicSQLHelper::getInstance().getElixiresRequeridosNivelLogica(nivel);
}

