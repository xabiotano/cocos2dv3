#include "SandClockScene.h"
#include "SimpleAudioEngine.h"
#include "Variables.h"
using namespace cocos2d;
using namespace CocosDenshion;

 static float  mScaleLeftClock=0.5f;
 static float  mScaleRightClock=0.7f;
    static const int CAPACIDAD_IZQUIERDA=7;
    static const int CAPACIDAD_DERECHA=11;

Scene* SandClockScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    
    // 'layer' is an autorelease object
    SandClockScene *layer = SandClockScene::create();
  
    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

SandClockScene::~SandClockScene(void){
    
    CC_SAFE_RELEASE(mListaPasos);
  
}
// on "init" you need to initialize your instance
bool SandClockScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !BaseScene::init() )
    {
        return false;
    }

    mNombrePantalla="SandClockScene";
    mTime=0;
    mSize=Director::getInstance()->getWinSize();
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    Sprite* mFondo = Sprite::create("sandclock/fondo.png");
    // position the sprite on the center of the screen
    mFondo->setPosition(Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    
    this->addChild(mFondo, 0);
    float scalex=Director::getInstance()->getVisibleSize().width/mFondo->getContentSize().width;
    float scaley=Director::getInstance()->getVisibleSize().height/mFondo->getContentSize().height;
    if(scalex>scaley){
        scaley=scalex;
    }
    mFondo->setScale(scaley);
    
    Director::getInstance()->setContentScaleFactor(960/visibleSize.width);
  

    Sprite* mesa=Sprite::create("sandclock/mesa.png");
    mesa->setPosition(Point(mSize.width/2,mSize.height*0.4f));
    this->addChild(mesa);

    
    mSandClockLeft=  SandClock::create(this, menu_selector(SandClockScene::onClickSandClockLeft),7,mScaleLeftClock);
    mSandClockLeft->setPosition(Point(mSize.width*0.3f,mSize.height*0.5f));
    this->addChild(mSandClockLeft);
    
    mSandClockRight= SandClock::create(this, menu_selector(SandClockScene::onClickSandClockRight),11,mScaleRightClock);
    mSandClockRight->setPosition(Point(mSize.width*0.7f,mSize.height*0.55f));
    this->addChild(mSandClockRight);
    
    
    mBoiler= Boton::createBoton(this,menu_selector(SandClockScene::botonClockButton),"sandclock/boiler.png");
    mBoiler->setPosition(Point(mSize.width/2,mSize.height/2));
    this->addChild(mBoiler);
    
    mTimeLabel=Label::createWithTTF("0 /15", CCGetFont(), 70);/*, Size(0, 100), kCCTextAlignmentLeft,kCCVerticalTextAlignmentCenter);*/
    
    mTimeLabel->setHeight(100);
    mTimeLabel->setHorizontalAlignment(TextHAlignment::LEFT);
    mTimeLabel->setVerticalAlignment(TextVAlignment::CENTER);
    mTimeLabel->enableOutline(Color4B(0,0,0,255),2);
    mTimeLabel->setPosition(Point(mVisibleSize.width*0.52f ,mVisibleSize.height*0.85f));
    this->addChild(mTimeLabel,1);
   
    Sprite* mReloj=Sprite::create("sandclock/reloj.png");
    mTimeLabel->addChild(mReloj);
    mReloj->setPosition(Point(-mTimeLabel->getContentSize().width/2 , mTimeLabel->getContentSize().height/2));
    mReloj->setScale(0.5f);
    
    mTextoHelp=std::string(LanguageManager::getInstance()->getStringForKey("HELP_SANDCLOCK", "HELP_SANDCLOCK"));
    mRetryVisibleDuringGame=true;
    
    return true;
}


void SandClockScene::iniciarScene(){
    mTime=0;
    char temp[200];
    sprintf(temp, "%i /15",mTime);
    mTimeLabel->setString(temp);
    
    if(mSandClockLeft!=NULL){
        mSandClockLeft->removeFromParent();
        mSandClockLeft=NULL;
    }
    if(mSandClockRight!=NULL){
        mSandClockRight->removeFromParent();
        mSandClockRight=NULL;
    }
    mSandClockLeft=  SandClock::create(this, menu_selector(SandClockScene::onClickSandClockLeft),7,mScaleLeftClock);
    mSandClockLeft->setPosition(Point(mSize.width*0.3f,mSize.height*0.5f));
    this->addChild(mSandClockLeft);
    
    mSandClockRight= SandClock::create(this, menu_selector(SandClockScene::onClickSandClockRight),11,mScaleRightClock);
    mSandClockRight->setPosition(Point(mSize.width*0.7f,mSize.height*0.55f));
    this->addChild(mSandClockRight);
    
    
   

}


void SandClockScene::boilingEffect(bool visible)
{
    if(mParticle){
        this->removeChild(mParticle, true);
    }
    if(visible){
        mParticle = ParticleSystemQuad::create("sandclock/ebullicion.plist");
        mParticle->setPosition(Point(mBoiler->getPositionX(),mBoiler->getPositionY()+mBoiler->getContentSize().height*0.5f ));
        this->addChild(mParticle,1);
    }
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/boiling.mp3",true);
}


void SandClockScene::onClickSandClockLeft(Ref* sender)
{
    CCLOG("left");
    mSandClockLeft->toggleVerticalPosition();
}

void SandClockScene::onClickSandClockRight(Ref* sender)
{
    CCLOG("right");
    mSandClockRight->toggleVerticalPosition();
}


void SandClockScene::pararTiempo(float capacidad,float tiempogastado)
{
    boilingEffect(false);
    CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
    if(capacidad==CAPACIDAD_IZQUIERDA)
    {
        //es el reloj izquierdo el que se ha parado
         mSandClockRight->stopTime(tiempogastado);
    }
    else if (capacidad==CAPACIDAD_DERECHA)
    {
         mSandClockLeft->stopTime(tiempogastado);
    }
    mBoiler->setEnabled(true);
    //mClockPalos->stopAllActions();
   // mBotonClock->setVisible(true);
    
    
}

void SandClockScene::botonClockButton(Ref* sender){
    botonClock(false);
}

void SandClockScene::botonClock(bool vieneDeHint)
{
    if(mSituacionHint && !vieneDeHint){
        return;
    }
    
    CCLOG("boton clock");
    if(mSandClockLeft->getTimeLeft()<=0.f && mSandClockRight->getTimeLeft()<=0.f)
    {
        CCLOG("No puedes empezar si no hay reloj");
        LayerInfo* layer= LayerInfo::mostrar(LanguageManager::getInstance()->getStringForKey("SANDCLOCK_GIRAR_RELOJES"));
        Sprite* fondo=(Sprite*) layer->getCanvas();
        Sprite* reloj=Sprite::create("sandclock/sanclock.png");
        fondo->addChild(reloj);
        reloj->setScale(0.2f);
        reloj->runAction(RepeatForever::create(Sequence::create(RotateBy::create(1.f, 180.f),RotateBy::create(1.f, 0.f),RotateBy::create(1.f,0.f),RotateBy::create(1.f, 0.f),NULL)));
        reloj->setPosition(Point(fondo->getContentSize().width/2,fondo->getContentSize().height*0.20f));
        return;
    }
   boilingEffect(true);
    minTime=0.f;
   
    if(mSandClockLeft->getTimeLeft()==0.f){
         minTime=mSandClockRight->getTimeLeft();
    }
    else if(mSandClockRight->getTimeLeft()==0.f){
          minTime=mSandClockLeft->getTimeLeft();
    }
    
    else if(mSandClockLeft->getTimeLeft()!=0.f && mSandClockLeft->getTimeLeft()<=mSandClockRight->getTimeLeft()){
        minTime=mSandClockLeft->getTimeLeft();
    }
    else if(mSandClockRight->getTimeLeft()!=0.f && mSandClockRight->getTimeLeft()<=mSandClockLeft->getTimeLeft()){
         minTime=mSandClockRight->getTimeLeft();
    }
     mCuentaAtrasSchedule=schedule_selector(SandClockScene::cuentaAtras);
    this->schedule(mCuentaAtrasSchedule,1.f);
    
    
    mSandClockLeft->startTime();
    mSandClockRight->startTime();
    mBoiler->setEnabled(false);
    //mBotonClock->setVisible(false);
    //mClockPalos->runAction(RepeatForever::create(RotateBy::create(0.5f, 180.f)));
    
}


void SandClockScene::cuentaAtras(float dt)
{
    minTime-=1.f;
    mTime+=1;
    if(minTime<=0.f){
        this->unschedule(mCuentaAtrasSchedule);
        mCuentaAtrasSchedule=NULL;
        if(mTime==15)
        {
            Success();
        }
        else if(mTime>15){
            GameOver();
        }
    }
   
    if(mTime>15){
        mTimeLabel->setColor(Color3B(200,100,100));
    }
    char temp[200];
    sprintf(temp, "%i /15",mTime);
    mTimeLabel->setString(temp);
    
   
}






void SandClockScene::GameOver()
{
    mTextoGameOver=std::string(LanguageManager::getInstance()->getStringForKey("SANDCLOCK_GAMEOVER","You have to boil the eggs exactly 15 minutes!"));
    this->unscheduleAllCallbacks();
    this->stopAllActions();
    BaseScene::GameOver();
    CCLOG("GAME OVER");
}

void SandClockScene::Success()
{
    BaseScene::Success();
    CCLOG("Success");
}



void SandClockScene::TutorialStart(Ref* sender){
    if(!mSituacionTutorial){
        BaseScene::TutorialStart(NULL);
        mHand=Sprite::create("ui/hand.png");
        mHand->setPosition(Point(0,0));
        addChild(mHand);
        mSituacionTutorial=true;
    }
    CCLOG("TutorialStart %i",mStepTutorial);
    switch(mStepTutorial){
        case 0:
            LayerTutorial::mostrar(LanguageManager::getInstance()->getStringForKey("SANDCLOCK_TUTORIAL0","You have 2 sandglasses, you can turn unlimited times. You have to use them to calculate 15 minutes of boiling"));
            break;
        case 1:
            LayerTutorial::ocultar(NULL);
            LayerTutorial::mostrar(LanguageManager::getInstance()->getStringForKey("SANDCLOCK_TUTORIAL1","The first of the sandglasses lasts 7 minutes."))->setSprite(Sprite::create("sandclock/sanclock.png"));
            mHand->runAction(MoveTo::create(0.5f, mSandClockLeft->getPosition()));
            break;
        case 2:
            LayerTutorial::ocultar(NULL);
            (LayerTutorial::mostrar(LanguageManager::getInstance()->getStringForKey("SANDCLOCK_TUTORIAL2","The second one lasts 11 minutes.")))->setSprite(Sprite::create("sandclock/sanclock.png"));
            mHand->runAction(MoveTo::create(0.5f, mSandClockRight->getPosition()));
            break;
        case 3:
            LayerTutorial::ocultar(NULL);
            (LayerTutorial::mostrar(LanguageManager::getInstance()->getStringForKey("SANDCLOCK_TUTORIAL3","Click the boiler when you want to start boiling. Once clicked, sandglasses also start to run")))->setSprite(Sprite::create("sandclock/boiler.png"));
             mHand->runAction(MoveTo::create(0.5f, mBoiler->getPosition()));
            break;
        case 4:
            LayerTutorial::ocultar(NULL);
            LayerTutorial::mostrar(LanguageManager::getInstance()->getStringForKey("SANDCLOCK_TUTORIAL4","When a sandglass ends, the boiling is paused.Once paused, you can turn the sanglass/es and continue boiling"));
            break;
       
       
    }
    if(mStepTutorial>4){
        BaseScene::TutorialStop(NULL);
    }
}


void SandClockScene::TutorialNext(Ref* sender){
    mStepTutorial++;;
    TutorialStart(NULL);
}


void SandClockScene::Retry(Ref* sender){
    Scene* scene=SandClockScene::scene();
    Director *pDirector = Director::getInstance();
    pDirector->replaceScene(TransitionCrossFade::create(0.5f, scene));
    
}

void SandClockScene::Hint(Ref* sender){
    BaseScene::Hint(NULL);
}


void SandClockScene::HintStart(Ref* sender){
    
    if(mCuentaAtrasSchedule!=NULL)
    {
        CCLOG("Reloj is boiling");
        return;
    }
    switch(mHintStep){
        case 0:
            LayerInfo::mostrar(LanguageManager::getInstance()->getStringForKey("SANDCLOCK_HINT_0"));
            break;
        case 1:
            LayerInfo::ocultar(NULL);
            botonClock(true);
            break;
        case 2:
            LayerInfo::mostrar(LanguageManager::getInstance()->getStringForKey("SANDCLOCK_HINT_2"));
            break;
        case 3:
            LayerInfo::ocultar(NULL);
            onClickSandClockLeft(NULL);
            break;
        case 4:
            LayerInfo::ocultar(NULL);
            botonClock(true);
            break;
        case 5:
            LayerInfo::mostrar(LanguageManager::getInstance()->getStringForKey("SANDCLOCK_HINT_5"));
            break;
        case 6:
            LayerInfo::ocultar(NULL);
            onClickSandClockLeft(NULL);
            break;
        case 7:
            botonClock(true);
            break;
    }
    if(mHintStep>=8){
        BaseScene::HintStop(NULL);
    }
    mHintStep++;



}

void SandClockScene::HintStartTemp(float dt){
    HintStart(NULL);
}
void SandClockScene::HintCallback(bool accepted){
    
    if(accepted){
        CCLOG("MOSTRAR");
        BaseScene::HintCallback(accepted);
        iniciarScene();
        mSituacionHint=true;
        mHintStep=0;
        LayerHintOk::mostrar(true);
        mScheudleHint=schedule_selector(SandClockScene::HintStartTemp);
        this->schedule(mScheudleHint,3.f);
    }
}






void SandClockScene::onExit()
{
     CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
     BaseScene::onExit();
    
}


void SandClockScene::onEnterTransitionDidFinish(){
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sounds/volcan/forest.mp3", true);
    BaseScene::onEnterTransitionDidFinish();
}
