#include "InicioScene.h"
#include "BaseScene.h"
#include "TipoNivelTypes.h"
#include "Levels.h"

#include "Levelsv2.h"
#include "SimpleAudioEngine.h"
#include "Variables.h"
#include "widgets/LanguageManager.h"

USING_NS_CC;

using namespace sdkbox;

template <typename T> std::string Inicio::tostr(const T& t) { std::ostringstream os; os<<t; return os.str(); }


Scene* Inicio::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = Inicio::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}





// on "init" you need to initialize your instance
bool Inicio::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }

    Director::getInstance()->getTextureCache()->removeUnusedTextures();
   
    mVisibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    if(!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()){
        CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sounds/hawaii.mp3",true);
        CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(0.2f);
    }
  
   
    Sprite* mFondo = Sprite::create("inicio/background.png");
    // position the sprite on the center of the screen
    mFondo->setPosition(Point(mVisibleSize.width/2 + origin.x, mVisibleSize.height/2 + origin.y));
    
    this->addChild(mFondo, 0);
    float scalex=Director::getInstance()->getVisibleSize().width/mFondo->getContentSize().width;
    float scaley=Director::getInstance()->getVisibleSize().height/mFondo->getContentSize().height;
    if(scalex>scaley){
        scaley=scalex;
    }
    mFondo->setScale(scaley);
    Director::getInstance()->setContentScaleFactor(960/mVisibleSize.width);
    
    mLeft=Boton::createBoton(this, CC_MENU_SELECTOR(Inicio::irALevelsV2), "inicio/izquierda.png");
    mRight=Boton::createBoton(this, CC_MENU_SELECTOR(Inicio::irALogic), "inicio/derecha.png");

    mLeft->setPosition(Point(-mLeft->getContentSize().width/2, mVisibleSize.height*0.4f ));
    mRight->setPosition(Point(mVisibleSize.width+ mRight->getContentSize().width/2, mVisibleSize.height*0.4f ));
    
    
    
       
  
    this->addChild(mRight);
    this->addChild(mLeft);
    
    
    
    
    std::string logoImagen;
    logoImagen="inicio/logo.png";
   
    
    
    LanguageType curLanguage = Application::getInstance()->getCurrentLanguage();
    switch (curLanguage) {
            
        case LanguageType::RUSSIAN:
            logoImagen = "inicio/logo_ru.png";
            break;
        case LanguageType::KOREAN:
            logoImagen = "inicio/logo_ko.png";
            break;
        case LanguageType::GERMAN:
            logoImagen = "inicio/logo_de.png";
            break;
        default:
            logoImagen = "inicio/logo.png";
            break;
        
    }

     mLogo=Sprite::create(logoImagen.c_str());
    
    
    
    mLogo->setPosition(Point(mVisibleSize.width/2,mVisibleSize.height+ mLogo->getContentSize().height/2));
    this->addChild(mLogo);
    
    
    return true;
}


void Inicio::animarLogo(float dt){
    
    mLogo->runAction(EaseInOut::create(JumpTo::create(0.5f, Point(mVisibleSize.width/2,mVisibleSize.height*0.8f), -mLogo->getContentSize().height, 1),3));

   

                   
}


void Inicio::irALogic(Ref* sender){
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/ui/elegirnivel.mp3");
    Scene* scene=Levels::createScene();
    Director::getInstance()->replaceScene(TransitionCrossFade::create(0.5f, scene));
}

void Inicio::irALevelsV2(Ref* sender){
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/ui/elegirnivel.mp3");
    Scene* scene=Levelsv2::scene();
    Director::getInstance()->replaceScene(TransitionCrossFade::create(0.5f, scene));
}




void Inicio::onEnterTransitionDidFinish(){
   
    mLeft->runAction(EaseInOut::create(MoveTo::create(0.6f, Point(mVisibleSize.width*0.25 ,mVisibleSize.height*0.4)), 3));
    mRight->runAction(EaseInOut::create(MoveTo::create(0.6f, Point(mVisibleSize.width*0.75,mVisibleSize.height*0.4)), 3));

    
    this->scheduleOnce(schedule_selector(Inicio::animarLogo), 0.7f);
    this->scheduleOnce(schedule_selector(Inicio::cobrarRegalos), 0.7f);
    
    
    if(LogicSQLHelper::getInstance().getRegaloVoto() && !LogicSQLHelper::getInstance().getRegaloGastado()){
        LogicSQLHelper::getInstance().setRegaloGastado(true);
        int numeroRegalo=LogicSQLHelper::getInstance().getRegaloNumero();
        LogicSQLHelper::getInstance().addElixiresUsuario(numeroRegalo);
        LogicSQLHelper::getInstance().setRegaloVoto(false, -1);
        MessageBox(LanguageManager::getInstance()->getStringForKey("RATE_PREMIO_CONGRATS_CONTENT", "RATE_PREMIO_CONGRATS_CONTENT").c_str(), LanguageManager::getInstance()->getStringForKey("RATE_PREMIO_CONGRATS_TITLE", "RATE_PREMIO_CONGRATS_TITLE").c_str());
        //vuelve de votar
    }
    
    
  
    
    
}

void Inicio::onKeyReleased(EventKeyboard::KeyCode keyCode, Event *event) {
    if(keyCode == EventKeyboard::KeyCode::KEY_ESCAPE) {
         Director::getInstance()->end();
    }
}


void Inicio::cobrarRegalos(float dt){
    
    
    
}






