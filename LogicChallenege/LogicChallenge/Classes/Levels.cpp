#include "Levels.h"
#include "SimpleAudioEngine.h"
#include "widgets/NivelItem.h"
#include "Variables.h"
#include "layers/LayerHelp.h"
#include "InicioScene.h"

#include "SandClock/SandClockScene.h"
#include "fivefrogs/FiveFrogs.h"
#include "twofrogs/TwoFrogs.h"
#include "goatlettucewolf/GoatLettuceWolfScene.h"
#include "hanoi/DisksScene.h"
#include "canibals/CanibalsScene.h"
#include "volcan/VolcanScene.h"
#include "bridge/BridgeScene.h"
#include "botellas/BotellasScene.h"
#include "sacks/SacksScene.h"
#include "series/SeriesScene.h"
#include "psico/PsicoScene.h"
#include "elevators/ElevatorsScene.h"
#include "hardbridge/HardBridgeScene.h"
#include "lightsout/LightsOutScene.h"
#include "balanzas/BalanzasScene.h"
#include "layers/LayerHintOk.h"
#include "layers/LayerNecesitasItems.h"
#include "layers/LayerShop.h"
#include "helpers/LogicSQLHelper.h"
#include "layers/LayerNecesitasSuperar.h"
#include "layers/LayerPuedesVotar.h"




using namespace cocos2d;
using namespace CocosDenshion;


template <typename T> std::string Levels::tostr(const T& t) { std::ostringstream os; os<<t; return os.str(); }




Scene* Levels::createScene()
{
    auto scene = Scene::create();
    auto layer = Levels::create();
    scene->addChild(layer);
    return scene;
}





bool Levels::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !LevelsBase::initWithColor(Color4B(0,0,0,0)))
    {
        return false;
    }
    mVisibleSize= Director::getInstance()->getVisibleSize();
    Director::getInstance()->setContentScaleFactor(960/mVisibleSize.width);
    
    mSlidingLayer=CCSlidingLayer::create(Vertically, Size(mVisibleSize.width,1), Rect(0,0,mVisibleSize.width,mVisibleSize.height), Color4B(0,0,0,0));
    addChild(mSlidingLayer);
    
    mSlidingLayer->setHorizontalMargins(0.f);
    mSlidingLayer->setVerticalMargins(0.f);
    
    mFondo=Sprite::create("level_selector.png");
    mSlidingLayer->addChildWithSize(mFondo,mFondo->getContentSize(),kAlignmentLeft);
    
    mBack=Boton::createBoton(this, menu_selector(Levels::Back), "ui/back.png");
    mBack->setPosition(Point(mVisibleSize.width*0.1f,mVisibleSize.height*0.9f));
    this->addChild(mBack);
    mBack->setSound("sounds/ui/click1.mp3");
    
    pintarNiveles();
    this->setKeypadEnabled(true);
    
    return true;
}




void Levels::Back(Ref* sender){
    Scene* scena= Inicio::createScene();
    Director::getInstance()->replaceScene(TransitionCrossFade::create(0.5f, scena));
}






void Levels::pintarNiveles()
{
    for(int i=1;i<=14;i++){
        NivelItem* nivel=NivelItem::create(mFondo, menu_selector(Levels::onClickNivel), i,kNivelLogica);
        mFondo->addChild(nivel);
        
        if(BaseScene::getNivelJugable(i , kNivelLogica)  && !BaseScene::getNivelJugable((i+1), kNivelLogica)){
            nivel->runAction(RepeatForever::create(Sequence::createWithTwoActions(ScaleTo::create(0.5f,1.1f),ScaleTo::create(0.5f, 1.f))));
            mSlidingLayer->setPositionY(-nivel->getPositionY()+ mVisibleSize.height/2);
            
        }
    }
}


void Levels::onClickNivel(Ref* sender)
{
    bool bloqueadoPorElixir=false;
    int elixires=LogicSQLHelper::getInstance().getElixiresGanados();
    int elixiresRequeridos=LogicSQLHelper::getElixiresRequeridosNivelLogica(Variables::LEVELS_NIVEL_CLICKADO);
    if(elixires<elixiresRequeridos){
        bloqueadoPorElixir=true;
    }else{
        if(BaseScene::getNivelJugable(Variables::LEVELS_NIVEL_CLICKADO, kNivelLogica)){
             GoToScene(Variables::LEVELS_NIVEL_CLICKADO);
        }else{
            LayerNecesitasSuperar::mostrar();
        }
        return;
    }
    if(bloqueadoPorElixir){
        
        LayerNecesitasItems::mostrar(Variables::LEVELS_NIVEL_CLICKADO );
    }
    
    
}




bool Levels::GoToScene(int NUM_SCENE)
{
    Variables::CURRENT_SCENE= Variables::LEVELS_NIVEL_CLICKADO;
    Scene* scene;
    switch(NUM_SCENE)
    {
        case Variables::TWO_FROGS_SCENE:
            scene=TwoFrogs::scene();
            break;
        case Variables::SANDCLOCK_SCENE:
            scene=SandClockScene::scene();
            break;
        case Variables::FIVEFROGS_SCENE:
            scene=FiveFrogs::scene();
            break;
        case Variables::GOATLETTUCEWOLF_SCENE:
            scene=GoatLettuceWolf::scene();
            break;
        case Variables::HANOI_LEVEL:
            scene=DisksScene::scene();
            break;
        case Variables::CANIBALS_LEVEL:
            scene=CanibalsScene::scene();
            break;
        case Variables::VOLCAN_LEVEL:
            scene=VolcanScene::scene();
            break;
        case Variables::BRIDGE_LEVEL:
            scene=BridgeScene::scene();
            break;
        case Variables::BOTELLAS_LEVEL:
            scene=BotellasScene::scene();
            break;
            
        case Variables::LEVELS_SCENE:
            scene=Levels::createScene();
            break;
        case Variables::SACKS_LEVEL:
            scene=SacksScene::scene();
            break;
        case Variables::SERIES_LEVEL:
            scene=SeriesScene::scene();
            break;
        case Variables::PSICO_LEVEL:
            scene=PsicoScene::scene();
            break;
        case Variables::HARDBRIDGE_LEVEL:
            scene= HardBridgeScene::scene();
            break;
        case Variables::LIGHTSOUT_LEVEL:
            scene= LightsOut::scene();
            break;
        case Variables::ELEVATORS_LEVEL:
            scene=ElevatorsScene::scene();
            break;
        case Variables::SCALES_LEVEL:
            scene=BalanzasScene::scene();
            break;
        default:
            scene=SandClockScene::scene();
            break;
            
    }
   
    Director *pDirector = Director::getInstance();
    pDirector->replaceScene( TransitionCrossFade::create(0.5f, scene));

    return true;
}



/*
void Levels::onKeyBackClicked()
{
    
    if(Director::getInstance()->getRunningScene()->getChildByTag(LayerHelp::TAG_HELP)){
        LayerHelp::ocultar(NULL);
    }else if(Director::getInstance()->getRunningScene()->getChildByTag(LayerInfo::TAG_INFO)){
        LayerInfo::ocultar(NULL);
    }else if(Director::getInstance()->getRunningScene()->getChildByTag(LayerExplicarItems::TAG_EXPLICACION_ITEM)){
        LayerExplicarItems::ocultar(NULL);
    }else{
        Back(NULL);
    }
}
*/
void Levels::onKeyReleased(EventKeyboard::KeyCode keyCode, Event *event) {
    if(keyCode == EventKeyboard::KeyCode::KEY_ESCAPE) {
        if(Director::getInstance()->getRunningScene()->getChildByTag(LayerHelp::TAG_HELP)){
            LayerHelp::ocultar(NULL);
        }else if(Director::getInstance()->getRunningScene()->getChildByTag(LayerInfo::TAG_INFO)){
            LayerInfo::ocultar(NULL);
        }else if(Director::getInstance()->getRunningScene()->getChildByTag(LayerExplicarItems::TAG_EXPLICACION_ITEM)){
            LayerExplicarItems::ocultar(NULL);
        }else{
            Back(NULL);
        }

    }
}




void Levels::onEnterTransitionDidFinish(){
    if(!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()){
        CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sounds/hawaii.mp3",true);
    }
    mMostrarPremio=schedule_selector(Levels::mostrarPremio);
    this->scheduleOnce(mMostrarPremio,0.7f);
                       
    LevelsBase::onEnterTransitionDidFinish();
    
}


void Levels::mostrarPremio(float dt){
    //int elixires=LogicSQLHelper::getInstance().getElixiresGanados();
   // int elixiresRequeridos=LogicSQLHelper::getElixiresRequeridosNivelLogica(Variables::LEVELS_NIVEL_CLICKADO);
    if(!LogicSQLHelper::getInstance().estaElJuegoCompleto()
       && !LogicSQLHelper::getInstance().getRegaloGastado()
       && LogicSQLHelper::getInstance().getNivelSuperadoLogica(2)){
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
      LayerPuedesVotar::mostrar(60/*(elixiresRequeridos - elixires)*/);
#else
       //LayerPuedesVotar::mostrar(150/*(elixiresRequeridos - elixires)*/);
#endif
        mMostrarPremio=NULL;
        
        
    }
}

void Levels::onExitTransitionDidStart()
{
    this->unschedule(mMostrarPremio);
    LevelsBase::onExitTransitionDidStart();
}


                       


