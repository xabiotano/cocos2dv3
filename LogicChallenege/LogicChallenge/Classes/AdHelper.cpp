#include "AdHelper.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "objc/ObjCCalls.h"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "jni/InterfaceJNI.h"
#endif


using namespace cocos2d;

AdHelper* AdHelper::_adHelperinstancia=NULL;


AdHelper::AdHelper()
{
    mInteristialCached=false;
    mShowIntersitialAdWhenLoaded=false;
   
}

AdHelper* AdHelper::getInstance()
{
    if(!_adHelperinstancia)
    {
        CCLOG("new AdHelper()");
        _adHelperinstancia=new AdHelper();
        _adHelperinstancia->onEnter();
        
    }
    return _adHelperinstancia;
}





void AdHelper::showIntersitial()
{
     CCLOG("AdHelper::showIntersitial");
    if(AdHelper::getInstance()->getIntersitialCached())
    {
        CCLOG("AdHelper::showIntersitial - auncio cacheado");
        #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        ObjCCalls::showIntersitial();
        #elif(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        InterfaceJNI::showIntersitial();
        #endif
  
    }else{
        CCLOG("AdHelper::showIntersitial - auncio no cacheado aun");
        AdHelper::getInstance()->setIntersitialAdWhenLoaded(true);
        #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        ObjCCalls::cacheIntersitial();
        #elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        InterfaceJNI::cacheIntersitial();
        #endif
    }
     
}


void AdHelper::showIntersitialWithMaxDelay(float maxDelay)
{
    CallFuncN *callFunc= CallFuncN::create( CC_CALLBACK_1(AdHelper::showIntersitialPrivate, this));
    float ran=1.f + CCRANDOM_0_1()* maxDelay;
    bool running=getInstance()->isRunning();
    
    getInstance()->runAction(Sequence::createWithTwoActions(DelayTime::create(ran),callFunc));
}

void AdHelper::showBanner()
{
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    ObjCCalls::showBanner();
    #elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    InterfaceJNI::showBanner();
    #endif

}

void AdHelper::hideBanner()
{
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    ObjCCalls::hideBanner();
    #elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    InterfaceJNI::hideBanner();

    #endif
}

void AdHelper::cacheIntersitial()
{
    if(!getInstance()->getIntersitialCached())
    {
        #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        ObjCCalls::cacheIntersitial();
        #elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        InterfaceJNI::cacheIntersitial();
        #endif
    }

    
}


void AdHelper::setIntersitialCached(bool cacheado)
{
    CCLOG("AdHelper::setIntersitialCached %i",cacheado);
    mInteristialCached=cacheado;
    if(mInteristialCached && mShowIntersitialAdWhenLoaded)
    {
        showIntersitial();
        mShowIntersitialAdWhenLoaded=false;
    }
}




bool AdHelper::getIntersitialCached()
{
    return mInteristialCached;

}


void AdHelper::setIntersitialAdWhenLoaded(bool value)
{
    mShowIntersitialAdWhenLoaded=value;
}


bool AdHelper::getIntersitialAdWhenLoaded()
{
    return mShowIntersitialAdWhenLoaded;
}


void AdHelper::interstitialDidAppear()
{
    CCLOG("AdHelper::interstitialDidAppear");
    //nada mas mostrar cacheo uno nuevo
    mInteristialCached=false;
    mShowIntersitialAdWhenLoaded=false;
    cacheIntersitial();
}

void AdHelper::showIntersitialPrivate(Node* sender)
{
    AdHelper::showIntersitial();

}






