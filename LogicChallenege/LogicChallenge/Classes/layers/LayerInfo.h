//
//  LayerInfo.h
//  trivial
//
//  Created by Xabier on 25/03/13.
//
//

#ifndef __trivial__LayerInfo__
#define __trivial__LayerInfo__

#include <iostream>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "LayerBase.h"


USING_NS_CC;

USING_NS_CC_EXT;

class LayerInfo:  public LayerBase
{
    public:
    
    static LayerInfo* initLayer(std::string texto);
    Rect rect();
   
    bool init() override;
    // 'layer' is an autorelease object
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(LayerInfo);
    
    void dibujarContent();
    
    static void ocultar(Ref* pSender);
    static LayerInfo* mostrar(std::string texto);

    void onEnter() override;

    
    static int TAG_INFO;
    void setTexto(std::string texto);
    Node* getCanvas();
    Label* crearTexto(std::string texto);
    void setNoOcultable();
    void setSprite(Sprite* imagen);
    void setBoton(Boton* boton1);
    void set2Boton(Boton* boton1,Boton *boton2);
    void onKeyBackClicked();
private:
    void close(Ref* sender);
    Label* mTexto;
    Boton* mClose;
    ScrollView* mScroll;
    std::string mTextoHelp;
    Sprite* mFondo;
    bool mOcultable;
    
    


};

#endif 
