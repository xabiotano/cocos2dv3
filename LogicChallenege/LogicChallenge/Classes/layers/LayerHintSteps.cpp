#include "LayerHintSteps.h"
#include "Variables.h"
#include "SimpleAudioEngine.h"
#include "PluginIAP/PluginIAP.h"
#include "../BaseScene.h"



LayerHintSteps* LayerHintSteps::initLayer(CCArray* listaPasos){
    LayerHintSteps *layer = LayerHintSteps::create();
    layer->dibujarContent(listaPasos);
    layer->mSwallowTouches=true;
    return layer;
    
}


// on "init" you need to initialize your instance
bool LayerHintSteps::init()
{
    //////////////////////////////
    // 1. super init first
    if (!LayerColor::initWithColor(Color4B(0,0,0,180))){
        return false;
    }
    
    return true;
}

void LayerHintSteps::dibujarContent(CCArray* listaPasos){
    
    mSize = Director::getInstance()->getWinSize();
    mFondo=Sprite::create("ui/hint_panel.png");
    mClose=Boton::createBoton(menu_selector(LayerHintSteps::close), "ui/close.png");
    mFondo->addChild(mClose,1);
    mClose->setPosition(Point(mFondo->getContentSize().width*0.95f,mFondo->getContentSize().height*0.9f));
    mClose->setScale(0.7f);
    
      
    
    
    mFondo->setPosition(Point(mSize.width/2,mSize.height/2));
    addChild(mFondo);

  
    
    Sprite* item1=Sprite::create("ui/item_hint_panel.png");
    mContenido=LayerColor::create(Color4B(0,0,0,0), mFondo->getContentSize().width*1.1f,item1->getContentSize().height* listaPasos->count() );
    
    Object* it = NULL;
    int i=1;
    CCARRAY_FOREACH(listaPasos, it)
    {
        Sprite *item = (Sprite*)it;
        mContenido->addChild(item);
        item->setPosition(Point(mContenido->getContentSize().width/2,mContenido->getContentSize().height-i*item1->getContentSize().height));
        i++;
    }

    
    
    
    //listaPasos
    
    mScroll=ScrollView::create();
    
    mScroll->setPosition(Point(- mFondo->getContentSize().width*0.05f ,10));
    mScroll->setBounceable(true);
    mScroll->retain();
    mScroll->setClippingToBounds(true);
    mScroll->setDirection(ScrollView::Direction::VERTICAL);
    mScroll->setContainer(mContenido);
    mScroll->setViewSize(Size(mSize.width,mSize.height*0.75f));
    mScroll->setContentOffset(Point(5.f, (mSize.height*0.75-mContenido->getContentSize().height)), true);
    mScroll->setDelegate(this);
    // mScroll->setPosition(Point(size.width/2,size.height/2));
    mFondo->addChild(mScroll,1);
    
    mScrollIndicator=Sprite::create("ui/scroll_indicator.png");
    mScrollIndicator->setPosition(Point(mFondo->getContentSize().width*0.99f,mFondo->getContentSize().height/2));
    mFondo->addChild(mScrollIndicator);
    mScrollIndicator->setScaleY(0.8f);
    mScrollIndicatorBullet=Sprite::create("ui/scroll_indicator_bullet.png");
    mScrollIndicator->addChild(mScrollIndicatorBullet);
    scrollViewDidScroll(mScroll);

    
}


void LayerHintSteps::close(Ref* sender){
     CCLOG("close");
    ocultar(NULL);
    BaseScene::getInstance()->HintStop(NULL);
}
LayerHintSteps* LayerHintSteps::mostrar(CCArray* listaPasos)
{
    LayerHintSteps* devolver=NULL;
    
    if(!Director::getInstance()->getRunningScene()){
        CCLOG("no hay running scene");
        return devolver;
    }
    if(Director::getInstance()->getRunningScene()->getChildByTag(LayerHintSteps::TAG_HINTSTEPS)==NULL){
        LayerHintSteps* layer= LayerHintSteps::initLayer(listaPasos);
        layer->setContentSize( Director::getInstance()->getWinSize());
        layer->setTag(LayerHintSteps::TAG_HINTSTEPS);
        Director::getInstance()->getRunningScene()->addChild(layer,10);
        devolver=layer;
        CCLOG("Layer hint steps creada");
    }
    else{
        devolver=(LayerHintSteps*)Director::getInstance()->getRunningScene()->getChildByTag(LayerHintSteps::TAG_HINTSTEPS);
        CCLOG("Layer  hint steps previamente mostrada");
    }
    return devolver;
}

void LayerHintSteps::ocultar(Ref* pSender)
{
    if( Director::getInstance()->getRunningScene()->getChildByTag(LayerHintSteps::TAG_HINTSTEPS)){
        Director::getInstance()->getRunningScene()->removeChildByTag(LayerHintSteps::TAG_HINTSTEPS,true);
    }
  
}





bool LayerHintSteps::EstaMostrado(){
     LayerHintSteps* devolver=(LayerHintSteps*)Director::getInstance()->getRunningScene()->getChildByTag(LayerHintSteps::TAG_HINTSTEPS);
    if(devolver!=NULL){
        return true;
    }
    else{
        return false;
    }
}


void LayerHintSteps::scrollViewDidScroll(ScrollView* view){
    
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    float y= (-view->getContentOffset().y)/view->getContainer()->getContentSize().height;
    CCLOG("%f / %f => %f",-view->getContentOffset().y,view->getContainer()->getContentSize().height,y);
    //0.10   0.3
    y=y/0.2f;
    CCLOG("%f",y);
    if(y<0.f ){
        y=0;
    }
    if(y>1.f){
        y=1.f;
    }
    mScrollIndicatorBullet->setPosition(Point(mScrollIndicatorBullet->getParent()->getContentSize().width*0.5f,mScrollIndicatorBullet->getParent()->getContentSize().height*y));
}
