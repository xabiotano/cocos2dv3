//
//  LayerInfo.cpp
//  trivial
//
//  Created by Xabier on 25/03/13.
//
//

#include "LayerInfo.h"
#include "Variables.h"
#include "SimpleAudioEngine.h"
#include "../widgets/CCGetFont.h"



int LayerInfo::TAG_INFO=12113;


LayerInfo* LayerInfo::initLayer(std::string texto){
    LayerInfo *layer = LayerInfo::create();
    layer->setTexto(texto);
    layer->dibujarContent();
    return layer;
    
}


// on "init" you need to initialize your instance
bool LayerInfo::init()
{
    //////////////////////////////
    // 1. super init first
    if (!LayerBase::initWithColor(Color4B(100,100,100,180))){
        return false;
    }
    mOcultable=true;
    this->setKeypadEnabled(true);

    return true;
}

void LayerInfo::dibujarContent(){
    
    mSize = Director::getInstance()->getWinSize();
    mFondo=Sprite::create("ui/info_panel.png");
    mTexto=Label::createWithTTF(mTextoHelp.c_str(),CCGetFont(), 20);
    mTexto->setWidth(mFondo->getContentSize().width*0.85f);
    mTexto->setHeight(mFondo->getContentSize().height*0.63f);
    mTexto->setOverflow(Label::Overflow::SHRINK);
    mTexto->setHorizontalAlignment(TextHAlignment::CENTER);
    mTexto->setVerticalAlignment(TextVAlignment::TOP);
    mTexto->setTextColor(Color4B(116,90,62,255));
    mTexto->enableOutline(Color4B(255,255,255,255),2);
    mTexto->setPosition(Point(mFondo->getContentSize().width*0.5f, mFondo->getContentSize().height*0.6f));
    
    
    mFondo->addChild(mTexto,2);
    
    
    
    
    mClose=Boton::createBoton(menu_selector(LayerInfo::close), "ui/close.png");
    mFondo->addChild(mClose,1);
    mClose->setPosition(Point(mFondo->getContentSize().width*0.05f,mFondo->getContentSize().height));
    mClose->setScale(0.5f);
    
  
    
    
    mFondo->setPosition(Point(mSize.width/2,mSize.height/2));
    this->addChild(mFondo);

}


void LayerInfo::close(Ref* sender){
     CCLOG("close");
    ocultar(NULL);
}
LayerInfo* LayerInfo::mostrar(std::string texto)
{
    LayerInfo* devolver=NULL;
    
    if(!Director::getInstance()->getRunningScene()){
        CCLOG("no hay running scene");
        return devolver;
    }
    if(Director::getInstance()->getRunningScene()->getChildByTag(LayerInfo::TAG_INFO)==NULL){
        LayerInfo* layer= LayerInfo::initLayer(texto);
        layer->setContentSize( Director::getInstance()->getWinSize());
        layer->setTag(LayerInfo::TAG_INFO);
        Director::getInstance()->getRunningScene()->addChild(layer,10);
        devolver=layer;
    }
    else{
        devolver=(LayerInfo*)Director::getInstance()->getRunningScene()->getChildByTag(LayerInfo::TAG_INFO);
        CCLOG("Layer loading previamente mostrada");
    }
    return devolver;
}

void LayerInfo::ocultar(Ref* pSender)
{
    if( Director::getInstance()->getRunningScene()->getChildByTag(LayerInfo::TAG_INFO)){
        Director::getInstance()->getRunningScene()->removeChildByTag(LayerInfo::TAG_INFO,true);
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/ui/popup_close.mp3");
    }
  
}

void LayerInfo::onEnter()
{
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/ui/info.mp3");
    LayerBase::onEnter();
}



void LayerInfo::setTexto(std::string texto){
      this->mTextoHelp=texto;
}

Node* LayerInfo::getCanvas()
{
    return mFondo;
}


Label* LayerInfo::crearTexto(std::string texto){
    
    Label* Texto=Label::createWithTTF(texto.c_str(),"fonts/CarterOne.ttf", 18);
    Texto->setWidth(mFondo->getContentSize().width*0.6f);
    Texto->setHeight(mFondo->getContentSize().height*0.3f);
    Texto->setHorizontalAlignment(TextHAlignment::CENTER);
    Texto->setVerticalAlignment(TextVAlignment::TOP);
    Texto->setColor(Color3B(255,255,255));
    Texto->setPosition(Point(mSize.width*0.5f,mSize.height*0.3f));
    Texto->enableOutline(Color4B(116,90,62,255),2);
    return Texto;
}


void LayerInfo::setNoOcultable(){
    mOcultable=false;
}


void LayerInfo::setSprite(Sprite* imagen){
    mFondo->addChild(imagen);
    imagen->setPosition(Point(mFondo->getContentSize().width/2,mFondo->getContentSize().height*0.35f));
    float tamanoXImagen=imagen->getContentSize().width;
    float tamanoYImagen=imagen->getContentSize().height;
    
    float scaleX=mFondo->getContentSize().width*0.35f/tamanoXImagen;
    float scaleY=mFondo->getContentSize().height*0.45f/tamanoYImagen;
    
    if(scaleX<=scaleY){
        imagen->setScale(scaleX);
        CCLOG("scaleX %f",scaleX);
    }else{
         imagen->setScale(scaleY);
        CCLOG("scaleY %f",scaleY);
    }
}

void LayerInfo::setBoton(Boton* boton){
    mFondo->addChild(boton);
    boton->setPosition(Point(mFondo->getContentSize().width/2,mFondo->getContentSize().height*0.4f));
}


void LayerInfo::set2Boton(Boton* boton1,Boton *boton2){
    mFondo->addChild(boton1);
    mFondo->addChild(boton2);

    boton1->setPosition(Point(mFondo->getContentSize().width*0.25f,mFondo->getContentSize().height*0.4f));
    boton2->setPosition(Point(mFondo->getContentSize().width*0.75f,mFondo->getContentSize().height*0.4f));
}

void LayerInfo::onKeyBackClicked()
{
    this->close(NULL);
    
}

