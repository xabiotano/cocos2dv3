

#ifndef __trivial__LayerGastarPista__
#define __trivial__LayerGastarPista__

#include <iostream>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "LayerBase.h"


USING_NS_CC;

USING_NS_CC_EXT;

class LayerGastarPista:  public LayerBase
{
    public:
    
    void initLayer();
    Rect rect();
   
    virtual bool init();
    // 'layer' is an autorelease object
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(LayerGastarPista);
    
    void dibujarContent();
    
    static void ocultar(Ref* pSender);
    static LayerGastarPista* mostrar();

   
    
    static const int TAG_GASTARPISTA=8252;
    Node* getCanvas();
   
    void setNoOcultable();
private:
    void close(Ref* sender);
    void ok(Ref* sender);

    Label* mTexto;
    Boton* mClose,*mOkButton;
    ScrollView* mScroll;
    Sprite* mFondo;
    bool mOcultable;
    


};

#endif 
