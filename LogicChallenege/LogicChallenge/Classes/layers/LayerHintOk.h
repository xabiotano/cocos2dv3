//
//  LayerHintOk.h
//  trivial
//
//  Created by Xabier on 25/03/13.
//
//

#ifndef __trivial__LayerHintOk__
#define __trivial__LayerHintOk__

#include <iostream>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "LayerBase.h"


USING_NS_CC;
USING_NS_CC_EXT;

class LayerHintOk:  public LayerBase
{
    public:
    
    static LayerHintOk* initLayer(bool bloquearScreen);
    Rect rect();
   
    virtual bool init() override;
    // 'layer' is an autorelease object
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(LayerHintOk);
    
    void dibujarContent();
    
    static void ocultar(Ref* pSender);
    static void mostrar(bool bloquearScreen);
    bool onTouchBegan(Touch* touch, Event* event) override;
    
    static int TAG_HINTOK;
    void setTouchBlocked(bool BloquearScreen);
    
    
  
    
private:
    void close(Ref* sender);
   
    Boton* mClose;
    ScrollView* mScroll;
    bool mBloquearScreen;

};

#endif /* defined(__trivial__LayerHintOk__) */
