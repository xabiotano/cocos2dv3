#include "LayerSuccess.h"
#include "../widgets/Boton.h"
#include "../Levels.h"
#include "../Levelsv2.h"
#include "../Variables.h"
#include "SimpleAudioEngine.h"
LayerSuccess:: ~LayerSuccess(void){}





LayerSuccess* LayerSuccess::mostrar(BaseScene* base,int numStars, TipoNivel tipoNivel)
{
    LayerSuccess* devolver=NULL;
    if(!Director::getInstance()->getRunningScene()){
        return NULL;
    }
    devolver= (LayerSuccess*)Director::getInstance()->getRunningScene()->getChildByTag(LayerSuccess::TAG_LAYER_SUCCESS);
    if(devolver==NULL){
        LayerSuccess* layer= LayerSuccess::initLayer(base,numStars, tipoNivel);
        layer->setContentSize( Director::getInstance()->getWinSize());
        layer->setTag(LayerSuccess::TAG_LAYER_SUCCESS);
        Director::getInstance()->getRunningScene()->addChild(layer,7);
        devolver=layer;
    }
    else{
        CCLOG("LayerSuccess previamente mostrada");
    }
    return devolver;
}

LayerSuccess* LayerSuccess::initLayer(BaseScene* base,int numStars,TipoNivel tipoNivel)
{
    LayerSuccess *layer = LayerSuccess::create();
    layer->mTipoNivel=tipoNivel;
    if(tipoNivel==TipoNivel(kNivelLogica)){
         layer->dibujarEstrellas(numStars);
        layer->mNextButton->removeFromParent();
        layer->mMenuButton->setPositionX(layer->mFondo_panel->getContentSize().width*0.66f);
        layer->mReloadButton->setPositionX(layer->mFondo_panel->getContentSize().width*0.33f);
    }
    else if(tipoNivel==TipoNivel(kNivelElixir)){
        layer->dibujarElixires(numStars);
        layer->mMenuButton->removeFromParent();
        layer->mNextButton->setPositionX(layer->mFondo_panel->getContentSize().width*0.66f);
        layer->mReloadButton->setPositionX(layer->mFondo_panel->getContentSize().width*0.33f);
    }
    layer->mPriority=-101;
    layer->mBase=base;
    return layer;
}



void LayerSuccess::dibujarEstrellas(int numStars ){
    Sprite* estrella1,*estrella2,*estrella3;
    switch(numStars){
        case 0:
            estrella1=Sprite::create("ui/levels/star_off.png");
            estrella1->setPosition(Point(mSize.width*0.4f,mSize.height*0.55f));
            estrella2=Sprite::create("ui/levels/star_off.png");
            estrella2->setPosition(Point(mSize.width*0.5f,mSize.height*0.55f));
            estrella3=Sprite::create("ui/levels/star_off.png");
            estrella3->setPosition(Point(mSize.width*0.6f,mSize.height*0.55f));
            break;
        case 1:
            estrella1=Sprite::create("ui/levels/star_on.png");
            estrella1->setPosition(Point(mSize.width*0.4f,mSize.height*0.55f));
            estrella2=Sprite::create("ui/levels/star_off.png");
            estrella2->setPosition(Point(mSize.width*0.5f,mSize.height*0.55f));
            estrella3=Sprite::create("ui/levels/star_off.png");
            estrella3->setPosition(Point(mSize.width*0.6f,mSize.height*0.55f));
            break;
        case 2:
            estrella1=Sprite::create("ui/levels/star_on.png");
            estrella1->setPosition(Point(mSize.width*0.4f,mSize.height*0.55f));
            estrella2=Sprite::create("ui/levels/star_on.png");
            estrella2->setPosition(Point(mSize.width*0.5f,mSize.height*0.55f));
            estrella3=Sprite::create("ui/levels/star_off.png");
            estrella3->setPosition(Point(mSize.width*0.6f,mSize.height*0.55f));
            break;
        case 3:
            estrella1=Sprite::create("ui/levels/star_on.png");
            estrella1->setPosition(Point(mSize.width*0.4f,mSize.height*0.55f));
            estrella2=Sprite::create("ui/levels/star_on.png");
            estrella2->setPosition(Point(mSize.width*0.5f,mSize.height*0.55f));
            estrella3=Sprite::create("ui/levels/star_on.png");
            estrella3->setPosition(Point(mSize.width*0.6f,mSize.height*0.55f));
            break;
    }
    this->addChild(estrella1);
    this->addChild(estrella2);
    this->addChild(estrella3);
    
    estrella1->setTag(TAG_ESTRELLA1);
    estrella2->setTag(TAG_ESTRELLA2);
    estrella3->setTag(TAG_ESTRELLA3);
    estrella2->setScale(1.2f);
}

void LayerSuccess::dibujarElixires(int numElixires ){
    Sprite* grano1,*grano2,*grano3;
    Texture2D * cafeTex=Director::getInstance()->getTextureCache()->addImage("elixircomun/elixir_m_relieve.png");
    
    grano1=Sprite::createWithTexture(cafeTex);
    grano1->setPosition(Point(mSize.width*0.4f,mSize.height*0.48f));
    addChild(grano1);
    grano1->setScale(0.6f);
    grano1->setRotation(10);
    
    
   
    grano2=Sprite::createWithTexture(cafeTex);
    grano2->setPosition(Point(mSize.width*0.5f,mSize.height*0.48f));
    addChild(grano2);
    grano2->setScale(0.6f);
    
    grano3=Sprite::createWithTexture(cafeTex);
    grano3->setPosition(Point(mSize.width*0.6f,mSize.height*0.48f));
    addChild(grano3);
    grano3->setScale(0.6f);
    grano3->setRotation(-10);
    
    if(numElixires==1){
        grano2->setOpacity(100.f);
        grano3->setOpacity(100.f);
    }
    if(numElixires==2){
        grano3->setOpacity(100.f);
    }
    
 
}




// on "init" you need to initialize your instance
bool LayerSuccess::init()
{
    mSize = Director::getInstance()->getWinSize();
    if ( !LayerColor::initWithColor((Color4B){0,0,0,200}, mSize.width, mSize.height )){
        return false;
    }
  

    
    
    mFondo_panel=Scale9Sprite::create("ui/levels/panel.png");
    mFondo_panel->setContentSize( Size(mSize.width*0.7f,mSize.height*0.5f) );
    mFondo_panel->setPosition(Point(mSize.width*0.5f,mSize.height*0.5f));
    this->addChild(mFondo_panel);
    mFondo_panel->setTag(TAG_FONDO);
    
    Sprite* ribbon=Sprite::create("ui/levels/ribbon.png");
    ribbon->setPosition(Point(mFondo_panel->getContentSize().width/2,mFondo_panel->getContentSize().height*0.9f));
    mFondo_panel->addChild(ribbon);
    ribbon->setColor(Color3B(32,180,81));
    
    
    /*CCLabelStroke* label=CCLabelStroke::create(ribbon, "OK", CCGetFont(), 40, Size(0, 0), kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter);
    label->setPosition(Point(ribbon->getContentSize().width/2,ribbon->getContentSize().height*0.67f));
    ribbon->addChild(label,4);
    label->setString(LanguageManager::getInstance()->getStringForKey("LAYER_SUCCESS_LEVEL_CLEARED","Level Cleared"),Color3B(10,10,10));
    */
    Label* label=Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("LAYER_SUCCESS_LEVEL_CLEARED"), CCGetFont(), 40);
    label->setOverflow(Label::Overflow::RESIZE_HEIGHT);
    //mLabel->setWidth(this->getContentSize().width);
    //mLabel->setHeight(this->getContentSize().height/2);
    label->setHorizontalAlignment(TextHAlignment::CENTER);
    label->setVerticalAlignment(TextVAlignment::CENTER);
    label->enableOutline(Color4B(0,0,0,255),2);
    label->setPosition(Point(ribbon->getContentSize().width/2,ribbon->getContentSize().height*0.67f));
    ribbon->addChild(label,4);
    
    
    
    mMenuButton=Boton::createBotonWithPriority(this, menu_selector(LayerBase::Menu), "ui/levels/menu.png",-102);
    mMenuButton->setPosition(Point(mFondo_panel->getContentSize().width*0.25f, mFondo_panel->getContentSize().height*0.1f));
    mFondo_panel->addChild(mMenuButton);
    mMenuButton->setSound("sounds/ui/click1.mp3");
   
    
    mReloadButton=Boton::createBotonWithPriority(this, menu_selector(LayerBase::Reload), "ui/levels/reload.png",-102);
    mReloadButton->setPosition(Point(mFondo_panel->getContentSize().width*0.5f, mFondo_panel->getContentSize().height*0.1f));
    mFondo_panel->addChild(mReloadButton);
    mReloadButton->setSound("sounds/ui/click1.mp3");

    
    mNextButton=Boton::createBotonWithPriority(this, menu_selector(LayerSuccess::Next), "ui/levels/forward.png",-102);
    mNextButton->setPosition(Point(mFondo_panel->getContentSize().width*0.75f, mFondo_panel->getContentSize().height*0.1f));
    mFondo_panel->addChild(mNextButton);
    mNextButton->setSound("sounds/ui/click1.mp3");

    
    this->setKeypadEnabled(true);
   
    
    return true;
}

void LayerSuccess::setTexto(std::string texto){
    
    Label* label=Label::createWithTTF(texto.c_str(), CCGetFont(), 25);
    label->setOverflow(Label::Overflow::RESIZE_HEIGHT);
    label->setWidth(mFondo_panel->getContentSize(). width*0.7f);
    //mLabel->setHeight(this->getContentSize().height/2);
    label->setHorizontalAlignment(TextHAlignment::CENTER);
    label->setVerticalAlignment(TextVAlignment::CENTER);
    label->enableOutline(Color4B(0,0,0,255),2);
    label->setPosition(Point(mFondo_panel->getContentSize().width/2,mFondo_panel->getContentSize().height*0.5f));
    mFondo_panel->addChild(label,4);
    
    
    
    Sprite* estrella1=(Sprite*) this->getChildByTag(TAG_ESTRELLA1);
     Sprite* estrella2=(Sprite*) this->getChildByTag(TAG_ESTRELLA2);
     Sprite* estrella3=(Sprite*) this->getChildByTag(TAG_ESTRELLA3);
    if(estrella1!=NULL){
        estrella1->setOpacity(0.f);
    } if(estrella2!=NULL){
        estrella2->setOpacity(0.f);
    }if(estrella3!=NULL){
        estrella3->setOpacity(0.f);
    }
  
}













void LayerSuccess::close(Ref* sender)
{
    this->getParent()->removeChild(this);
}



void LayerSuccess::Next(Ref* sender){
    Scene* scene;
    if(mTipoNivel==TipoNivel(kNivelLogica)){
        Variables::CURRENT_SCENE++;
        
        Levels::GoToScene(Variables::CURRENT_SCENE);
    }
    else if(mTipoNivel==TipoNivel(kNivelElixir)){
        
        scene=Levelsv2::scene();
    }
    Director *pDirector = Director::getInstance();
    pDirector->replaceScene(TransitionCrossFade::create(0.5f,scene));
    CCLOG("NEXT");
   
}


void LayerSuccess::onEnter()
{
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("sounds/ui/success1.mp3");
    LayerBase::onEnter();
}





