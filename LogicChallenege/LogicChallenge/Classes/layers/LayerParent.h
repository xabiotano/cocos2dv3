//
//  LayerParent.h
//  trivial
//
//  Created by Xabier on 25/03/13.
//
//

#ifndef __trivial__LayerParent__
#define __trivial__LayerParent__

#include <iostream>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "LayerBase.h"
#include "../widgets/Boton.h"


USING_NS_CC;

USING_NS_CC_EXT;

class LayerParent:  public LayerColor
{
    public:
    virtual void onEnter();
    virtual void onExit();

    EventListenerTouchOneByOne* mListener;
    virtual bool onTouchBegan(Touch* touch, Event* event);
    virtual void onTouchMoved(Touch* touch, Event* event);
    virtual void onTouchEnded(Touch* touch, Event* event);

    
    bool containsTouchLocation(Touch* touch);
    static LayerParent* create();
    
    Sprite* fondo;
    Sprite* candado1Button,*candado2Button,*candado3Button;
     Sprite* candado1ButtonOk,*candado2ButtonOk,*candado3ButtonOk;
    
    Boton* closeParent;
    
    void candado1();
    void candado2();
    void candado3();
    
    void initLayer();
    
    void closeParentFunction(Ref* sender);
    void successParent();
    
    static const int TAG_LAYER_PARENT=192228;

    bool mCandado1Tocado,mCandado2Tocado,mCandado3Tocado;

};

#endif 
