//
//  LayerHintOk.cpp
//  trivial
//
//  Created by Xabier on 25/03/13.
//
//

#include "LayerHintOk.h"
#include "Variables.h"
#include "SimpleAudioEngine.h"
#include "../BaseScene.h"



int LayerHintOk::TAG_HINTOK=8274;


LayerHintOk* LayerHintOk::initLayer(bool bloquearScreen){
    
    LayerHintOk *layer = LayerHintOk::create();
    layer->setTouchBlocked(bloquearScreen);
    layer->dibujarContent();
    
    return layer;
}
void LayerHintOk::setTouchBlocked(bool BloquearScreen){
    mBloquearScreen=BloquearScreen;
}

// on "init" you need to initialize your instance
bool LayerHintOk::init()
{
    //////////////////////////////
    // 1. super init first
    if (!LayerColor::initWithColor(Color4B(0,0,0,0))){
        return false;
    }
    
    return true;
}

void LayerHintOk::dibujarContent(){
    
    mSize = Director::getInstance()->getWinSize();
    mClose=Boton::createBoton(menu_selector(LayerHintOk::close), "ui/stop.png");
    addChild(mClose,1);
    mClose->setPosition(Point(mSize.width*0.1f,mSize.height*0.9f));
    mClose->setScale(0.85f);
    
}


void LayerHintOk::close(Ref* sender){
    ocultar(NULL);
    BaseScene::getInstance()->HintStop(NULL);
}
void LayerHintOk::mostrar(bool bloquearScreen)
{
  
    if(!Director::getInstance()->getRunningScene()){
        return;
    }
    if(!Director::getInstance()->getRunningScene()->getChildByTag(LayerHintOk::TAG_HINTOK)){
        LayerHintOk* layer= LayerHintOk::initLayer(bloquearScreen);
        layer->setContentSize( Director::getInstance()->getWinSize());
        layer->setTag(LayerHintOk::TAG_HINTOK);
        Director::getInstance()->getRunningScene()->addChild(layer,100);
    }
    else{
        CCLOG("Layer hint ok previamente mostrada");
    }
}

void LayerHintOk::ocultar(Ref* pSender)
{
    
    if( Director::getInstance()->getRunningScene()->getChildByTag(LayerHintOk::TAG_HINTOK)){
        Director::getInstance()->getRunningScene()->removeChildByTag(LayerHintOk::TAG_HINTOK,true);
    }
  
}


bool LayerHintOk::onTouchBegan(Touch* touch, Event* event)
{
    return mBloquearScreen;
}


