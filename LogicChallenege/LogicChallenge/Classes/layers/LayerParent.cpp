#include "LayerParent.h"
#include "Variables.h"
#include "SimpleAudioEngine.h"
#include "../widgets/Boton.h"
#include "../widgets/CCGetFont.h"
#include "LayerShop.h"
#include "SimpleAudioEngine.h"
#include "../widgets/LanguageManager.h"


LayerParent* LayerParent::create()
{
    Size  mVisibleSize = Director::getInstance()->getVisibleSize();
   
    LayerParent * pLayer = new LayerParent();
    if( pLayer && pLayer->initWithColor(Color4B(0,0,0,0),mVisibleSize.width,mVisibleSize.height))
    {
        pLayer->initLayer();
        pLayer->autorelease();
        return pLayer;
    }
    
    
    
    
    CC_SAFE_DELETE(pLayer);
    return NULL;
    
}




void LayerParent::initLayer(){
    
    Size  mVisibleSize = Director::getInstance()->getVisibleSize();
    
    fondo=Sprite::create("parental/background.png");
    fondo->setPosition(Point(mVisibleSize.width/2,mVisibleSize.height/2));
    this->addChild(fondo);
    fondo->setTag(28401);
    
    /*CCLabelStroke* label=CCLabelStroke::create(fondo, LanguageManager::getInstance()->getStringForKey("PARENTAL_CONTROL", "PARENTAL_CONTROL"), CCGetFont(), 40, Size(mVisibleSize.width*0.7f,0), kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter);
    label->setPosition(Point(fondo->getContentSize().width*0.5f,fondo->getContentSize().height*0.8f ));
    fondo->addChild(label,3);
    
    label->setString(LanguageManager::getInstance()->getStringForKey("PARENTAL_CONTROL", "PARENTAL_CONTROL"), Color3B((0,0,0));*/
    Label* label= Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("PARENTAL_CONTROL", "PARENTAL_CONTROL"), CCGetFont(), 40);
    label->setWidth(fondo->getContentSize().width*0.5f);
    label->setOverflow(Label::Overflow::RESIZE_HEIGHT);
    //label->setHeight(mFondo_panel->getContentSize().width*0.5f);
    label->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    label->setPosition(Point(fondo->getContentSize().width*0.5f,fondo->getContentSize().height*0.8f ));
    label->enableOutline(Color4B(0,0,0,255),2);
    fondo->addChild(label,3);
    
    
    
    
    candado1Button=Sprite::create("parental/lock.png");
    candado1Button->setPosition(Point(mVisibleSize.width*0.25f,mVisibleSize.height*0.6f));
    fondo->addChild(candado1Button);
    
    candado2Button=Sprite::create("parental/lock.png");
    candado2Button->setPosition(Point(mVisibleSize.width*0.5f,mVisibleSize.height*0.6f));
    fondo->addChild(candado2Button);
    
    
    
    candado3Button=Sprite::create("parental/lock.png");
    candado3Button->setPosition(Point(mVisibleSize.width*0.75f,mVisibleSize.height*0.6f));
    fondo->addChild(candado3Button);

    
    closeParent=Boton::createBoton(NULL, menu_selector(LayerParent::closeParentFunction), "ui/close.png");
    closeParent->setPosition(Point(mVisibleSize.width*0.9f, mVisibleSize.height*0.9f));
    fondo->addChild(closeParent);
   
    mCandado1Tocado=false;
    mCandado2Tocado=false;
    mCandado3Tocado=false;
    
    
}


void LayerParent::closeParentFunction(Ref* sender){
    LayerShop::ocultar(NULL);
     Size  mVisibleSize = Director::getInstance()->getVisibleSize();
     CallFunc* remove=CallFunc::create([this]() { this->removeFromParent(); });
    fondo->runAction(Sequence::createWithTwoActions(MoveTo::create(0.2f, Point(mVisibleSize.width/2, +mVisibleSize.height*1.5f)),remove ));
      CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/serieslogic/delete.mp3",false);
}
void LayerParent::successParent(){
    Size  mVisibleSize = Director::getInstance()->getVisibleSize();
    CallFunc* remove=CallFunc::create([this]() { this->removeFromParent(); });
    fondo->runAction(Sequence::createWithTwoActions(MoveTo::create(0.2f, Point(mVisibleSize.width/2, +mVisibleSize.height*1.5f)),remove ));
    
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/serieslogic/delete.mp3",false);
}




void LayerParent::candado1(){
    CCLOG("CANDADO 1");
}

void LayerParent::candado2(){
    CCLOG("CANDADO 2");

}

void LayerParent::candado3(){
    CCLOG("CANDADO 3");
}

void LayerParent::onEnter()
{
    
    mListener = EventListenerTouchOneByOne::create();
    mListener->setSwallowTouches(true);
    mListener->onTouchBegan = CC_CALLBACK_2(LayerParent::onTouchBegan, this);
    mListener->onTouchMoved = CC_CALLBACK_2(LayerParent::onTouchMoved, this);
    mListener->onTouchEnded = CC_CALLBACK_2(LayerParent::onTouchEnded, this);
    
    this->getEventDispatcher()->addEventListenerWithFixedPriority(mListener, -1);

    Layer::onEnter();
}

void LayerParent::onExit()
{
    
     this->getEventDispatcher()->removeEventListener(mListener);
   
    
    Layer::onExit();
}






bool LayerParent::onTouchBegan(Touch* touch, Event* event)
{
    if(containsTouchLocation(touch)){
        successParent();
    }
    return true;
    
}

void LayerParent::onTouchEnded(Touch* touch, Event* event)
{
    mCandado1Tocado=false;
    mCandado2Tocado=false;
    mCandado3Tocado=false;
    
    if(candado1ButtonOk!=NULL){
        candado1ButtonOk->removeFromParent();
        candado1ButtonOk=NULL;
    }
    if(candado2ButtonOk!=NULL){
        candado2ButtonOk->removeFromParent();
        candado2ButtonOk=NULL;
    }
    if(candado3ButtonOk!=NULL){
        candado3ButtonOk->removeFromParent();
        candado3ButtonOk=NULL;
    }
  
}
void LayerParent::onTouchMoved(Touch* touch, Event* event)
{
}
       
bool LayerParent::containsTouchLocation(Touch* touch)
{
    Size closeParentSize = candado1Button->getTexture()->getContentSize();
    
    bool closeParentSizeTouch= Rect(closeParent->getPositionX()-closeParentSize.width/2 , closeParent->getPositionY()- closeParentSize.height/2, closeParentSize.width, closeParentSize.height).containsPoint(convertTouchToNodeSpace(touch));
    if(closeParentSizeTouch){
        closeParentFunction(NULL);
        return true;
    }
    

    
    
    Size sCandado1 = candado1Button->getTexture()->getContentSize();

    
    bool candado1touch= Rect(candado1Button->getPositionX()-sCandado1.width/2 , candado1Button->getPositionY()- sCandado1.height/2, sCandado1.width, sCandado1.height).containsPoint(convertTouchToNodeSpace(touch));
    
    bool candado2touch= Rect(candado2Button->getPositionX()-sCandado1.width/2 , candado2Button->getPositionY()- sCandado1.height/2, sCandado1.width, sCandado1.height).containsPoint(convertTouchToNodeSpace(touch));

    
    bool candado3touch= Rect(candado3Button->getPositionX()-sCandado1.width/2 , candado3Button->getPositionY()- sCandado1.height/2, sCandado1.width, sCandado1.height).containsPoint(convertTouchToNodeSpace(touch));

    if(candado1touch){
        mCandado1Tocado=true;
        candado1Button->runAction(Sequence::create(ScaleTo::create(0.2f, 1.1f),ScaleTo::create(0.2f, 1.f),NULL));
         CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/serieslogic/insert_letter1.mp3",false);
        candado1ButtonOk=Sprite::create("parental/lock_activo.png");
        candado1ButtonOk->setPosition(candado1Button->getPosition());
        fondo->addChild(candado1ButtonOk);
       
    }if(candado2touch){
        mCandado2Tocado=true;
          candado2Button->runAction(Sequence::create(ScaleTo::create(0.2f, 1.1f),ScaleTo::create(0.2f, 1.f),NULL));
          CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/serieslogic/insert_letter1.mp3",false);
        candado2ButtonOk=Sprite::create("parental/lock_activo.png");
        candado2ButtonOk->setPosition(candado2Button->getPosition());
        fondo->addChild(candado2ButtonOk);
        
    }if(candado3touch){
        mCandado3Tocado=true;
          candado3Button->runAction(Sequence::create(ScaleTo::create(0.2f, 1.1f),ScaleTo::create(0.2f, 1.f),NULL));
           CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/serieslogic/insert_letter1.mp3",false);
        candado3ButtonOk=Sprite::create("parental/lock_activo.png");
        candado3ButtonOk->setPosition(candado3Button->getPosition());
        fondo->addChild(candado3ButtonOk);
        
    }
    
    CCLOG("containsTouchLocation %i %i %i", candado1touch,candado2touch,candado3touch);
    return mCandado1Tocado && mCandado2Tocado && mCandado3Tocado;
    
}

