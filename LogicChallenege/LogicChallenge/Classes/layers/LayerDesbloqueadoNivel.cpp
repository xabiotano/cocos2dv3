//
//  LayerDesbloqueadoNivel.cpp
//  trivial
//
//  Created by Xabier on 25/03/13.
//
//

#include "LayerDesbloqueadoNivel.h"
#include "Variables.h"
#include "SimpleAudioEngine.h"
#include "../BaseScene.h"
#include "../Levels.h"



int LayerDesbloqueadoNivel::TAG_DESBLOQUEADO=12113;


LayerDesbloqueadoNivel* LayerDesbloqueadoNivel::initLayer(){
    LayerDesbloqueadoNivel *layer = LayerDesbloqueadoNivel::create();
    layer->dibujarContent();
    layer->mSwallowTouches=true;
    return layer;
    
}


// on "init" you need to initialize your instance
bool LayerDesbloqueadoNivel::init()
{
    //////////////////////////////
    // 1. super init first
    if (!LayerBase::initWithColor(Color4B(0,0,0,200))){
        return false;
    }
    this->setKeypadEnabled(true);
    
    return true;
}

void LayerDesbloqueadoNivel::dibujarContent(){
    
    Size mVisibleSize = Director::getInstance()->getVisibleSize();
   
    Boton* close=Boton::createBoton(this,menu_selector(LayerDesbloqueadoNivel::close),"ui/close.png");
    close->setPosition(Point(mVisibleSize.width*0.9f,mVisibleSize.height*0.9f));
    this->addChild(close);
    
    Sprite* explorer=Sprite::create("levelunlocked/explorer_xxl_without_bulb.png");
    explorer->setPosition(Point(explorer->getContentSize().width/2, explorer->getContentSize().height/2));
    this->addChild(explorer);
    
    Sprite* yeah=Sprite::create("levelunlocked/yeah.png");
    yeah->setPosition(Point(explorer->getContentSize().width, explorer->getContentSize().height*0.3f));
    explorer->addChild(yeah);
    
    Sprite* bulb=Sprite::create("levelunlocked/bombilla.png");
    bulb->setPosition(Point(mVisibleSize.width- bulb->getContentSize().width*0.8/2, bulb->getContentSize().height*0.3f));
    this->addChild(bulb);
    bulb->runAction(RepeatForever::create(Sequence::createWithTwoActions(EaseInOut::create(ScaleTo::create(0.5f, 1.02f),3), ScaleTo::create(0.5f, 1.f))));
    
    Boton* play=Boton::createBoton(this,menu_selector(LayerDesbloqueadoNivel::irALogicMode),"levelunlocked/play.png");
    play->setPosition(Point(bulb->getContentSize().width* 0.5, bulb->getContentSize().height*0.5f));
    bulb->addChild(play);
    play->setSound("sounds/elixircomun/select.mp3");
    
    
    /*Label* mTexto=CCLabelStroke::create(this, "", CCGetFont(), 40, Size(mVisibleSize.width*0.4f, 1), kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter);
    mTexto->setColor(Color3B(255,255,255));
    mTexto->setPosition(Point(this->getContentSize().width*0.5f,this->getContentSize().height*0.7f));
    this->addChild(mTexto,3);
    mTexto->setString(LanguageManager::getInstance()->getStringForKey("UNLOCKED_ELIXIR", "UNLOCKED_ELIXIR"), Color3B(0,0,0));*/
   
    Label* mTexto= Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("UNLOCKED_ELIXIR", "UNLOCKED_ELIXIR"), CCGetFont(), 40);
    mTexto->setColor(Color3B(255,255,255));
    mTexto->setWidth(mVisibleSize.width*0.4f);
    //mTexto->setHeight(mPanelAvisos->getContentSize().height);
    mTexto->setPosition(Point(this->getContentSize().width*0.5f,this->getContentSize().height*0.7f));
    mTexto->setAlignment(TextHAlignment::CENTER, TextVAlignment::TOP);
    mTexto->enableOutline(Color4B(0,0,0,255),2);
    mTexto->setTag(999);
    this->addChild(mTexto,3);

    
    
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elixircomun/level_unlock.mp3",false);
    

   // this->scheduleOnce(schedule_selector(LayerDesbloqueadoNivel::moverPopUp), 2.f);
}

void LayerDesbloqueadoNivel::moverPopUp(){
     this->setOpacity(1.f);
}


void LayerDesbloqueadoNivel::close(Ref* sender){
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/ui/popup_close.mp3");
    CCLOG("close");
    ocultar(NULL);
    
}
LayerDesbloqueadoNivel* LayerDesbloqueadoNivel::mostrar()
{
    //se muestra si el nivel de Logica que con los items actuales tiene le sirve para jugar al siguiente nivel
    LayerDesbloqueadoNivel* devolver=NULL;
    
    if(!Director::getInstance()->getRunningScene()){
        CCLOG("no hay running scene");
        return devolver;
    }
    if(Director::getInstance()->getRunningScene()->getChildByTag(LayerDesbloqueadoNivel::TAG_DESBLOQUEADO)==NULL){
        LayerDesbloqueadoNivel* layer= LayerDesbloqueadoNivel::initLayer();
        layer->setContentSize( Director::getInstance()->getWinSize());
        layer->setTag(LayerDesbloqueadoNivel::TAG_DESBLOQUEADO);
        Director::getInstance()->getRunningScene()->addChild(layer,LayerBase::ZINDEX_MEDIO);
        devolver=layer;
    }
    else{
        devolver=(LayerDesbloqueadoNivel*)Director::getInstance()->getRunningScene()->getChildByTag(LayerDesbloqueadoNivel::TAG_DESBLOQUEADO);
        CCLOG("Layer desbloqueo previamente mostrada");
    }
    return devolver;
}

void LayerDesbloqueadoNivel::ocultar(Ref* pSender)
{
    if( Director::getInstance()->getRunningScene()->getChildByTag(LayerDesbloqueadoNivel::TAG_DESBLOQUEADO)){
        Director::getInstance()->getRunningScene()->removeChildByTag(LayerDesbloqueadoNivel::TAG_DESBLOQUEADO,true);
    }
  
}





void LayerDesbloqueadoNivel::irALogicMode(Ref* sender)
{
    Scene* scene=Levels::createScene();
    Director *pDirector = Director::getInstance();
    pDirector->replaceScene(TransitionCrossFade::create(0.5f,scene));
}



void LayerDesbloqueadoNivel::onKeyBackClicked()
{
   this->close(NULL);
    
}






