//
//  LayerNecesitasSuperar.h
//  trivial
//
//  Created by Xabier on 25/03/13.
//
//

#ifndef __trivial__LayerNecesitasSuperar__
#define __trivial__LayerNecesitasSuperar__

#include <iostream>
#include "cocos2d.h"
#include "LayerBase.h"
#include "cocos-ext.h"



USING_NS_CC_EXT;
USING_NS_CC;


class LayerNecesitasSuperar: public LayerBase
{
    public:
       template <typename T> std::string tostr(const T& t);
        const int TAG_FONDO=11;
        static const int TAG_LAYER_NECESITASSUPERAR=2942066;
    
        virtual ~LayerNecesitasSuperar(void);
        static LayerNecesitasSuperar* get_instance();
        static LayerNecesitasSuperar* initLayer();
        static LayerNecesitasSuperar* mostrar();
        Rect rect();
        // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
        virtual bool init();
        // 'layer' is an autorelease object
        // preprocessor macro for "static create()" constructor ( node() deprecated )
        CREATE_FUNC(LayerNecesitasSuperar);
        virtual void onEnter();
        virtual void onExit();
        Size mSize;
    
        void dibujarItems();
        void onKeyBackClicked();
        static void ocultar(Ref* pSender);

    private:
        void close();
        int mNumeroNivel;
       // void Next();
        Scale9Sprite* mFondo_panel;
    
    
    
    
    
        
    
};

#endif /* defined(__trivial__LayerNecesitasSuperar__) */
