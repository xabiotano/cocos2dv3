#include "LayerShop.h"
#include "SimpleAudioEngine.h"
#include "PluginIAP/PluginIAP.h"
#include "../../BaseScene.h"
#include "../../helpers/LogicSQLHelper.h"
#include "../LayerParent.h"
#include <algorithm>
#include <string>
#include "PluginIAP/PluginIAP.h"
#include "PluginGoogleAnalytics/PluginGoogleAnalytics.h"

using namespace cocos2d;
using namespace sdkbox;




LayerShop* LayerShop::mInstance = 0;


void LayerShop::releaseLayer()
{
    CC_SAFE_RELEASE_NULL(mInstance);
    mInstance=0;
}

LayerShop* LayerShop::getInstance()
{
    if (mInstance == 0)
    {
        mInstance = LayerShop::create();
        mInstance->retain();
        
    }
    return mInstance;
}









LayerShop::~LayerShop(void){
    CC_SAFE_RELEASE(mScroll);
    
}





std::string LayerShop::COMPRA_COMPLETE_GAME="com.kangaroo.logic.complete_game";
std::string LayerShop::COMPRA_REMOVE_ADS="com.kangaroo.logic.remove_ads";
std::string LayerShop::COMPRA_1_PISTA="com.kangaroo.logic.1_hint";
std::string LayerShop::COMPRA_3_PISTAS="com.kangaroo.logic.3_hint";
std::string LayerShop::COMPRA_5_PISTAS="com.kangaroo.logic.5_hint";
std::string LayerShop::COMPRA_10_PISTAS="com.kangaroo.logic.10_hint";


std::string LayerShop::COMPRA_20_ELIXIRES="com.kangaroo.logic.20elixires";
std::string LayerShop::COMPRA_60_ELIXIRES="com.kangaroo.logic.60elixirs";
std::string LayerShop::COMPRA_150_ELIXIRES="com.kangaroo.logic.100Elixirs";
std::string LayerShop::COMPRA_500_ELIXIRES="com.kangaroo.logic.500elixirs";





// on "init" you need to initialize your instance
bool LayerShop::init()
{
   // mSwallowTouches=false;
    //////////////////////////////
    // 1. super init first
    if (!LayerBase::initWithColor(Color4B(0,0,0,180))){
        return false;
    }
    _products=std::vector<sdkbox::Product>();
    _products.clear();
    
    setContentSize( Director::getInstance()->getWinSize());
    setTag(LayerShop::TAG_SHOP);
    
    
    sdkbox::IAP::setListener(this);
    sdkbox::IAP::refresh();
    dibujarContent();
    
    return true;
}

void LayerShop::dibujarContent(){
    
    mSize = Director::getInstance()->getVisibleSize();
    mFondo=Sprite::create("ui/shop/fondo_shop.png");
    
    
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    
    // position the sprite on the center of the screen
    mFondo->setPosition(Point(mSize.width/2 + origin.x, mSize.height/2 + origin.y));
    
    
    float scalex=Director::getInstance()->getVisibleSize().width*0.9f/mFondo->getContentSize().width;
    float scaley=Director::getInstance()->getVisibleSize().height*0.9f/mFondo->getContentSize().height;
    
    mFondo->setScaleX(scalex);
    mFondo->setScaleY(scaley);
    
    
    mClose=Boton::createBoton(menu_selector(LayerShop::close), "ui/close.png");
    mFondo->addChild(mClose,1);
    mClose->setPosition(Point(mFondo->getContentSize().width*0.95f,mFondo->getContentSize().height*0.9f));
    mClose->setScale(0.7f);
    
    
    addChild(mFondo);
    
    Sprite* loading=Sprite::create("ui/shop/loading.png");
    mFondo->addChild(loading);
    loading->runAction(RepeatForever::create(RotateBy::create(1.f, 360)));
    loading->setPosition(Point(mFondo->getContentSize().width/2,mFondo->getContentSize().height/2));
    loading->setTag(555);
    
    mContenido=LayerColor::create(Color4B(0,0,0,0), mFondo->getContentSize().width, mFondo->getContentSize().height*2.9f);
    
    
    mScroll=ScrollView::create();
    mScroll->retain();
    
    mScroll->setContentSize(mContenido->getContentSize());
    mScroll->setDirection(ScrollView::Direction::VERTICAL);
    mScroll->setPosition(Point(mSize.width*0.1f,mSize.height*0.1f));
    addChild(mScroll,1);
    
    
    mScroll->setBounceable(true);
    mScroll->setClippingToBounds(true);
    mScroll->setContainer(mContenido);
    
    mScroll->setViewSize(Size(mSize.width*0.9f,mSize.height*0.7f));
    mScroll->setContentOffset(Point(5.f, (mSize.height*0.75-mContenido->getContentSize().height)), true);
    mScroll->setDelegate(this);
    
    
    
    mScrollIndicator=Sprite::create("ui/scroll_indicator.png");
    mScrollIndicator->setPosition(Point(mFondo->getContentSize().width*0.99f,mFondo->getContentSize().height/2));
    mFondo->addChild(mScrollIndicator);
    mScrollIndicator->setScaleY(0.8f);
    mScrollIndicatorBullet=Sprite::create("ui/scroll_indicator_bullet.png");
    mScrollIndicator->addChild(mScrollIndicatorBullet);
    scrollViewDidScroll(mScroll);
    
    mProductoComplete=sdkbox::Product();
    
    mProducto1Hint=sdkbox::Product();
    mProducto3Hint=sdkbox::Product();
    mProducto5Hint=sdkbox::Product();
    mProducto10Hint=sdkbox::Product();
    
    mProductoComplete=sdkbox::Product();
    mProductoRemoveAds=sdkbox::Product();
    
    mProducto20Elixir=sdkbox::Product();
    mProducto60Elixir=sdkbox::Product();
    mProducto150Elixir=sdkbox::Product();
    mProducto500Elixir=sdkbox::Product();;
    
    
}


void LayerShop::close(Ref* sender){
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/ui/popup_close.mp3");
    CCLOG("close");
    ocultar(NULL);
}
LayerShop* LayerShop::mostrar(ILayerShop* receiver)
{
    LayerShop* devolver=NULL;
    
    if(!Director::getInstance()->getRunningScene()){
        CCLOG("no hay running scene");
        return devolver;
    }
    
    if(Director::getInstance()->getRunningScene()->getChildByTag(LayerShop::TAG_SHOP)==NULL){
        LayerShop* layer= LayerShop::getInstance();
        layer->setReceiver(receiver);
        Director::getInstance()->getRunningScene()->addChild(layer,10);
        devolver=layer;
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/ui/popup_open.mp3");
        sdkbox::IAP::refresh();
        CCLOG("Layer Shop creada");
    }
    else{
        devolver=(LayerShop*)Director::getInstance()->getRunningScene()->getChildByTag(LayerShop::TAG_SHOP);
        CCLOG("Layer Shop previamente mostrada");
    }
    return devolver;
}

void LayerShop::setReceiver(ILayerShop* receiver)
{
    mReceiver=receiver;
}

void LayerShop::ocultar(Ref* pSender)
{
    if( Director::getInstance()->getRunningScene()->getChildByTag(LayerShop::TAG_SHOP)){
        Director::getInstance()->getRunningScene()->removeChildByTag(LayerShop::TAG_SHOP,true);
    }
    
}




bool LayerShop::onTouchBegan(Touch* touch, Event* event)
{
    return true;
}

void LayerShop::onTouchEnded(Touch* touch, Event* event)
{
    
    
}
void LayerShop::onTouchMoved(Touch* touch, Event* event)
{
}

void LayerShop::cargarProductos(const std::vector<sdkbox::Product>& products)
{
    mFondo->removeChildByTag(555,true);
    mContenido->removeAllChildrenWithCleanup(true);
    
    Size tamanoContenedor=mContenido->getContentSize();
    
    Sprite* item=Sprite::create("ui/shop/item.png");
    Sprite* itemXXL=Sprite::create("ui/shop/item.png");
    
    float widthOrigen=tamanoContenedor.width/4;
    float marginX=item->getContentSize().width;
    
    float heightOrigen=tamanoContenedor.height - item->getContentSize().height*0.8f;
    float marginY=item->getContentSize().height;
    
    Point posTextoTitulo=Point(item->getContentSize().width*0.42f, item->getContentSize().height*0.73f);
    Point posTextoSubTitulo=Point(item->getContentSize().width*0.33f, item->getContentSize().height*0.33f);
    Point posBotonCompra=Point(item->getContentSize().width*0.8f, item->getContentSize().height/2);
    
    
    
    CCLOG("Products(size):%i",(int)products.size());
    for (int i=0; i < products.size(); i++)
    {
        std::string produdcto=products[i].id;
        std::transform(produdcto.begin(),produdcto.end(), produdcto.begin(), ::tolower);
        
        if(produdcto==COMPRA_COMPLETE_GAME){
            mProductoComplete=products[i];
        }else if(products[i].id==COMPRA_REMOVE_ADS){
            mProductoRemoveAds=products[i];
        }else if(products[i].id==COMPRA_1_PISTA){
            mProducto1Hint=products[i];
        }else if(products[i].id==COMPRA_3_PISTAS){
            mProducto3Hint=products[i];
        }else if(products[i].id==COMPRA_5_PISTAS){
            mProducto5Hint=products[i];
        }else if(products[i].id==COMPRA_10_PISTAS){
            mProducto10Hint=products[i];
        }else if(products[i].id==COMPRA_20_ELIXIRES){
            mProducto20Elixir=products[i];
        }else if(products[i].id==COMPRA_60_ELIXIRES){
            mProducto60Elixir=products[i];
        }else if(products[i].id==COMPRA_150_ELIXIRES || products[i].id=="com.kangaroo.logic.100elixirs"){
            mProducto150Elixir=products[i];
        }else if(products[i].id==COMPRA_500_ELIXIRES){
            mProducto500Elixir=products[i];
        }
    }
    
    
    
    
    int index=0;
    for(int i=0;i<=13;i++){
        
        sdkbox::Product product=sdkbox::Product();
        SEL_MenuHandler menu_handler;
        bool compradoPreviamente=false;
        Point lugarItem;
        
        Sprite* item;
        if(i==0){
            product=mProductoComplete;
            // CCLOG("Product Bucle %s %s  %s",product.id.c_str(),product.name.c_str(),product.price.c_str());
            compradoPreviamente=LogicSQLHelper::getInstance().estaElJuegoCompleto();
            if(compradoPreviamente){
                menu_handler=menu_selector(LayerShop::BuyYaComprado);
            }else{
                menu_handler=menu_selector(LayerShop::Buy1);
            }
            item=crearItem(false, menu_handler, product, compradoPreviamente);
            index++;
        }else if(i==1){
            product=mProductoRemoveAds;
            compradoPreviamente=LogicSQLHelper::getInstance().estaSinPublicidadCompradoCompra();
            if(compradoPreviamente){
                menu_handler=menu_selector(LayerShop::BuyYaComprado);
            }else{
                menu_handler=menu_selector(LayerShop::Buy2);
            }
            item=crearItem(false, menu_handler, product, compradoPreviamente);
            index++;
        }else if(i==2){
            item =crearSeparador(i);
            item->setPosition(Point(mContenido->getContentSize().width*0.5f,heightOrigen - ((i*marginY) +marginY*0.25 )));
            index++;
            index++;
        }else if(i==3){
            product=  product=mProducto1Hint;;
            menu_handler=menu_selector(LayerShop::Buy3);
            item=crearItem(false, menu_handler, product, false);
            index++;
        }else if(i==4){
            product=mProducto3Hint;
            menu_handler=menu_selector(LayerShop::Buy4);
            item=crearItem(false, menu_handler, product, false);
            index++;
        }else if(i==5){
            product=  product=mProducto5Hint;;
            menu_handler=menu_selector(LayerShop::Buy5);
            item=crearItem(false, menu_handler, product, false);
            index++;
        }else if(i==6){
            product=mProducto10Hint;
            menu_handler=menu_selector(LayerShop::Buy6);
            item=crearItem(false, menu_handler, product, false);
            index++;
        }else if(i==7){
            item=crearSeparador(i);
            
            index++;
            index++;
        }
        else if(i==8){
            product=mProducto20Elixir;
            menu_handler=menu_selector(LayerShop::Buy7);
            item=crearItem(true, menu_handler, product, false);
            index++;
        }else if(i==9){
            product=mProducto60Elixir;
            menu_handler=menu_selector(LayerShop::Buy8);
            item=crearItem(true, menu_handler, product, false);
            index++;
        }else if(i==10){
            product=mProducto150Elixir;
            menu_handler=menu_selector(LayerShop::Buy9);
            item=crearItem(true, menu_handler, product, false);
            index++;
            index++;
        }else if(i==11){
            product=mProducto500Elixir;
            menu_handler=menu_selector(LayerShop::Buy10);
            item=crearItem(true, menu_handler, product, false);
            index++;
        }else if(i==12){
            item=crearSeparador(i);
            item->setPosition(Point(mContenido->getContentSize().width*0.5f,heightOrigen - ((i*marginY) +marginY*0.25 )));
            index++;
            index++;
        }else if(i==13){
            product=sdkbox::Product();
            product.name=LanguageManager::getInstance()->getStringForKey("RESTORE_ITEMS_TITLE", "RESTORE_ITEMS_TITLE");
            product.title=LanguageManager::getInstance()->getStringForKey("RESTORE_ITEMS_TITLE", "RESTORE_ITEMS_TITLE");
            product.description=LanguageManager::getInstance()->getStringForKey("RESTORE_ITEMS_SUBTITLE","RESTORE_ITEMS_SUBTITLE");
            product.price="go!";
            menu_handler=menu_selector(LayerShop::RestoreInApps);
            item=crearItem(false, menu_handler, product, false);
            
            
        }
        CCLOG("Product %s %s  %s",product.id.c_str(),product.name.c_str(),product.price.c_str());
        Point punto;//=Point(widthOrigen +(j*marginX),heightOrigen - i*marginY);
        //COMPLETE & REMOVEADS
        if(i==0){
            punto=Point(widthOrigen,heightOrigen );
        }else if(i==1){
            punto=Point(widthOrigen+marginX,heightOrigen );
        }
        //SEPARADOR
        else if(i==2){
            punto=Point(mContenido->getContentSize().width*0.5f,heightOrigen - ((marginY) +marginY*0.25 ));
        }
        //HINTS
        else if(i==3){
            punto=Point(widthOrigen,heightOrigen - 2*marginY);
        }else if(i==4){
            punto=Point(widthOrigen+marginX,heightOrigen - 2*marginY);
        }else if(i==5){
            punto=Point(widthOrigen,heightOrigen - 3*marginY);
        }else if(i==6){
            punto=Point(widthOrigen+marginX,heightOrigen - 3*marginY);
        }
        //SEPARADOR
        else if(i==7){
            punto=Point(mContenido->getContentSize().width*0.5f,heightOrigen - ((4*marginY) +marginY*0.25 ));
        }
        //UNO DE ELIXIR POR FILA
        else if(i==8){
            punto=Point(widthOrigen + itemXXL->getContentSize().width/2,heightOrigen - 5*marginY);
        }else if(i==9){
            punto=Point(widthOrigen + itemXXL->getContentSize().width/2,heightOrigen - 6*marginY);
        }else if(i==10){
            punto=Point(widthOrigen+ itemXXL->getContentSize().width/2,heightOrigen - 7*marginY);
        }else if(i==11){
            punto=Point(widthOrigen+ itemXXL->getContentSize().width/2,heightOrigen - 8*marginY);
        }
        //SEPARADOR RESTORE
        else if(i==12){
            punto=Point(mContenido->getContentSize().width*0.5f,heightOrigen - ((9*marginY) +marginY*0.25 ));
        }
        //RESTORE
        else if(i==13){
            punto=Point(widthOrigen,heightOrigen - 10*marginY);
        }
        item->setPosition(punto);
        mContenido->addChild(item);
    }
    
}

void LayerShop::BuyYaComprado(Ref* sender){
    //NO HAGO NADA
}

void LayerShop::Buy(sdkbox::Product producto){
    
    onIAP(producto);
    
}

void LayerShop::Buy1(Ref* sender){
    Buy(mProductoComplete);
}
void LayerShop::Buy2(Ref* sender){
    Buy(mProductoRemoveAds);
}
void LayerShop::Buy3(Ref* sender){
    Buy(mProducto1Hint);
}
void LayerShop::Buy4(Ref* sender){
    Buy(mProducto3Hint);
}
void LayerShop::Buy5(Ref* sender){
    Buy(mProducto5Hint);
}
void LayerShop::Buy6(Ref* sender){
    Buy(mProducto10Hint);
}
void LayerShop::Buy7(Ref* sender){
    Buy(mProducto20Elixir);
}
void LayerShop::Buy8(Ref* sender){
    Buy(mProducto60Elixir);
}
void LayerShop::Buy9(Ref* sender){
    Buy(mProducto150Elixir);
}
void LayerShop::Buy10(Ref* sender){
    Buy(mProducto500Elixir);
}


void LayerShop::RestoreInApps(Ref* sender){
    onRestoreIAP();
}






bool LayerShop::EstaMostrado(){
    LayerShop* devolver=(LayerShop*)Director::getInstance()->getRunningScene()->getChildByTag(LayerShop::TAG_SHOP);
    if(devolver!=NULL){
        return true;
    }
    else{
        return false;
    }
}


void LayerShop::scrollViewDidScroll(ScrollView* view){
    
    float y= (-view->getContentOffset().y)/view->getContainer()->getContentSize().height;
    //CCLOG("%f / %f => %f",-view->getContentOffset().y,view->getContainer()->getContentSize().height,y);
    //0.10   0.3
    y=y/0.8f;
    // CCLOG("%f",y);
    if(y<0.f ){
        y=0;
    }
    if(y>1.f){
        y=1.f;
    }
    mScrollIndicatorBullet->setPosition(Point(mScrollIndicatorBullet->getParent()->getContentSize().width*0.5f,mScrollIndicatorBullet->getParent()->getContentSize().height*y));
}





void LayerShop::OnSuccesPurchase(sdkbox::Product const& p){
    if (p.id ==LayerShop::COMPRA_REMOVE_ADS) {
        UserDefault::getInstance()->setBoolForKey(LayerShop::COMPRA_REMOVE_ADS.c_str(), true);
        UserDefault::getInstance()->flush();
        mReceiver->onRemoveAdsComprado();
    }else if (p.id == LayerShop::COMPRA_COMPLETE_GAME) {
        UserDefault::getInstance()->setBoolForKey(LayerShop::COMPRA_COMPLETE_GAME.c_str(), true);
        LogicSQLHelper::getInstance().addElixiresUsuario(500);
        UserDefault::getInstance()->flush();
        mReceiver->onCompleteGameComprado();
    }
    
    else if (p.id == LayerShop::COMPRA_1_PISTA) {
        LogicSQLHelper::getInstance().addHintsUsuario(1);
        mReceiver->onHintsComprados(1);
        
    }else if (p.id == LayerShop::COMPRA_3_PISTAS) {
        LogicSQLHelper::getInstance().addHintsUsuario(3);
        mReceiver->onHintsComprados(3);
    }else if (p.id == LayerShop::COMPRA_5_PISTAS) {
        LogicSQLHelper::getInstance().addHintsUsuario(5);
        mReceiver->onHintsComprados(5);
    }else if (p.id == LayerShop::COMPRA_10_PISTAS) {
        LogicSQLHelper::getInstance().addHintsUsuario(10);
        mReceiver->onHintsComprados(10);
    }
    
    else if (p.id == LayerShop::COMPRA_20_ELIXIRES) {
        LogicSQLHelper::getInstance().addElixiresUsuario(20);
         mReceiver->onElixirComprado(20);
    }else if (p.id == LayerShop::COMPRA_60_ELIXIRES) {
        LogicSQLHelper::getInstance().addElixiresUsuario(60);
         mReceiver->onElixirComprado(60);
    }else if (p.id == LayerShop::COMPRA_150_ELIXIRES) {
        LogicSQLHelper::getInstance().addElixiresUsuario(150);
         mReceiver->onElixirComprado(150);
    }else if (p.id == LayerShop::COMPRA_500_ELIXIRES) {
        LogicSQLHelper::getInstance().addElixiresUsuario(500);
        mReceiver->onElixirComprado(500);
    }
    CCLOG("Purchase Success & Saved: %s", p.id.c_str());
    
    
    
}

Sprite* LayerShop::crearSeparador(int numero){
    
    Sprite* separador=Sprite::create("ui/shop/separador_tienda.png");
    std::string textoSeparador;
    if(numero==2){
        textoSeparador=std::string(LanguageManager::getInstance()->getStringForKey("SHOP_HINTS","Hints"));
    }else if(numero==7){
        textoSeparador=std::string(LanguageManager::getInstance()->getStringForKey("SHOP_ELIXIR","Energy Items"));
    }else if(numero==10){
        textoSeparador=std::string(LanguageManager::getInstance()->getStringForKey("SHOP_RESTORE_ITEMS","RESTORE_ITEMS"));
    }
    
    
    
    Label* separadorTextLabel =Label::createWithTTF(textoSeparador.c_str(), CCGetFont(), 40, Size(separador->getContentSize().width*0.55f,0), TextHAlignment::CENTER, TextVAlignment::CENTER);
    
    separadorTextLabel->setPosition(Point(separador->getContentSize().width*0.3f,separador->getContentSize().height+ separadorTextLabel->getContentSize().height*0.5f));
    separador->addChild(separadorTextLabel,4);
    separadorTextLabel->setString(textoSeparador.c_str());
    separadorTextLabel->setColor(Color3B(228,209,149));
    separadorTextLabel->enableOutline(Color4B(0,0,0,255),2);
    
    return separador;
    
}


Sprite* LayerShop::crearItem(bool tamanoxxl,SEL_MenuHandler accion,sdkbox::Product producto,bool compradoPreviamente){
    
    Point posTextoTitulo;
    Point posTextoSubTitulo;
    Point posBotonCompra;
    TextHAlignment alineacion;
    int tituloSize;
    int subtituloSize;
    
    Color4B colorTitulo;
    Color4B colorStrokeTitulo;
    Color3B colorSubitutlo;
    int widthSubtitulo=0;
    
    Sprite* item;
    if(!tamanoxxl){
        item=Sprite::create("ui/shop/item.png");
        posTextoTitulo=Point(item->getContentSize().width*0.42f, item->getContentSize().height*0.73f);
        posTextoSubTitulo=Point(item->getContentSize().width*0.33f, item->getContentSize().height*0.33f);
        posBotonCompra=Point(item->getContentSize().width*0.8f, item->getContentSize().height/2);
        alineacion=TextHAlignment::LEFT;
        tituloSize=23;
        subtituloSize=17;
        colorTitulo=Color4B(255,255,255,255);
        colorSubitutlo=Color3B(58,35,10);
        colorStrokeTitulo=Color4B(0,0,0,255);
        widthSubtitulo=item->getContentSize().width*0.6f;
    }else{
        item=Sprite::create("ui/shop/item_xxl.png");
        posTextoTitulo=Point(item->getContentSize().width*0.5f, item->getContentSize().height*0.73f);
        posTextoSubTitulo=Point(item->getContentSize().width*0.5f, item->getContentSize().height*0.33f);
        posBotonCompra=Point(item->getContentSize().width*0.87f, item->getContentSize().height/2);
        alineacion=TextHAlignment::CENTER;
        tituloSize=30;
        subtituloSize=22;
        colorTitulo=Color4B(86,60,39,255);
        colorSubitutlo=Color3B( 171,8,91);
        colorStrokeTitulo=Color4B(255,255,255,255);
        widthSubtitulo=0;
    }
    
    CCLOG("Product crearItem  id:%s name:%s  pricestr:%s priceFloat:%f  title:%s desc:%s",producto.id.c_str(),producto.name.c_str(),producto.price.c_str(),producto.priceValue, producto.title.c_str(),producto.description.c_str());
    
    std::string tituloStr;
    std::string precioStr;
    std::string descStr;
    
    
    tituloStr=producto.title;
    precioStr= producto.price;
    descStr=producto.description;
    
    if(producto.title==""){
        
        
        if(producto.id==COMPRA_COMPLETE_GAME){
            tituloStr=LanguageManager::getInstance()->getStringForKey("COMPRA_COMPLETE_GAME_TITLE", "Complete Game");
            descStr=LanguageManager::getInstance()->getStringForKey("COMPRA_COMPLETE_GAME_DESC", "");
            
        }else if(producto.id==COMPRA_REMOVE_ADS){
            tituloStr=LanguageManager::getInstance()->getStringForKey("COMPRA_REMOVE_ADS_TITLE", "Complete Game");
            descStr=LanguageManager::getInstance()->getStringForKey("COMPRA_REMOVE_ADS_DESC", "");
        }else if(producto.id==COMPRA_1_PISTA){
            tituloStr=LanguageManager::getInstance()->getStringForKey("COMPRA_1_PISTA_TITLE", "Complete Game");
            descStr=LanguageManager::getInstance()->getStringForKey("COMPRA_1_PISTA_DESC", "");
        }else if(producto.id==COMPRA_3_PISTAS){
            tituloStr=LanguageManager::getInstance()->getStringForKey("COMPRA_3_PISTAS_TITLE", "Complete Game");
            descStr=LanguageManager::getInstance()->getStringForKey("COMPRA_3_PISTAS_DESC", "");
        }else if(producto.id==COMPRA_5_PISTAS){
            tituloStr=LanguageManager::getInstance()->getStringForKey("COMPRA_5_PISTAS_TITLE", "Complete Game");
            descStr=LanguageManager::getInstance()->getStringForKey("COMPRA_5_PISTAS_DESC", "");
        }else if(producto.id==COMPRA_10_PISTAS){
            tituloStr=LanguageManager::getInstance()->getStringForKey("COMPRA_10_PISTAS_TITLE", "Complete Game");
            descStr=LanguageManager::getInstance()->getStringForKey("COMPRA_10_PISTAS_DESC", "");
        }else if(producto.id==COMPRA_20_ELIXIRES){
            tituloStr=LanguageManager::getInstance()->getStringForKey("COMPRA_20_ELIXIRES_TITLE", "Complete Game");
            descStr=LanguageManager::getInstance()->getStringForKey("COMPRA_20_ELIXIRES_DESC", "");
        }else if(producto.id==COMPRA_60_ELIXIRES){
            tituloStr=LanguageManager::getInstance()->getStringForKey("COMPRA_60_ELIXIRES_TITLE", "Complete Game");
            descStr=LanguageManager::getInstance()->getStringForKey("COMPRA_60_ELIXIRES_DESC", "");
        }else if(producto.id==COMPRA_150_ELIXIRES || producto.id=="com.kangaroo.logic.100elixirs"){
            tituloStr=LanguageManager::getInstance()->getStringForKey("COMPRA_150_ELIXIRES_TITLE", "Complete Game");
            descStr=LanguageManager::getInstance()->getStringForKey("COMPRA_150_ELIXIRES_DESC", "");
        }else if(producto.id==COMPRA_500_ELIXIRES){
            tituloStr=LanguageManager::getInstance()->getStringForKey("COMPRA_500_ELIXIRES_TITLE", "Complete Game");
            descStr=LanguageManager::getInstance()->getStringForKey("COMPRA_500_ELIXIRES_DESC", "");
        }
        precioStr="Get!";
        
        
        
    }
    
    std::size_t pos = tituloStr.find("(Logic");      // position of "live" in str
    tituloStr = tituloStr.substr (0,pos);
    
    
    Label* titulo=Label::createWithTTF(tituloStr.c_str(), CCGetFont(), tituloSize);
    titulo->setWidth(item->getContentSize().width*0.7f);
    titulo->setHeight(item->getContentSize().height);
    titulo->setHorizontalAlignment(alineacion);
    titulo->setVerticalAlignment(TextVAlignment::CENTER);
    titulo->setTextColor(colorTitulo);
    titulo->enableOutline(colorStrokeTitulo,2);
    titulo->setPosition(posTextoTitulo);
    item->addChild(titulo,4);
    
    
    Label* subtitulo=Label::createWithTTF(descStr.c_str(), "fonts/CarterOne.ttf", subtituloSize);
    subtitulo->setWidth(widthSubtitulo);
    subtitulo->setHeight(item->getContentSize().height* 0.2f);
    subtitulo->setWidth(item->getContentSize().width*0.6f);
    subtitulo->setAlignment(alineacion,TextVAlignment::CENTER);
    subtitulo->setPosition(posTextoSubTitulo);
    subtitulo->setColor(colorSubitutlo);
    subtitulo->setOverflow(Label::Overflow::SHRINK);
    
    item->addChild(subtitulo);
    
    Boton* comprar=Boton::createBotonWithPriorityAndSwallow(this, accion, "ui/shop/buy_icon_elixir.png",-2,false);
    comprar->setPosition(posBotonCompra);
    if(!compradoPreviamente){
        item->addChild(comprar);
    }
    comprar->setTag(10);
    
    
    Label* compraPrecio=Label::createWithTTF(precioStr.c_str(),  "fonts/CarterOne.ttf", 22);
    compraPrecio->setWidth(comprar->getContentSize().width);
    compraPrecio->setHeight(comprar->getContentSize().height);
    compraPrecio->setHorizontalAlignment(TextHAlignment::LEFT);
    compraPrecio->setVerticalAlignment(TextVAlignment::CENTER);
    compraPrecio->setOverflow(Label::Overflow::SHRINK);
    compraPrecio->enableOutline(Color4B(0,0,0,255),2);
    compraPrecio->setPosition(Point(comprar->getContentSize().width*0.84f,comprar->getContentSize().height/2));
    comprar->addChild(compraPrecio,5);
    if(compradoPreviamente){
        
        compraPrecio->setString(LanguageManager::getInstance()->getStringForKey("COMPRADO"));
    }
    
    
    
    Sprite* extra=NULL;
    bool creadoExtra=false;
    if(producto.id==mProducto20Elixir.id && producto.id!=""){
        extra=Sprite::create("ui/shop/treasure_20.png");
        creadoExtra=true;
    }else if(producto.id==mProducto60Elixir.id && producto.id!=""){
        extra=Sprite::create("ui/shop/treasure_60.png");
        creadoExtra=true;
    }else if(producto.id==mProducto150Elixir.id && producto.id!=""){
        extra=Sprite::create("ui/shop/treasure_150.png");
        creadoExtra=true;
    }else if(producto.id==mProducto500Elixir.id && producto.id!=""){
        extra=Sprite::create("ui/shop/treasure_500.png");
        creadoExtra=true;
    }
    
    if(creadoExtra){
        extra->setPosition(Point(extra->getContentSize().width/2 + extra->getContentSize().width*0.2f , item->getContentSize().height/2));
        item->addChild(extra);
        Sprite* disccountExtra=Sprite::create("ui/shop/disccount.png");
        disccountExtra->setPosition(Point(subtitulo->getPositionX()-subtitulo->getContentSize().width*0.7f , subtitulo->getPositionY()));
        item->addChild(disccountExtra);
    }
    
    
    
    
    
    
    
    return item;
    
    
}



void LayerShop::onKeyBackClicked()
{
    this->close(NULL);
    
}






//EVENTOS IAP





void LayerShop::onRequestIAP()
{
    sdkbox::IAP::refresh();
}

void LayerShop::onInitialized(bool success)
{
    CCLOG("IAP initialized");
}



void LayerShop::onSuccess(const Product &p)
{
    CCLOG("Purchase onSuccess: %s", p.id.c_str());
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/hint.mp3",false);
    OnSuccesPurchase(p);
    try{
        sdkbox::PluginGoogleAnalytics::logEvent("COMPRA","LevelsBase", p.id,0);
        
    }catch(...){
        //no hago na
    }
}


void LayerShop::onFailure(const Product &p, const std::string &msg)
{
    CCLOG("Purchase Failed: %s", msg.c_str());
    LayerInfo::mostrar("The in-app purchase failed");
    
}

void LayerShop::onCanceled(const Product &p)
{
    CCLOG("Purchase Canceled: %s", p.id.c_str());
}

void LayerShop::onRestored(const Product& p)
{
    CCLOG("Purchase Restored: %s", p.name.c_str());
    LayerShop::OnSuccesPurchase(p);
    
}

void LayerShop::updateIAP(const std::vector<sdkbox::Product>& products)
{
    _products = products;
    if(LayerShop::EstaMostrado())
    {
        LayerShop::getInstance()->cargarProductos(_products);
    }
}

void LayerShop::onProductRequestSuccess(const std::vector<Product> &products)
{
    updateIAP(products);
}

void LayerShop::onProductRequestFailure(const std::string &msg)
{
    CCLOG("Fail to load products %s", msg.c_str());
    
}

void LayerShop::onRestoreComplete(bool ok, const std::string &msg)
{
    CCLOG("Restore complete");
    
    if (ok)
    {
        CCLOG("Restore Successful: %s", msg.c_str());
        // LayerShop::ocultar(NULL);
    }
    else
    {
        CCLOG("Restore failed: %s", msg.c_str());
    }
}

void LayerShop::onIAP(sdkbox::Product p)
{
    CCLOG("Start IAP %s", p.name.c_str());
    IAP::purchase(p.name);
}


void LayerShop::onRestoreIAP()
{
    IAP::restore();
}





template <typename T> std::string LayerShop::tostr(const T& t) { std::ostringstream os; os<<t; return os.str(); }
