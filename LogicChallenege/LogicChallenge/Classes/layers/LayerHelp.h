//
//  LayerHelp.h
//  trivial
//
//  Created by Xabier on 25/03/13.
//
//

#ifndef __trivial__LayerHelp__
#define __trivial__LayerHelp__

#include <iostream>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "LayerBase.h"


USING_NS_CC;

USING_NS_CC_EXT;

class LayerHelp:  public LayerBase
{
    public:
    
    virtual ~LayerHelp(void);
    static LayerHelp* initLayer(std::string texto);
    Rect rect();
   
    virtual bool init() override;
  
    CREATE_FUNC(LayerHelp);
    
    void dibujarContent();
    
    static void ocultar(Ref* pSender);
    void eliminar(Ref* pSender);
    static void mostrar(std::string texto,Boton* mbotonHelp);
    static int TAG_HELP;
    void setTexto(std::string texto);
    void onKeyBackClicked();
    Boton* mbotonHelp;
private:
    
    void close(Ref* sender);
    Label* mTexto;
    Boton* mClose;
    ScrollView* mScroll;
    std::string mTextoHelp;
    


};

#endif /* defined(__trivial__LayerHelp__) */
