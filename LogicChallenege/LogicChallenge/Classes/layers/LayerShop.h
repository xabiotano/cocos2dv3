//
//  LayerShop.h
//  trivial
//
//  Created by Xabier on 25/03/13.
//
//

/*SIN ADS!!!*/

#ifndef __trivial__LayerShop__
#define __trivial__LayerShop__

#include <iostream>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "LayerBase.h"
#include "PluginIAP/PluginIAP.h"
#include "ILayerShop.h"
USING_NS_CC;

USING_NS_CC_EXT;

class LayerShop:  public LayerBase,public ScrollViewDelegate,public sdkbox::IAPListener
{
public:
    
    template <typename T> std::string tostr(const T& t);
    
    virtual ~LayerShop(void);
    Rect rect();
    
    virtual bool init() override;
    // 'layer' is an autorelease object
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(LayerShop);
    
    void dibujarContent();
    
    static void ocultar(Ref* pSender);
    static LayerShop* mostrar(ILayerShop* receiver);
    EventListenerTouchOneByOne* mListener;
    virtual bool onTouchBegan(Touch* touch, Event* event) override;
    virtual void onTouchMoved(Touch* touch, Event* event) override;
    virtual void onTouchEnded(Touch* touch, Event* event) override;
    
    
    void cargarProductos(const std::vector<sdkbox::Product>& products);
    static const  int TAG_SHOP=1928;
    static bool EstaMostrado();
    
    virtual void scrollViewDidScroll(ScrollView* view) override;
    virtual void scrollViewDidZoom(ScrollView* view) override{};
    
    static std::string COMPRA_COMPLETE_GAME;
    static std::string COMPRA_REMOVE_ADS;
    static std::string COMPRA_1_PISTA;
    static std::string COMPRA_3_PISTAS;
    static std::string COMPRA_5_PISTAS;
    static std::string COMPRA_10_PISTAS;
    static std::string COMPRA_20_ELIXIRES;
    static std::string COMPRA_60_ELIXIRES;
    static std::string COMPRA_150_ELIXIRES;
    static std::string COMPRA_500_ELIXIRES;
    
    void OnSuccesPurchase(sdkbox::Product const& p);
    void onKeyBackClicked();
    static LayerShop* getInstance();
    std::vector<sdkbox::Product> _products;
    static void releaseLayer();

private:
    void setReceiver(ILayerShop* receiver);
    ILayerShop* mReceiver;
    static LayerShop* mInstance;
    void close(Ref* sender);
    Boton* mClose;
    ScrollView* mScroll;
    Sprite* mFondo;
    LayerColor* mContenido;
    
    
    sdkbox::Product mProducto1Hint;
    sdkbox::Product mProducto3Hint;
    sdkbox::Product mProducto5Hint;
    sdkbox::Product mProducto10Hint;
    
    sdkbox::Product mProductoComplete;
    sdkbox::Product mProductoRemoveAds;
    
    sdkbox::Product mProducto20Elixir;
    sdkbox::Product mProducto60Elixir;
    sdkbox::Product mProducto150Elixir;
    sdkbox::Product mProducto500Elixir;
    
    void Buy(sdkbox::Product producto);
    Label* mNumeroPistas;
    void Buy1(Ref* sender);
    void Buy2(Ref* sender);
    void Buy3(Ref* sender);
    void Buy4(Ref* sender);
    void Buy5(Ref* sender);
    void Buy6(Ref* sender);
    void Buy7(Ref* sender);
    
    void Buy8(Ref* sender);
    void Buy9(Ref* sender);
    void Buy10(Ref* sender);
    
    void RestoreInApps(Ref* sender);
    void BuyYaComprado(Ref* sender);
    
    Sprite* mScrollIndicator;
    Sprite* mScrollIndicatorBullet;
    Sprite* crearSeparador(int numero);
    Sprite* crearItem(bool tamanoxxl,SEL_MenuHandler accion,sdkbox::Product producto,bool compradoPreviamente);
    
    
    //IAPListener
    
    void onIAP(sdkbox::Product p);
    
    void onRequestIAP();
    void onRestoreIAP();
    void updateIAP(const std::vector<sdkbox::Product>& products);
    virtual void onInitialized(bool ok) override;
    virtual void onSuccess(sdkbox::Product const& p) override;
    virtual void onFailure(sdkbox::Product const& p, const std::string &msg) override;
    virtual void onCanceled(sdkbox::Product const& p) override;
    virtual void onRestored(sdkbox::Product const& p) override;
    virtual void onProductRequestSuccess(std::vector<sdkbox::Product> const &products) override;
    virtual void onProductRequestFailure(const std::string &msg) override;
    void onRestoreComplete(bool ok, const std::string &msg) override;
    
    
    
    
    
    
    
};

#endif
