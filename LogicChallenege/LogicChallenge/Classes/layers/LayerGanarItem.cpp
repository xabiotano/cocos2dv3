#include "SimpleAudioEngine.h"
#include "LayerGanarItem.h"
#include "../widgets/Boton.h"
#include "../Levels.h"
#include "../series/SeriesScene.h"
#include "../Variables.h"
#include "../widgets/NivelItem.h"
#include "../helpers/LogicSQLHelper.h"
template <typename T> std::string LayerGanarItem::tostr(const T& t) { std::ostringstream os; os<<t; return os.str(); }



LayerGanarItem:: ~LayerGanarItem(void){}



LayerGanarItem* LayerGanarItem::initLayer(int numItems)
{
    LayerGanarItem *layer = LayerGanarItem::create();
    layer->dibujarItems(numItems);
    layer->mNumeroItems=numItems;
    layer->mSwallowTouches=true;
    return layer;
}


void LayerGanarItem::dibujarItems(int numItems ){
   //dibujo fondo
    mVisibleSize=Director::getInstance()->getVisibleSize();
    
    //relleno los items
    int numero=LogicSQLHelper::getInstance().getElixiresGanados();
    std::string numeroItems=tostr(numero);
    tag1=380;
    tag2_1=381;
    tag2_2=382;
    tag3_1=383;
    tag3_2=384;
    tag3_3=385;
    
    primerUpdate=true;
    if(numItems==1){
        mRuleta=Sprite::create("ui/layerganaritem/fondo_ruleta_2.png");
        mRuleta->setScale(0.7f);
    }
    else if(numItems==2){
         mRuleta=Sprite::create("ui/layerganaritem/fondo_ruleta_2.png");
    }else if(numItems==3){
         mRuleta=Sprite::create("ui/layerganaritem/fondo_ruleta_3.png");
    }
    mRuleta->setPosition(Point(mVisibleSize.width/2,mVisibleSize.height/2));
    this->addChild(mRuleta);
    Size sizeRuleta=mRuleta->getContentSize();
    
    Texture2D * itemTex;
    itemTex =Director::getInstance()->getTextureCache()->addImage("elixircomun/elixir_m_relieve.png");
    
   
    float scale=0.8f;
    
    if(numItems==1){
        Sprite* elixir1_Izquierda_1=Sprite::createWithTexture(itemTex);
        elixir1_Izquierda_1->setPosition(Point(sizeRuleta.width*0.5f, sizeRuleta.height*0.5f));
        mRuleta->addChild(elixir1_Izquierda_1);
        elixir1_Izquierda_1->setRotation(-15);
        elixir1_Izquierda_1->setTag(tag1);

    }
    else if(numItems==2){
        
        //Izquierda 2
        Sprite* elixir2_Izquierda_1=Sprite::createWithTexture(itemTex);
        elixir2_Izquierda_1->setPosition(Point(sizeRuleta.width*0.2f, sizeRuleta.height*0.5f));
        mRuleta->addChild(elixir2_Izquierda_1);
        elixir2_Izquierda_1->setRotation(-30);
        elixir2_Izquierda_1->setTag(tag2_1);
        
        Sprite* cafe2_Izquierda_2=Sprite::createWithTexture(itemTex);
        cafe2_Izquierda_2->setPosition(Point(sizeRuleta.width*0.32f, sizeRuleta.height*0.5f));
        mRuleta->addChild(cafe2_Izquierda_2);
        cafe2_Izquierda_2->setTag(tag2_2);

        

        
        //derecha 1
        Sprite* elixir2_Derecha_1=Sprite::createWithTexture(itemTex);
        elixir2_Derecha_1->setPosition(Point(sizeRuleta.width*0.7f, sizeRuleta.height*0.5f));
        mRuleta->addChild(elixir2_Derecha_1);
        elixir2_Derecha_1->setRotation(-15);
        elixir2_Derecha_1->setTag(tag1);
        
        elixir2_Izquierda_1->setScale(scale);
        cafe2_Izquierda_2->setScale(scale);
        elixir2_Derecha_1->setScale(scale);

        
    }else if(numItems==3){
        Sprite* elixir3_Izquierda_1=Sprite::createWithTexture(itemTex);
        elixir3_Izquierda_1->setPosition(Point(sizeRuleta.width*0.2f, sizeRuleta.height*0.55f));
        mRuleta->addChild(elixir3_Izquierda_1);
            
        Sprite* elixir3_Izquierda_2=Sprite::createWithTexture(itemTex);
        elixir3_Izquierda_2->setPosition(Point(sizeRuleta.width*0.25f, sizeRuleta.height*0.65f));
        mRuleta->addChild(elixir3_Izquierda_2);
        
        Sprite* elixir3_Izquierda_3=Sprite::createWithTexture(itemTex);
        elixir3_Izquierda_3->setPosition(Point(sizeRuleta.width*0.34f, sizeRuleta.height*0.74f));
        mRuleta->addChild(elixir3_Izquierda_3);
    
        elixir3_Izquierda_3->setScale(scale);
        elixir3_Izquierda_2->setScale(scale);
        elixir3_Izquierda_1->setScale(scale);
        
        elixir3_Izquierda_3->setTag(tag3_1);
        elixir3_Izquierda_2->setTag(tag3_2);
        elixir3_Izquierda_1->setTag(tag3_3);
        
        
        
        
        elixir3_Izquierda_3->setRotation(-60);
        elixir3_Izquierda_2->setRotation(-80);
        elixir3_Izquierda_1->setRotation(-100);
        
        
        
        
        Sprite* elixir3_Abajo_1=Sprite::createWithTexture(itemTex);
        elixir3_Abajo_1->setPosition(Point(sizeRuleta.width*0.43f, sizeRuleta.height*0.3f));
        mRuleta->addChild(elixir3_Abajo_1);
        Sprite* cafe3_Abajo_2=Sprite::createWithTexture(itemTex);
        cafe3_Abajo_2->setPosition(Point(sizeRuleta.width*0.55f, sizeRuleta.height*0.3f));
        mRuleta->addChild(cafe3_Abajo_2);
        elixir3_Abajo_1->setRotation(-30);

        elixir3_Abajo_1->setTag(tag2_1);
        cafe3_Abajo_2->setTag(tag2_2);
            
        Sprite* elixir3_Derecha_1=Sprite::createWithTexture(itemTex);
        elixir3_Derecha_1->setPosition(Point(sizeRuleta.width*0.7f, sizeRuleta.height*0.6f));
        mRuleta->addChild(elixir3_Derecha_1);
        
        elixir3_Derecha_1->setRotation(-20);
        elixir3_Derecha_1->setTag(tag1);
        
        elixir3_Izquierda_3->setScale(scale);
        elixir3_Izquierda_2->setScale(scale);
        elixir3_Izquierda_1->setScale(scale);
        elixir3_Abajo_1->setScale(scale);
        cafe3_Abajo_2->setScale(scale);
        elixir3_Derecha_1->setScale(scale);

        
    }
    if(numItems!=1){
        mFlecha=Sprite::create("ui/layerganaritem/flecha.png");
        mFlecha->setPosition(Point(mRuleta->getContentSize().width/2,mRuleta->getContentSize().height/2));
        mFlecha->setAnchorPoint(Point(mFlecha->getContentSize().width *0.43f/mFlecha->getContentSize().width,mFlecha->getContentSize().height*0.14f/mFlecha->getContentSize().height));
        mRuleta->addChild(mFlecha);
        mRuleta->setScale(0.7f);
        mIndiceRotacion=2500.f;
        mPaso=-1;
        mSonidoRuleta= CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/ui/loopspin.mp3",true);
        this->scheduleUpdate();
    }else{
        mGanados=1;
        ganarItem(NULL);
    }
    
  
}



void LayerGanarItem::iniciarSorteo(float dt){
    
    if( mPaso==0){
        mPaso++;
        mFlecha->stopAllActions();
        this->scheduleOnce(schedule_selector(LayerGanarItem::iniciarSorteo), 0.01f);
        return;
    }
   
    if(mIniciada){
        return;
    }
    
    CocosDenshion::SimpleAudioEngine::getInstance()->stopEffect(mSonidoRuleta);
    mIniciada=true;
    srand(time(0));
    mAngle= rand() % 360;
    int numerovueltas= rand() % 5 + 10;
    RotateTo* rotar= RotateTo::create(3, 360*numerovueltas + mAngle);
    CallFunc *finRotacionF = CallFunc::create([this]() { this->finRotacion(); });
    mFlecha->runAction(Sequence::createWithTwoActions(EaseOut::create(rotar, 2),finRotacionF));
                       
}

void LayerGanarItem::finRotacion(){
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/ui/ruleta_elegido.mp3");
    mFlecha->runAction(Sequence::create(EaseInOut::create(ScaleTo::create(0.2f, 1.2f), 3),EaseInOut::create(ScaleTo::create(0.2f, 1.f), 3),NULL ));
    CocosDenshion::SimpleAudioEngine::getInstance()->stopEffect(mSonidoRuleta);
    mFlecha->runAction(Sequence::createWithTwoActions(ScaleTo::create(0.2f, 1.2f), ScaleTo::create(0.2f, 1.f)));
    float angle=mFlecha->getRotation();
   
    
    CCLOG("mNumeroItems %i angle:%f realAngle:%i",mNumeroItems,angle,mAngle);
    if(mNumeroItems==1){
        mGanados=1;
        if(mRuleta->getChildByTag(tag1)!=NULL){
              mRuleta->getChildByTag(tag1)->runAction(Sequence::create(EaseInOut::create(ScaleTo::create(0.2f, 1.2f), 3),EaseInOut::create(ScaleTo::create(0.2f, 1.f), 3),NULL ));
        }
      
    }
    else if(mNumeroItems==2){
        if(angle<=180){
             mGanados=1;
            if(mRuleta->getChildByTag(tag1)!=NULL){
                mRuleta->getChildByTag(tag1)->runAction(Sequence::create(EaseInOut::create(ScaleTo::create(0.2f, 1.2f), 3),EaseInOut::create(ScaleTo::create(0.2f, 1.f), 3),NULL ));
            }
        }else{
             mGanados=2;
            if(mRuleta->getChildByTag(tag2_1)!=NULL){
               mRuleta->getChildByTag(tag2_1)->runAction(Sequence::create(EaseInOut::create(ScaleTo::create(0.2f, 1.2f), 3),EaseInOut::create(ScaleTo::create(0.2f, 1.f), 3),NULL ));
            }
            if(mRuleta->getChildByTag(tag2_2)!=NULL){
               mRuleta->getChildByTag(tag2_2)->runAction(Sequence::create(EaseInOut::create(ScaleTo::create(0.2f, 1.2f), 3),EaseInOut::create(ScaleTo::create(0.2f, 1.f), 3),NULL ));
            }
        }
        
        
    }else if(mNumeroItems==3){
        if(angle>=0 && angle<=120){
            mGanados=1;
            if(mRuleta->getChildByTag(tag1)!=NULL){
                mRuleta->getChildByTag(tag1)->runAction(Sequence::create(EaseInOut::create(ScaleTo::create(0.2f, 1.2f), 3),EaseInOut::create(ScaleTo::create(0.2f, 1.f), 3),NULL ));
            }
        }else if(angle>120 && angle<240)
        {
            mGanados=2;
            if(mRuleta->getChildByTag(tag2_1)!=NULL){
                mRuleta->getChildByTag(tag2_1)->runAction(Sequence::create(EaseInOut::create(ScaleTo::create(0.2f, 1.2f), 3),EaseInOut::create(ScaleTo::create(0.2f, 1.f), 3),NULL ));
            }
            if(mRuleta->getChildByTag(tag2_2)!=NULL){
                mRuleta->getChildByTag(tag2_2)->runAction(Sequence::create(EaseInOut::create(ScaleTo::create(0.2f, 1.2f), 3),EaseInOut::create(ScaleTo::create(0.2f, 1.f), 3),NULL ));
            }
        }
        else if( angle>=240)
        {
            mGanados=3;
            if(mRuleta->getChildByTag(tag3_1)!=NULL){
                mRuleta->getChildByTag(tag3_1)->runAction(Sequence::create(EaseInOut::create(ScaleTo::create(0.2f, 1.2f), 3),EaseInOut::create(ScaleTo::create(0.2f, 1.f), 3),NULL ));
            }
            if(mRuleta->getChildByTag(tag3_2)!=NULL){
                mRuleta->getChildByTag(tag3_2)->runAction(Sequence::create(EaseInOut::create(ScaleTo::create(0.2f, 1.2f), 3),EaseInOut::create(ScaleTo::create(0.2f, 1.f), 3),NULL ));
            }
            if(mRuleta->getChildByTag(tag3_3)!=NULL){
                mRuleta->getChildByTag(tag3_3)->runAction(Sequence::create(EaseInOut::create(ScaleTo::create(0.2f, 1.2f), 3),EaseInOut::create(ScaleTo::create(0.2f, 1.f), 3),NULL ));
            }
        }

    }
    
    if(mGanados>0){
        //macahco lo que hubiera por las estrellas de verdad
        LogicSQLHelper::getInstance().setAnimacionDesbloqueoPendiente(true,Variables::LEVELS_ELIXIR_CLICKADO,mGanados);
        CallFuncN* funcion= CallFuncN::create(CC_CALLBACK_1(LayerGanarItem::ganarItem,this));
        this->runAction(Sequence::create(DelayTime::create(0.7f), funcion,NULL));

       // this->scheduleOnce(schedule_selector(LayerGanarItem::ganarItem), 0.7f);
    }
    
}

// on "init" you need to initialize your instance
bool LayerGanarItem::init()
{
    mSize = Director::getInstance()->getWinSize();
    if (!LayerBase::initWithColor(Color4B(0,0,0,200))){
        return false;
    }
    mIniciada=false;
    this->setKeypadEnabled(true);
    
    return true;
}



void LayerGanarItem::ganarItem(Ref* sender){
    CCLOG("%i ganados %i ",mGanados,mAngle);
    int itemsGanadosPre= LogicSQLHelper::getInstance().getElixiresGanados();
    mDesbloqueo=false;
    BaseScene::getInstance()->setStars(mGanados);
    itemsGanadosRestantes=mGanados;
    CallFuncN* funcion= CallFuncN::create(CC_CALLBACK_1(LayerGanarItem::animarPanelClose,this));
    this->runAction(Sequence::create(DelayTime::create(1.f), funcion,NULL));

    
    //mSelectorGanarItem=schedule_selector(LayerGanarItem::animarPanelClose);
    //this->scheduleOnce(mSelectorGanarItem, 1.f);
    mPaso=4;
}


void LayerGanarItem::animarPanelClose(Ref* sender){
    irANiveles();
}

void LayerGanarItem::irANiveles(){
    if( Director::getInstance()->getRunningScene()->getChildByTag(LayerGanarItem::TAG_GANAR_ITEM)){
        Director::getInstance()->getRunningScene()->removeChildByTag(LayerGanarItem::TAG_GANAR_ITEM,true);
        BaseScene::getInstance()->Success();
    }
}






bool LayerGanarItem::onTouchBegan(Touch* touch, Event* event)
{
    if(mPaso==-1){
        mPaso++;
         CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/ui/ruleta_stop.mp3");
    }
    if(mPaso==4){
        irANiveles();
    }
    return true;

    
}



void LayerGanarItem::close(Ref* sender)
{
    this->getParent()->removeChild(this);
}



void LayerGanarItem::mostrar(int numItems)
{
    
    if(!Director::getInstance()->getRunningScene()){
        return;
    }
    if(!Director::getInstance()->getRunningScene()->getChildByTag(LayerGanarItem::TAG_GANAR_ITEM)){
        LayerGanarItem* layer= LayerGanarItem::initLayer(numItems);
        layer->setContentSize( Director::getInstance()->getWinSize());
        layer->setTag(LayerGanarItem::TAG_GANAR_ITEM);
        Director::getInstance()->getRunningScene()->addChild(layer,LayerBase::ZINDEX_ALTO);
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/ui/mostrar_ruleta.mp3");
    }
    else{
        CCLOG("LayerGanarItem previamente mostrada");
    }
}

void LayerGanarItem::update(float dt){
    float startAngle_ =mFlecha->getRotation();
    if(mPaso==0 && primerUpdate){
       
            primerUpdate=false;
            if(mNumeroItems==3){
                float random= CCRANDOM_0_1();
                if(random>0.15f){
                    mFlecha->setRotation(230.f);
                    mIndiceRotacion=2100.f;
                }
               
            }
            CCLOG("startAngle %f", mFlecha->getRotation());
    }
    startAngle_ =mFlecha->getRotation();
    if (startAngle_ > 0)
        startAngle_ = fmodf(startAngle_, 360.0f);
    else
        startAngle_ = fmodf(startAngle_, -360.0f);
    
    
    
    mFlecha->setRotation(startAngle_+ dt*mIndiceRotacion);
    
    if(mPaso==0){
        if(mIndiceRotacion>0){
            mIndiceRotacion-=1200*dt;
        }else{
            mIndiceRotacion=0;
            mPaso++;
            CCLOG("FIN ROTACION");
            finRotacion();
            
           
        }
    }
}







