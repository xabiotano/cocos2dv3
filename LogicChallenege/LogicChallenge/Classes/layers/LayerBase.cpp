#include "LayerBase.h"
#include "../Levelsv2.h"
#include "../Variables.h"
#include "../Levels.h"
#include "LayerShop.h"



// on "init" you need to initialize your instance
bool LayerBase::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !LayerColor::initWithColor(Color4B(0,0,0,0),  Director::getInstance()->getVisibleSize().width,  Director::getInstance()->getVisibleSize().height ))
    {
        return false;
    }
    addEventListener();
    return true;
}

bool LayerBase::initWithColor(Color4B color)
{
    if ( !LayerColor::initWithColor(color,  Director::getInstance()->getVisibleSize().width,  Director::getInstance()->getVisibleSize().height ))
    {
        return false;
    }
    addEventListener();
    return true;
}

bool LayerBase::initWithPriority(int priority)
{
    mPriority=priority;
    if (!LayerBase::init())
    {
        return false;
    }
    return true;
}

bool LayerBase::initWithPriority(Color4B color, int priority)
{
    if ( !LayerColor::initWithColor(color,  Director::getInstance()->getVisibleSize().width,  Director::getInstance()->getVisibleSize().height ))
    {
        return false;
    }
    mPriority=priority;
    addEventListener();
    return true;

}



void LayerBase::Menu(Ref* sender){
    Scene* scene;
    if(mTipoNivel==TipoNivel(kNivelLogica)){
        scene=Levels::createScene();
    }else{
          scene=Levelsv2::scene();
    }
    Director *pDirector = Director::getInstance();
    pDirector->replaceScene(TransitionCrossFade::create(0.5f, scene));
    CCLOG("NEXT");
}



void LayerBase::Comprar(Ref* sender){
    LevelsBase* level= (LevelsBase*) Director::getInstance()->getRunningScene();
    level->Shop(NULL);
}

void LayerBase::Reload(Ref* sender){
    BaseScene::getInstance()->Retry(NULL);
}

bool LayerBase::containsTouchLocation(Touch* touch)
{
    return  Rect(-mSize.width / 2, -mSize.height / 2, mSize.width, mSize.height).containsPoint(convertTouchToNodeSpaceAR(touch));
}

bool LayerBase::onTouchBegan(Touch* touch, Event* event)
{
    return mSwallowTouches;
   
}
void LayerBase::addEventListener(){
    mListener = EventListenerTouchOneByOne::create();
    mListener->setSwallowTouches(mSwallowTouches);
    mListener->onTouchBegan = CC_CALLBACK_2(LayerBase::onTouchBegan, this);
    if(mPriority!=-1){
        this->getEventDispatcher()->addEventListenerWithFixedPriority( mListener,mPriority);
    }else{
        this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(mListener,  this);
    }

}


void LayerBase::onExit(){
    this->stopAllActions();
    this->getEventDispatcher()->removeEventListener(mListener);
    LayerColor::onExit();
}

