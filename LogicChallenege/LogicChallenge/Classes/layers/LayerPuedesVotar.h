//
//  LayerPuedesVotar.h
//  trivial
//
//  Created by Xabier on 25/03/13.
//
//

#ifndef __trivial__LayerPuedesVotar__
#define __trivial__LayerPuedesVotar__

#include <iostream>
#include "cocos2d.h"
#include "LayerBase.h"
#include "cocos-ext.h"
#include "PluginReview/PluginReview.h"


USING_NS_CC_EXT;
USING_NS_CC;


class LayerPuedesVotar: public LayerBase, public sdkbox::ReviewListener
{
    public:
       template <typename T> std::string tostr(const T& t);
        const int TAG_FONDO=11;
        static const int TAG_LAYER_PUEDESVOTAR=3922381;
    
        virtual ~LayerPuedesVotar(void);
        static LayerPuedesVotar* get_instance();
        static LayerPuedesVotar* initLayer(int numeroElixirReq);
        static LayerPuedesVotar* mostrar(int numeroElixirReq);
        Rect rect();
        // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
        virtual bool init();
        // 'layer' is an autorelease object
        // preprocessor macro for "static create()" constructor ( node() deprecated )
        CREATE_FUNC(LayerPuedesVotar);

        Size mSize;
    
        void dibujarItems();
        void onKeyBackClicked();
        static void ocultar(Ref* pSender);

    private:
        void ok(Ref* sender);
        int mNumeroElixirReq;
        void close(Ref* sender);
        int mNumeroNivel;
       // void Next();
        Scale9Sprite* mFondo_panel;
    
    /*RATE*/
    void onDisplayAlert();
    void onDeclineToRate();
    void onRate();
    void onRemindLater();
    
    
    
    
    
        
    
};

#endif /* defined(__trivial__LayerPuedesVotar__) */
