//
//  LayerDesbloqueadoNivel.h
//  trivial
//
//  Created by Xabier on 25/03/13.
//
//

#ifndef __trivial__LayerDesbloqueadoNivel__
#define __trivial__LayerDesbloqueadoNivel__

#include <iostream>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "LayerBase.h"


USING_NS_CC;

USING_NS_CC_EXT;

class LayerDesbloqueadoNivel:  public LayerBase
{
    public:
    
    static LayerDesbloqueadoNivel* initLayer();
    Rect rect();
   
    virtual bool init();
    // 'layer' is an autorelease object
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(LayerDesbloqueadoNivel);
    
    void dibujarContent();
    
    static void ocultar(Ref* pSender);
    static LayerDesbloqueadoNivel* mostrar();

    
    static int TAG_DESBLOQUEADO;

    void irALogicMode(Ref* sender);
    void onKeyBackClicked();

   
    
private:
    void close(Ref* sender);
    Boton* mClose;
    
    void moverPopUp();
   


    


};

#endif 
