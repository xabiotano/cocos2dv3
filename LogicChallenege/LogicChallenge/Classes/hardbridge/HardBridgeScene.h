#ifndef __HardBridgeScene_SCENE_H__
#define __HardBridgeScene_SCENE_H__

#include "cocos2d.h"
#include "../BaseScene.h"
#include "../widgets/Boton.h"
#include "Protagonista.fwd.h"
#include "Barca.fwd.h"
#include "LayerSituacionNoOk.h"
#include "TipoSituacionProblemaTypes.h"

USING_NS_CC;


class HardBridgeScene : public BaseScene
{
    
    
public:
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init();
    virtual ~HardBridgeScene(void);

    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static Scene* scene();
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(HardBridgeScene);
    static HardBridgeScene* getInstance();
    void iniciarScene();
    
    virtual void checkFinish();
    virtual void Hint(Ref* sender);
    virtual void HintCallback(bool accepted);
    virtual void HintStart(Ref* sender);
    virtual void TutorialStart(){};
    virtual void Retry(Ref* sender);
    void barcaClick();
    Protagonista* mPerro,*mGranjero,*mPato1,*mPato2,*mCastor1,*mCastor2,*mNino,*mNina;
    Barca* mBarca;
   
    
    bool isBarcaMoving();
    bool estaBarcaIzquierda();
    
    bool esMovimientoValido();
    
   
    bool sonTodoAnimalesMontados();
    
    void hintFollowed(Sprite* HintNodeToTouch);
    int mHintStep;
    int getHumanosMontados();
    void finMoveBarca();
     void onEnterTransitionDidFinish();
    
    
    void HintStartTemp(float dt);
};


#endif // __CanibalsScene_SCENE_H__
