#include "HardBridgeScene.h"
#include "SimpleAudioEngine.h"
#include "../layers/LayerGameOver.h"
#include "../layers/LayerSuccess.h"
#include "../Variables.h"
#include "../Levels.h"
#include "Protagonista.h"
#include "Barca.h"

using namespace cocos2d;
using namespace CocosDenshion;


HardBridgeScene* empty_instanciaHard;


HardBridgeScene::~HardBridgeScene(void){
  CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
}

Scene* HardBridgeScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    // 'layer' is an autorelease object
    HardBridgeScene *layer = HardBridgeScene::create();
    // add layer as a child to scene
    scene->addChild(layer);
     // return the scene
    return scene;
}

bool HardBridgeScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !BaseScene::init() )
    {
        return false;
    }
    empty_instanciaHard=this;
    mNombrePantalla="HardBridgeScene";
    mTipoNivel=kNivelLogica;
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    Sprite* mFondo = Sprite::create("hardbridge/fondo.png");
    // position the sprite on the center of the screen
    mFondo->setPosition(Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    
    this->addChild(mFondo, 0);
    float scalex=Director::getInstance()->getVisibleSize().width/mFondo->getContentSize().width;
    float scaley=Director::getInstance()->getVisibleSize().height/mFondo->getContentSize().height;
    if(scalex>scaley){
        scaley=scalex;
    }
    mFondo->setScale(scaley);
    
    iniciarScene();
    

    mTextoHelp=std::string(LanguageManager::getInstance()->getStringForKey("HELP_HARDBRIDGE", "HELP_HARDBRIDGE"));

    
    return true;
}


void HardBridgeScene::iniciarScene(){
    
    this->removeChild(mBarca);
    this->removeChild(mPerro);
    this->removeChild(mGranjero);
    this->removeChild(mNino);
    this->removeChild(mNina);
    this->removeChild(mCastor1);
    this->removeChild(mCastor2);
    this->removeChild(mPato1);
    this->removeChild(mPato2);
    
    mPerro=Protagonista::create(this, TipoProtagonista(kPerro));
    mNino=Protagonista::create(this, TipoProtagonista::kNino);
    mNina=Protagonista::create(this, TipoProtagonista::kNina);
    mCastor1=Protagonista::create(this, TipoProtagonista::kCastor1);
    mCastor2=Protagonista::create(this, TipoProtagonista::kCastor2);
    mPato1=Protagonista::create(this, TipoProtagonista::kPato1);
    mPato2=Protagonista::create(this, TipoProtagonista::kPato2);
    mGranjero=Protagonista::create(this, TipoProtagonista::kGranjero);
    mBarca=Barca::create(this);
    
    
    
    

    
    this->addChild(mBarca);
    this->addChild(mPerro);
    this->addChild(mGranjero);
    this->addChild(mNino);
    this->addChild(mNina);
    this->addChild(mCastor1);
    this->addChild(mCastor2);
    this->addChild(mPato1);
    this->addChild(mPato2);
    
    
    
    this->reorderChild(mPerro, mPerro->mZIndex);
    this->reorderChild(mGranjero, mGranjero->mZIndex);
    this->reorderChild(mNino, mNino->mZIndex);
    this->reorderChild(mNina, mNina->mZIndex);
    this->reorderChild(mCastor1, mCastor1->mZIndex);
    this->reorderChild(mCastor2, mCastor2->mZIndex);
    this->reorderChild(mPato1, mPato1->mZIndex);
    this->reorderChild(mPato2, mPato2->mZIndex);
    this->reorderChild(mBarca, 3);

}

void HardBridgeScene::barcaClick(){
    
}


bool HardBridgeScene::isBarcaMoving(){
    return mBarca->mIsMoving;
}
bool HardBridgeScene::estaBarcaIzquierda(){
    return mBarca->estaEnIzquierda();
}


HardBridgeScene* HardBridgeScene::getInstance(){
    return empty_instanciaHard;
};

void HardBridgeScene::checkFinish(){
    //compruebo que estan todos en la derecha
    //success
    if(mPerro->estaEnDerecha() && mGranjero->estaEnDerecha() && mPato1->estaEnDerecha() && mPato2->estaEnDerecha() && mCastor1->estaEnDerecha() && mCastor2->estaEnDerecha() && mNino->estaEnDerecha() && mNina->estaEnDerecha()){
        mSuccess=true;
        mStars=3;
        
    }
    BaseScene::checkFinish(NULL);
    
};


bool HardBridgeScene::esMovimientoValido(){
    
    if(mPerro->mEstado==EstadoProtagonista(kProtagonistaMontado)){
        if(mNino->mEstado==EstadoProtagonista(kProtagonistaMontado)){
            CCLOG("esMovimientoValido:1");
            LayerSituacionNoOk::mostrar(kPerroNino);
            return false;
        }else if(mNina->mEstado==EstadoProtagonista(kProtagonistaMontado)){
            CCLOG("esMovimientoValido:2");
            LayerSituacionNoOk::mostrar(kPerroNina);
            return false;
        }
    }
    if(mNina->mEstado==EstadoProtagonista(kProtagonistaMontado)
       && ( mNino->mEstado!=EstadoProtagonista(kProtagonistaMontado)&&
           (mNino->mEstado==mCastor1->mEstado || mNino->mEstado==mCastor2->mEstado)
      ))
       {
           CCLOG("esMovimientoValido:3");
           LayerSituacionNoOk::mostrar(kNinoCastor);
           return false;
       }
    if(mNino->mEstado==EstadoProtagonista(kProtagonistaMontado)
       && ( mNina->mEstado!=EstadoProtagonista(kProtagonistaMontado)&&
           (mNina->mEstado==mPato1->mEstado || mNina->mEstado==mPato2->mEstado)
           ))
    {
        CCLOG("esMovimientoValido:4");
        LayerSituacionNoOk::mostrar(kNinaPato);
        return false;
    }
    
    if(mNino->mEstado==EstadoProtagonista(kProtagonistaMontado) && mNina->mEstado==EstadoProtagonista(kProtagonistaMontado)){
        if(mGranjero->mEstado!=mPerro->mEstado){
            if(
                (mPerro->estaEnDerecha() && mBarca->estaEnIzquierda())
                ||
                (mPerro->estaEnIzquierda() && !mBarca->estaEnIzquierda())){
                CCLOG("esMovimientoValido:14");
                LayerSituacionNoOk::mostrar(kPerroLadrando);
                return false;

            }
        }
    }

    
    if(mNina->mEstado!=mNino->mEstado)
    {
        if(mNino->mEstado==EstadoProtagonista(kProtagonistaMontado)
           && (
                (mCastor1->mEstado==EstadoProtagonista(kProtagonistaMontado) || mCastor1->mEstado!=mNina->mEstado)
               ||
                (mCastor2->mEstado==EstadoProtagonista(kProtagonistaMontado) || mCastor2->mEstado!=mNina->mEstado)
               )
           )
        {
            CCLOG("esMovimientoValido:5");
            LayerSituacionNoOk::mostrar(kNinoCastor);
            return false;
        }
        if(mNina->mEstado==EstadoProtagonista(kProtagonistaMontado)
           && (
                (mPato1->mEstado==EstadoProtagonista(kProtagonistaMontado) || mPato1->mEstado!=mNino->mEstado)
               
                ||
                (mPato2->mEstado==EstadoProtagonista(kProtagonistaMontado) || mPato2->mEstado!=mNino->mEstado)
               )
           )
           
        {
            CCLOG("esMovimientoValido:6");
            LayerSituacionNoOk::mostrar(kNinaPato);
            return false;
        }
    }
    
    if(mPerro->mEstado!=mGranjero->mEstado && mPerro->mEstado!=EstadoProtagonista(kProtagonistaMontado))
    {
        
        if(mPerro->mEstado==mNino->mEstado){
            CCLOG("esMovimientoValido:7");
            LayerSituacionNoOk::mostrar(kPerroNino);
            return false;
        }
        else if(mPerro->mEstado==mNina->mEstado){
            CCLOG("esMovimientoValido:8");
            LayerSituacionNoOk::mostrar(kPerroNina);
            return false;
        }
        else if(mPerro->mEstado==mCastor1->mEstado || mPerro->mEstado==mCastor2->mEstado){
            CCLOG("esMovimientoValido:9");
            LayerSituacionNoOk::mostrar(kPerroCastor);
            return false;
        }
        else if(mPerro->mEstado==mPato1->mEstado || mPerro->mEstado==mPato2->mEstado){
            CCLOG("esMovimientoValido:10");
            LayerSituacionNoOk::mostrar(kPerroPato);
            return false;
        }
    }
    else{
        if((mNino->mEstado==mCastor1->mEstado || mNino->mEstado ==mCastor2->mEstado)
           && mNino->mEstado !=EstadoProtagonista(kProtagonistaMontado) && mNino->mEstado!=mNina->mEstado && mNina->mEstado!=EstadoProtagonista(kProtagonistaMontado)){
                CCLOG("esMovimientoValido:11");
                LayerSituacionNoOk::mostrar(kNinoCastor);
                return false;
        }
        else if((mNina->mEstado==mPato1->mEstado || mNina->mEstado==mPato2->mEstado)&& mNina->mEstado !=EstadoProtagonista(kProtagonistaMontado)&& mNino->mEstado!=mNina->mEstado && mNino->mEstado!=EstadoProtagonista(kProtagonistaMontado)){
                CCLOG("esMovimientoValido:13");
                LayerSituacionNoOk::mostrar(kNinaPato);
                return false;
        }

        
    }
    return true;
    
}

void HardBridgeScene::HintStart(Ref* sender){
    LayerInfo::ocultar(NULL);
    if(!mSituacionHint){
        return;
    }
    mHintStep++;
    CCLOG("PASO %i",mHintStep);
    switch (mHintStep) {
        case 1:
            setHand(mGranjero);
            mHintNodeToTouch=mGranjero;
            break;
        case 2:
            setHand(mPerro);
            mHintNodeToTouch=mPerro;
            break;
        case 3:
            setHand(mBarca);
            mHintNodeToTouch=mBarca;
            break;
        case 4:
            setHand(mPerro);
            mHintNodeToTouch=mPerro;
            break;
        case 5:
            setHand(mBarca);
            mHintNodeToTouch=mBarca;
            break;
        case 6:
            setHand(mPato1);
            mHintNodeToTouch=mPato1;
            break;
            
            
        case 7:
            setHand(mBarca);
            mHintNodeToTouch=mBarca;
            break;
        case 8:
            setHand(mPato1);
            mHintNodeToTouch=mPato1;
            break;
        case 9:
            setHand(mPerro);
            mHintNodeToTouch=mPerro;
            break;
        case 10:
            setHand(mBarca);
            mHintNodeToTouch=mBarca;
            break;
        case 11:
            setHand(mPerro);
            mHintNodeToTouch=mPerro;
            break;
        case 12:
            setHand(mGranjero);
            mHintNodeToTouch=mGranjero;
            break;
            
            
        case 13:
            setHand(mNino);
            mHintNodeToTouch=mNino;
            break;
        case 14:
            setHand( mPato2);
            mHintNodeToTouch=mPato2;
            break;
        case 15:
            setHand( mBarca);
            mHintNodeToTouch=mBarca;
            break;
        case 16:
            setHand( mPato2);
            mHintNodeToTouch=mPato2;
            break;
        case 17:
            setHand( mBarca);
            mHintNodeToTouch=mBarca;
            break;
        case 18:
            setHand( mNina);
            mHintNodeToTouch=mNina;
            break;
            
            
        case 19:
            setHand( mBarca);
            mHintNodeToTouch=mBarca;
            break;
        case 20:
            setHand( mNino);
            mHintNodeToTouch=mNino;
            break;
        case 21:
            setHand( mBarca);
            mHintNodeToTouch=mBarca;
            break;
        case 22:
            setHand( mNina);
            mHintNodeToTouch=mNina;
            break;
        case 23:
            setHand( mGranjero);
            mHintNodeToTouch=mGranjero;
            break;
        case 24:
            setHand( mPerro);
            mHintNodeToTouch=mPerro;
            break;
            
            
        case 25:
            setHand( mBarca);
            mHintNodeToTouch=mBarca;
            break;
        case 26:
            setHand( mGranjero);
            mHintNodeToTouch=mGranjero;
            break;
        case 27:
            setHand( mPerro);
            mHintNodeToTouch=mPerro;
            break;
        case 28:
            setHand( mNino);
            mHintNodeToTouch=mNino;
            break;
        case 29:
            setHand( mBarca);
            mHintNodeToTouch=mBarca;
            break;
        case 30:
            setHand( mNina);
            mHintNodeToTouch=mNina;
            break;
        case 31:
            setHand( mBarca);
            mHintNodeToTouch=mBarca;
            break;
        case 32:
            setHand( mNino);
            mHintNodeToTouch=mNino;
            break;
        case 33:
            setHand( mBarca);
            mHintNodeToTouch=mBarca;
            break;
        case 34:
            setHand( mCastor1);
            mHintNodeToTouch=mCastor1;
            break;
        case 35:
            setHand( mBarca);
            mHintNodeToTouch=mBarca;
            break;
        case 36:
            setHand( mCastor1);
            mHintNodeToTouch=mCastor1;
            break;
        case 37:
            setHand( mNina);
            mHintNodeToTouch=mNina;
            break;
        case 38:
            setHand(mGranjero);
            mHintNodeToTouch=mGranjero;
            break;
        case 39:
            setHand( mPerro);
            mHintNodeToTouch=mPerro;
            break;
        case 40:
            setHand( mBarca);
            mHintNodeToTouch=mBarca;
            break;
        case 41:
            setHand( mPerro);
            mHintNodeToTouch=mPerro;
            break;
        case 42:
            setHand( mCastor2);
            mHintNodeToTouch=mCastor2;
            break;
        case 43:
            setHand( mBarca);
            mHintNodeToTouch=mBarca;
            break;
        case 44:
            setHand( mCastor2);
            mHintNodeToTouch=mCastor2;
            break;
        case 45:
            setHand( mBarca);
            mHintNodeToTouch=mBarca;
            break;
        case 46:
            setHand( mPerro);
            mHintNodeToTouch=mPerro;
            break;
        case 47:
            setHand( mBarca);
            mHintNodeToTouch=mBarca;
            break;
        case 48:
            setHand( mGranjero);
            mHintNodeToTouch=mGranjero;
            break;
        case 49:
            setHand( mPerro);
            mHintNodeToTouch=mPerro;
            break;
            
            
            
            
    }

 
    
}

void HardBridgeScene::hintFollowed(Sprite* HintNodeToTouch){
    HintNodeToTouch->stopAction(mAccionTintado);
    if(!mSituacionHint){
        return;
    }
    if(mHintNodeToTouch==HintNodeToTouch){
        HintStart(NULL);
    }else{
        mHintNodeToTouch->stopAction(mAccionTintado);
        mHintNodeToTouch->setColor(Color3B(255,255,255));
        mHintNodeToTouch=NULL;
        LayerHintOk::ocultar(NULL);
        //Boton* boton=Boton::createBoton(this,menu_selector(HardBridgeScene::HintStart), "ui/reload.png");
        Boton* botonStop=Boton::createBoton(this,menu_selector(HardBridgeScene::HintStop), "ui/close.png");
        LayerInfo::mostrar(LanguageManager::getInstance()->getStringForKey("HARDBRIDGE_DONT_FOLLOW_HINT","You didn't follow the hint. Please restart the hint process"))->setBoton(botonStop);
        mSituacionHint=false;
    }
    
    
}





bool HardBridgeScene::sonTodoAnimalesMontados(){
    return !(mGranjero->mEstado==EstadoProtagonista(kProtagonistaMontado)
        || mNino->mEstado==EstadoProtagonista(kProtagonistaMontado)
            || mNina->mEstado==EstadoProtagonista(kProtagonistaMontado));
}

void HardBridgeScene::Retry(Ref* sender){
    
    Director::getInstance()->replaceScene(HardBridgeScene::scene());
}



void HardBridgeScene::Hint(Ref* sender){
    BaseScene::Hint(NULL);
}


void HardBridgeScene::HintCallback(bool accepted){
    if(!accepted){
        CCLOG("CANCELADO");
        return;
    }
    BaseScene::HintCallback(accepted);
    iniciarScene();
    mSituacionHint=true;
    mHintStep=0;
    LayerHintOk::mostrar(true);
    mScheudleHint=schedule_selector(HardBridgeScene::HintStartTemp);
    this->scheduleOnce(mScheudleHint,2.f);
    
}


int HardBridgeScene::getHumanosMontados(){
    int devolver=0;
    if(mGranjero->mEstado==EstadoProtagonista(kProtagonistaMontado)){
         CCLOG("getHumanosMontados mGranjero");
        devolver++;
    }if(mNino->mEstado==EstadoProtagonista(kProtagonistaMontado)){
        CCLOG("getHumanosMontados mNino");
        devolver++;
    }if(mNina->mEstado==EstadoProtagonista(kProtagonistaMontado)){
        CCLOG("getHumanosMontados mNina");
        devolver++;
    }
    CCLOG("getHumanosMontados %i",devolver);
    return devolver;
}


void HardBridgeScene::finMoveBarca(){
    if(mGranjero->mEstado==EstadoProtagonista(kProtagonistaMontado)){
        mGranjero->click(NULL);
    }if(mNino->mEstado==EstadoProtagonista(kProtagonistaMontado)){
        mNino->click(NULL);
    }if(mNina->mEstado==EstadoProtagonista(kProtagonistaMontado)){
        mNina->click(NULL);
    }if(mCastor1->mEstado==EstadoProtagonista(kProtagonistaMontado)){
        mCastor1->click(NULL);
    }if(mCastor2->mEstado==EstadoProtagonista(kProtagonistaMontado)){
        mCastor2->click(NULL);
    }if(mPato1->mEstado==EstadoProtagonista(kProtagonistaMontado)){
        mPato1->click(NULL);
    }if(mPato2->mEstado==EstadoProtagonista(kProtagonistaMontado)){
        mPato2->click(NULL);
    }if(mPerro->mEstado==EstadoProtagonista(kProtagonistaMontado)){
        mPerro->click(NULL);
    }

    
}

void HardBridgeScene::HintStartTemp(float dt){
    HintStart(NULL);
}


void HardBridgeScene::onEnterTransitionDidFinish(){
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sounds/riversound.mp3",true);
    BaseScene::onEnterTransitionDidFinish();
}


