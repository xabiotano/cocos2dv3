//
//  Protagonista.cpp
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#include "Protagonista.h"
#include "SimpleAudioEngine.h"
#include "Barca.h"

Protagonista::Protagonista(Point mPointIzq,Point mPointDch,int z_index,TipoProtagonista tipo){
    mEstaIzquierda=true;
    mIsMoving=false;
    mPosicionIzquierda=mPointIzq;
    mPosicionDerecha=mPointDch;
    mEstado=kProtagonistaIzquierda;
    setPosition(mPointIzq);
    mZIndex=z_index;
    mTipo=tipo;
    mSelected=false;
    mPosicionAsiento=-1;

}

Protagonista::~Protagonista(void){}


Protagonista* Protagonista::create(Node* parent,TipoProtagonista tipo)
{
    Point puntoIzquierda,puntoDerecha;
    Size tamanoPadre=parent->getContentSize();
    float widthPadre=tamanoPadre.width;
    float heightPadre=tamanoPadre.height;
    std::string imagen;
    int z_index;
    if(tipo==TipoProtagonista(kNino)){
        puntoIzquierda=Point(widthPadre* 0.041f,heightPadre*0.25f);
        puntoDerecha=Point(widthPadre* 0.95f,heightPadre*0.75f);
        imagen=std::string("hardbridge/nino.png");
        z_index=8;
        
    }else if(tipo==TipoProtagonista(kNina)){
        puntoIzquierda=Point(widthPadre* 0.22f,heightPadre*0.25f);
        puntoDerecha=Point(widthPadre* 0.95f,heightPadre*0.5f);
        imagen=std::string("hardbridge/nina.png");
        z_index=11;
    }else if(tipo==TipoProtagonista(kCastor1)){
        puntoIzquierda=Point(widthPadre* 0.30f,heightPadre*0.24f);
        puntoDerecha=Point(widthPadre* 0.75f,heightPadre*0.58f);
        imagen=std::string("hardbridge/castor.png");
        z_index=10;
    }else if(tipo==TipoProtagonista(kCastor2)){
        puntoIzquierda=Point(widthPadre* 0.36f,heightPadre*0.19f);
        puntoDerecha=Point(widthPadre* 0.83f,heightPadre*0.5f);
        imagen=std::string("hardbridge/castor.png");
        z_index=9;
    }else if(tipo==TipoProtagonista(kPato1)){
        puntoIzquierda=Point(widthPadre* 0.12f,heightPadre*0.33f);
        puntoDerecha=Point(widthPadre* 0.68f,heightPadre*0.9f);
        imagen=std::string("hardbridge/duck.png");
         z_index=9;
    }else if(tipo==TipoProtagonista(kPato2)){
        puntoIzquierda=Point(widthPadre* 0.11f,heightPadre*0.20f);
        puntoDerecha=Point(widthPadre* 0.68f,heightPadre*0.74f);
        imagen=std::string("hardbridge/duck.png");
         z_index=10;
    }else if(tipo==TipoProtagonista(kPerro)){
        puntoIzquierda=Point(widthPadre* 0.07f,heightPadre*0.46f);
        puntoDerecha=Point(widthPadre* 0.45f,heightPadre*0.87f);
        imagen=std::string("hardbridge/perro.png");
        z_index=5;
    }else if(tipo==TipoProtagonista(kGranjero)){
        puntoIzquierda=Point(widthPadre* 0.2f,heightPadre*0.50f);
        puntoDerecha=Point(widthPadre* 0.56f,heightPadre*0.85f);
        imagen=std::string("hardbridge/farmer.png");
        z_index=4;
    }
    Protagonista *devolver= new Protagonista(puntoIzquierda,puntoDerecha,z_index,tipo);
    if(devolver && devolver->initBotonWithPriority(devolver, menu_selector(Protagonista::click), imagen,100-z_index))
    {
        devolver->autorelease();
        devolver->setPosition(devolver->mPosicionIzquierda);
        return devolver;
    }
    CC_SAFE_DELETE(devolver);
    return NULL;
  
}


void Protagonista::click(Ref* sender)
{
    if(mIsMoving){
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevator_badselected.mp3",false);
        return;
    }
    if(HardBridgeScene::getInstance()->isBarcaMoving()){
        CCLOG("Barca MOVING");
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevator_badselected.mp3",false);
        return;
    }
    

   
    if(mEstado==EstadoProtagonista(kProtagonistaIzquierda)){
        int huecosLibresBarca=HardBridgeScene::getInstance()->mBarca->getNumeroSitiosLibres();
        CCLOG("huecosLibresBarca %i",huecosLibresBarca);
        if(HardBridgeScene::getInstance()->estaBarcaIzquierda() && huecosLibresBarca >0  ){
            int posicion;
            Point destino=HardBridgeScene::getInstance()->mBarca->getSitioLibre(true,posicion);
            mPosicionAsiento=posicion;
            HardBridgeScene::getInstance()->mBarca->ocuparAsiento(posicion);
            CallFunc *finUnmount = CallFunc::create([this]() { this->finMove(); });
            this->runAction(Sequence::createWithTwoActions(JumpTo::create(0.5f, destino, Director::getInstance()->getWinSize().height*0.1f, 1),finUnmount));
            mIsMoving=true;
           
        }else{
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevator_badselected.mp3",false);

        }
    }
    else if(mEstado==EstadoProtagonista(kProtagonistaDerecha)){
        int huecosLibresBarca=HardBridgeScene::getInstance()->mBarca->getNumeroSitiosLibres();
        CCLOG("huecosLibresBarca %i",huecosLibresBarca);
        if(!HardBridgeScene::getInstance()->estaBarcaIzquierda() && huecosLibresBarca >0  ){
            int posicion;
            Point destino=HardBridgeScene::getInstance()->mBarca->getSitioLibre(true,posicion);
            mPosicionAsiento=posicion;
            HardBridgeScene::getInstance()->mBarca->ocuparAsiento(posicion);
            CallFunc *finUnmount =CallFunc::create([this]() { this->finMove(); });
            this->runAction(Sequence::createWithTwoActions(JumpTo::create(0.5f, destino, Director::getInstance()->getWinSize().height*0.1f, 1),finUnmount));
             mIsMoving=true;
          
            
        }else{
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevator_badselected.mp3",false);
            
        }
    }else if(mEstado==EstadoProtagonista(kProtagonistaMontado)){
        if(HardBridgeScene::getInstance()->estaBarcaIzquierda()){
            //izquierda
            Point punto=this->getParent()->convertToWorldSpace(getPosition());
            CCLOG("Punto desmontar %f %f",punto.x,punto.y);
            this->retain();
            this->removeFromParent();
            HardBridgeScene::getInstance()->addChild(this,mZIndex);
            this->autorelease();
            this->setPosition(punto);
            CallFunc *finUnmount = CallFunc::create([this]() { this->finUnMount(); });
            this->runAction(Sequence::createWithTwoActions(JumpTo::create(0.5f, mPosicionIzquierda, Director::getInstance()->getWinSize().height*0.1f, 1),finUnmount));
            this->mEstado=EstadoProtagonista(kProtagonistaIzquierda);
            HardBridgeScene::getInstance()->mBarca->liberarAsiento(mPosicionAsiento);
             mIsMoving=true;
            
        }
        else
        {
            //derecha
            Point punto=this->getParent()->convertToWorldSpace(getPosition());
            CCLOG("Punto desmontar %f %f",punto.x,punto.y);
            this->retain();
            this->removeFromParentAndCleanup(false);
            HardBridgeScene::getInstance()->addChild(this,mZIndex);
            this->autorelease();
            this->setPosition(punto);
            CallFunc *finUnmount = CallFunc::create([this]() { this->finUnMount(); });
            this->runAction(Sequence::createWithTwoActions(JumpTo::create(0.5f, mPosicionDerecha, Director::getInstance()->getWinSize().height*0.1f, 1),finUnmount));
            this->mEstado=EstadoProtagonista(kProtagonistaDerecha);
            HardBridgeScene::getInstance()->mBarca->liberarAsiento(mPosicionAsiento);
             mIsMoving=true;
            

        }
        
    }
    if(mIsMoving){
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/hardbridge/jump.mp3");
    }
    HardBridgeScene::getInstance()->hintFollowed(this);
}


void Protagonista::finMove(){
    Point posicion= this->getPosition();
    CCLOG("POSICION absoluta %f %f",posicion.x,posicion.y);
    posicion=HardBridgeScene::getInstance()->mBarca->convertToNodeSpace(posicion);
    CCLOG("POSICION  convertida %f %f",posicion.x,posicion.y);
    this->retain();
    this->removeFromParent();
    HardBridgeScene::getInstance()->mBarca->addChild(this,-2);
    this->autorelease();
    this->setPosition( posicion);
    this->mEstado=EstadoProtagonista(kProtagonistaMontado);
    mIsMoving=false;
    
    HardBridgeScene::getInstance()->mBarca->checkAlertaMover();
}

void Protagonista::finUnMount(){
    mPosicionAsiento=-1;
    mIsMoving=false;
    HardBridgeScene::getInstance()->mBarca->checkAlertaMover();
    HardBridgeScene::getInstance()->checkFinish();
    
    
}

bool Protagonista::estaEnIzquierda(){
    return (mEstado==EstadoProtagonista(kProtagonistaIzquierda));
}

void Protagonista::setMoving(bool moving){
    this->mIsMoving=moving;
}


bool Protagonista::isSelected(){
    return mSelected;
}

std::string Protagonista::getNameOfHuman(){
    return mName;
}








void Protagonista::move(){
    if(isSelected()){
        
        Point destino;
        if(HardBridgeScene::getInstance()->estaBarcaIzquierda()){
            destino=mPosicionDerecha;
            mEstado=EstadoProtagonista(kProtagonistaDerecha);
        }
        else{
            destino=mPosicionIzquierda;
            mEstado=EstadoProtagonista(kProtagonistaIzquierda);
        }
        CallFunc* finmove=CallFunc::create([this]() { this->finMove(); });
        runAction(Sequence::createWithTwoActions(MoveTo::create(1.5f, destino),finmove));
        
    }
}


bool Protagonista::estaEnDerecha(){
   return mEstado==EstadoProtagonista(kProtagonistaDerecha);
}






