//
//  LayerSituacionNoOk.h
//  trivial
//
//  Created by Xabier on 25/03/13.
//
//

#ifndef __logic__LayerNoOk
#define __logic__LayerNoOk

#include <iostream>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "TipoSituacionProblemaTypes.h"
#include "../BaseScene.h"

USING_NS_CC;

USING_NS_CC_EXT;

class LayerSituacionNoOk: public LayerColor
{
    
    
    public:
    
    static LayerSituacionNoOk* initLayer(TipoSituacionProblema tipoSituacionProblema);
    Rect rect();
   
    virtual bool init();

    CREATE_FUNC(LayerSituacionNoOk);
    
    void dibujarContent(TipoSituacionProblema tipoSituacionProblema);
    
    static void ocultar(Ref* pSender);
    static void mostrar(TipoSituacionProblema tipoSituacionProblema);
    EventListenerTouchOneByOne* mListener;
    virtual bool onTouchBegan(Touch* touch, Event* event);
    virtual void onTouchMoved(Touch* touch, Event* event);
    virtual void onTouchEnded(Touch* touch, Event* event);
    virtual void onEnter();
    virtual void onExit();


    const static int TAG_LAYER_SITUACION_NO_OK=1021;


};

#endif /* defined(__logic__LayerNoOk) */
