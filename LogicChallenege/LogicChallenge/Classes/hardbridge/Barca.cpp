//
//  Barca.cpp
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#include "Barca.h"
#include "SimpleAudioEngine.h"


Barca::Barca(Point mPointIzq,Point mPointDch){
    mEstaIzquierda=true;
    mIsMoving=false;
    mPosicionIzquierda=mPointIzq;
    mPosicionDerecha=mPointDch;
    mEstado=kBarcaLadoIzquierdo;
    setPosition(mPointIzq);
    mSitioBarca1=false;
    mSitioBarca2=false;
    mSitios= Dictionary::create();
    mSitios->retain();
    mSitios->setObject(Bool::create(false), "0");
    mSitios->setObject(Bool::create(false), "1");
  
}


Barca::~Barca(void){
    CC_SAFE_RELEASE(mSitios);
}


Barca* Barca::create(Node* parent)
{
    Point puntoIzquierda,puntoDerecha;
    Size tamanoPadre=parent->getContentSize();
    float widthPadre=tamanoPadre.width;
    float heightPadre=tamanoPadre.height;
    puntoIzquierda=Point(widthPadre*0.4f, heightPadre*0.4f);
    puntoDerecha=Point(widthPadre*0.6f, heightPadre*0.46f);
    
    Barca *devolver= new Barca(puntoIzquierda,puntoDerecha);
    if(devolver && devolver->initBotonWithPriority(devolver, menu_selector(Barca::click),"hardbridge/barca_front.png",1000))
    {
        devolver->autorelease();
        devolver->setPosition(devolver->mPosicionIzquierda);
        Sprite* barca_fondo= Sprite::create("hardbridge/barca.png");
        barca_fondo->setPosition(Point(barca_fondo->getContentSize().width/2,barca_fondo->getContentSize().height/2));
        devolver->addChild(barca_fondo,-5);
        Sprite* motor=Sprite::create("hardbridge/barca_motor.png");
        devolver->addChild(motor);
        motor->setPosition(Point(devolver->getContentSize().width*0.05f,devolver->getContentSize().height*0.65f));
        
        ParticleSystemQuad* mParticle = ParticleSystemQuad::create("hardbridge/particle_texture.plist");
        mParticle->setPosition(Point(motor->getPositionX(),motor->getPositionY()+motor->getContentSize().height*0.5f ));
        devolver->addChild(mParticle,1);
        return devolver;
    }
    CC_SAFE_DELETE(devolver);
    return NULL;
}


void Barca::click(Ref* sender)
{
    if(mIsMoving){
        CCLOG("Barca MOVING");
        return;
    }
    if(getNumeroSitiosLibres()==2){
        CCLOG("Barca vacia");
        LayerInfo::mostrar(LanguageManager::getInstance()->getStringForKey("HARDBRIDGE_BOAT_EMPTY"))->setSprite(Sprite::create("hardbridge/barca.png"));
        return;
    }
    Point destino;
    if(HardBridgeScene::getInstance()->sonTodoAnimalesMontados()){
        return;
    }
    
    if(!HardBridgeScene::getInstance()->esMovimientoValido()){
        return;
    }
    if(mEstado==BarcaState(kBarcaLadoIzquierdo)){
        
        destino=mPosicionDerecha;
        this->mEstado=BarcaState(kBarcaLadoDerecho);
    }
    else{
        destino=mPosicionIzquierda;
        this->mEstado=BarcaState(kBarcaLadoIzquierdo);
    }
    
    
    mIsMoving=true;
    CallFunc *finUnmount =CallFunc::create([this]() { this->finMove(); });
    this->runAction(Sequence::createWithTwoActions(MoveTo::create(2.f, destino),finUnmount));
     CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/hardbridge/boat.mp3");
    HardBridgeScene::getInstance()->hintFollowed(this);

    
}


void Barca::finMove(){
    mIsMoving=false;
    HardBridgeScene::getInstance()->finMoveBarca();
    
}

                    
bool Barca::estaEnIzquierda(){
    return (mEstado==BarcaState(kBarcaLadoIzquierdo));
}

void Barca::setMoving(bool moving){
    this->mIsMoving=moving;
}

Point Barca::getSitioLibre(bool convertidoAWorld,int &posicion){
    Point devolver;
    Bool* sitio1=(Bool*)mSitios->objectForKey("0");
    Bool* sitio2=(Bool*)mSitios->objectForKey("1");

    if(!sitio1->getValue()){
        devolver= Point(this->getContentSize().width*0.33f, this->getContentSize().height*0.75f);
        CCLOG("getSitioLibre()=> 1");
        posicion=0;
    }
    else if(!sitio2->getValue()){
        devolver= Point(this->getContentSize().width*0.72f, this->getContentSize().height*0.75f);
        CCLOG("getSitioLibre()=> 2");
        posicion=1;
    }
    else
    {
        return Point(0,0);
    }
    if(convertidoAWorld)
    {
        devolver=this->convertToWorldSpace(devolver);;
        CCLOG("getSitioLibre %f %f",devolver.x,devolver.y);
        return devolver;
    }
    else{
        CCLOG("getSitioLibre %f %f",devolver.x,devolver.y);
        return devolver;
    }
}

int Barca::getNumeroSitiosLibres(){
    Bool* sitio1=(Bool*)mSitios->objectForKey("0");
    Bool* sitio2=(Bool*)mSitios->objectForKey("1");
    
    int numero=0;
    if(!sitio1->getValue()){
        numero++;
    }if(!sitio2->getValue()){
        numero++;
    }
    return numero;
}



void Barca::ocuparAsiento(int posicion){
    CCLOG("Ocupar posicion %i",posicion);
    if(posicion==0){
        mSitios->setObject(Bool::create(true), "0");
    }else if(posicion==1){
       mSitios->setObject(Bool::create(true), "1");
    }
    
    
}


void Barca::move(Ref* sender){
    Point destino;
        if(this->mEstaIzquierda){
            destino=mPosicionDerecha;
            mEstado=BarcaState(kBarcaLadoDerecho);
        }
        else{
            destino=mPosicionIzquierda;
            mEstado=BarcaState(kBarcaLadoIzquierdo);
        }
        CallFunc* finmove=CallFunc::create([this]() { this->finMove(); });
        runAction(Sequence::createWithTwoActions(MoveTo::create(1.5f, destino),finmove));
        
    
}


void Barca::liberarAsiento(int posicion)
{
    if(posicion==0){
         mSitios->setObject(Bool::create(false), "0");
    }
    else if(posicion==1){
      mSitios->setObject(Bool::create(false), "1");
    }
    CCLOG("liberarAsiento %i",posicion);
    
    
}


void Barca::checkAlertaMover(){
    if(HardBridgeScene::getInstance()->getHumanosMontados()>0){
        if(this->getChildByTag(6290)!=NULL){
            return;
        }
        //muestro el icono de mover
        mMover=Boton::createBoton(this, menu_selector(Barca::click),"hardbridge/alerta_mover.png");
   
        Label* label= Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("HARDBRIDGE_MOVER"), CCGetFont(), 20);
        label->setColor(Color3B(255,255,255));
        label->setPosition(Point(mMover->getContentSize().width/2,mMover->getContentSize().height/2));
        label->setWidth(mMover->getContentSize().width*0.8f);
        label->setHeight(mMover->getContentSize().height*0.8f);
        label->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        label->enableOutline(Color4B(0,0,0,255),2);
        mMover->addChild(label,3);
        
        
        
        
        mMover->setPosition(Point(getContentSize().width*0.85f,0));
        mMover->setTag(6290);
        this->addChild(mMover);
        mMover->setOpacity(0.f);
        
        mMover->runAction(EaseInOut::create(FadeIn::create(0.5f), 3));
    }
    
    if(HardBridgeScene::getInstance()->getHumanosMontados()==0){
        this->removeChildByTag(6290);
    }
}







