
#ifndef __logicmaster2__SackTypes__
#define __logicmaster2__SackTypes__

#include <iostream>



typedef enum SacksTypes
{
    kNone,
    kTomato,
    kBanana,
    kCoins,
    kWaterMelon,
    kTennis
    
} SacksTypes;

typedef enum BanderinState
{
    kLocked,
    kUnlocked,
    kAsigned
    
} BanderinState;


//2 3 1
//3 1 2
//2 1 3
typedef enum TipoEscenario
{
    kDosTresUno,
    kTresUnoDos,
    kDosUnoTres,

} TipoEscenario;



typedef enum PaloState
{
    kPaloAsigned,
    kPaloUnasigned,
    
    
} PaloState;








#endif
