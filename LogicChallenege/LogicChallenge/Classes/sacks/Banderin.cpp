//
//  Banderin.cpp
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#include "Banderin.h"
#include "SimpleAudioEngine.h"
#include "SacksScene.h"






Banderin::Banderin(SacksTypes tipo1,SacksTypes tipo2,int index){
    mTipo1=tipo1;
    mTipo2=tipo2;
    noActionLaunch();
    _color=Color3B(255,255,255);
    mIndex=index;
    mState=BanderinState(kLocked);
    
}

Banderin::~Banderin(void){}


Banderin* Banderin::create(SacksTypes tipo1,SacksTypes tipo2,int index)
{
    Banderin *devolver= new Banderin(tipo1,tipo2,index);
    devolver->autorelease();
    devolver->initBoton( NULL, NULL,"sacks/cartel.png");
    for(int i=0;i<=1;i++){
        SacksTypes tipoActual;
        if(i==0){
            tipoActual=tipo1;
        }else{
            tipoActual=tipo2;
        }
        Sprite* mercancia=Sprite::create();
        if(tipoActual==SacksTypes(kTomato)){
            mercancia->initWithTexture( Director::getInstance()->getTextureCache()->addImage("sacks/tomato.png"));
            //  mercancia->setPosition(Point(devolver->getContentSize().width/2,devolver->getContentSize().heigth*0.8f));
        }else if(tipoActual==SacksTypes(kBanana)){
            mercancia->initWithTexture( Director::getInstance()->getTextureCache()->addImage("sacks/banana.png"));
        }else if(tipoActual==SacksTypes(kTennis)){
            mercancia->initWithTexture( Director::getInstance()->getTextureCache()->addImage("sacks/tennis.png"));
        }else if(tipoActual==SacksTypes(kWaterMelon)){
            mercancia->initWithTexture( Director::getInstance()->getTextureCache()->addImage("sacks/watermelon.png"));
        }else if(tipoActual==SacksTypes(kCoins)){
            mercancia->initWithTexture( Director::getInstance()->getTextureCache()->addImage("sacks/coin.png"));
        }
        if(i==0){
              mercancia->setPosition(Point(devolver->getContentSize().width*0.27,devolver->getContentSize().height*0.52f));
        }else{
              mercancia->setPosition(Point(devolver->getContentSize().width*0.7,devolver->getContentSize().height*0.50f));
        }
        mercancia->setScale(0.5f);
       
        devolver->addChild(mercancia);
        devolver->setState(kLocked);
    }
    
    
    return devolver;
}

SacksTypes Banderin::getType1(){
     return mTipo1;
}
SacksTypes Banderin::getType2(){
     return mTipo2;
}
bool Banderin::onTouchBegan(Touch* touch, Event* event)
{
    
    if ( containsTouchLocation(touch) ){
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/pop.mp3",false);
    }
    else{
    
    }
    return Boton::onTouchBegan(touch,event);
    
}


void Banderin::onTouchMoved(Touch* touch, Event* event){
    if(mState==BanderinState(kLocked)|| this->mState==BanderinState(kAsigned)){
        return;
    }
    Point puntonuevo=Director::getInstance()->convertToGL(touch->getLocationInView());
    this->setPosition(puntonuevo);
    SacksScene::getInstance()->checkCollision(this);
}

void Banderin::onTouchEnded(Touch* touch, Event* event){
    Boton::onTouchEnded(touch, event);

    if(mState!=BanderinState(kUnlocked)){
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevator_badselected.mp3",false);
        return;
    }
   
    if(!SacksScene::getInstance()->setBanderin(this)){
        this->runAction(MoveTo::create(0.5f, mPuntoOrigen));
        SacksScene::getInstance()->finishAnimations();
        
    }
    
}

int Banderin::getIndex(){
    return mIndex;
}

BanderinState Banderin::getState(){
    return mState;
}

void  Banderin::setState(BanderinState state){
    mState=state;
}









