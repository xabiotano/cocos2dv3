//
//  Sack.h
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#ifndef __logicmaster2__Sack__
#define __logicmaster2__Sack__

#include <iostream>
#include "../widgets/Boton.h"
#include "SacksScene.h"
#include "SacksTypes.h"



class Sack: public Boton
{
    
    
    public:
        Sack(SacksScene* parent,int index);
        virtual ~Sack(void);
        static Sack* create(SacksScene* parent,int mIndex);
        SacksTypes getType();
        void setType(SacksTypes tipo);
        int getIndex();
        bool getLocked();
        void setLocked(bool fixed);
    
    protected:
      
        void onTouchEnded(Touch* touch, Event* event) override;

    private:
        SacksTypes mTipo;
        SacksScene* mParent;
        int mIndex;
        bool mFixed;
    
    
};


#endif 
