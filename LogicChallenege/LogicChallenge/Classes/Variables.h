//
//  Variables.h
//  logicmaster2
//
//  Created by Xabier on 14/11/13.
//
//

#ifndef __logicmaster2__Variables__
#define __logicmaster2__Variables__
#include "cocos2d.h"


#include <iostream>

USING_NS_CC;

class Variables{
public:
    const static bool ES_DEBUG=false;
    static bool VIENE_DE_NOTIFICACION_GIFT;
   
    const static int TWO_FROGS_SCENE=1;//1=>7
    const static int GOATLETTUCEWOLF_SCENE=2;//1=>7
    const static int VOLCAN_LEVEL=3;//5=>2
    const static int CANIBALS_LEVEL=4;//4=>3
    const static int ELEVATORS_LEVEL=5;//2=>5
    const static int BOTELLAS_LEVEL=6;//7=>9
    const static int FIVEFROGS_SCENE=7;//3=>1
    const static int SANDCLOCK_SCENE=8;//10=>6
    const static int BRIDGE_LEVEL=9;//6=>8
    const static int HANOI_LEVEL=10;//8=>4
    const static int HARDBRIDGE_LEVEL=11;//9=>10
    const static int SACKS_LEVEL=12;
    const static int SCALES_LEVEL=13;
    const static int LIGHTSOUT_LEVEL=14;
    const static int SERIES_LEVEL=15;
    const static int PSICO_LEVEL=16;
    const static int FRUTAS_LEVEL=17;

    
    const static int LEVELS_SCENE=20;
    
    static int CURRENT_SCENE;
    
    
    static int LEVELS_NIVEL_CLICKADO;

    static int LEVELS_ELIXIR_CLICKADO;
    
    static void playHint(const char* hint);
   
    
    
  
    
};


#endif /* defined(__logicmaster2__Variables__) */
