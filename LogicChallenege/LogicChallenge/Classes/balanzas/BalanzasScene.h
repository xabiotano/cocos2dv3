#ifndef __BalanzasScene_SCENE_H__
#define __BalanzasScene_SCENE_H__

#include "cocos2d.h"
#include "../BaseScene.h"
#include "../widgets/Boton.h"
#include "Cake.fwd.h"
#include "Balanza.fwd.h"
#include "../widgets/LanguageManager.h"

USING_NS_CC;


class BalanzasScene : public BaseScene
{
public:
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init();
    
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static Scene* scene();
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(BalanzasScene);
    static BalanzasScene* getInstance();
    bool isMoving();
    virtual void checkFinish();
    virtual void Retry(Ref* sender);
    virtual void Hint(Ref* sender);
    virtual void HintCallback(bool accepted);
    virtual void TutorialStart(){};
    void pesar(Ref* sender);

    Balanza* checkIfIstouching(Cake* sender);
    bool mIsMoving;
    void finMoveB();
    bool getFinPesadas();
    bool getEleccionPeso();
    void selectDifferentCake(Cake* cake);
    void heavier(Ref* sender);
    void lighter(Ref* sender);
    
    
private:
    Boton* mBoton;
    Sprite* mBalanza;
    
    Sprite *mParteBaja;
    Cake *mCake1,*mCake2,*mCake3,*mCake4,*mCake5,*mCake6,*mCake7,*mCake8,*mCake9;
    Balanza* mBalanzaIzq,*mBalanzaDch;
    CCArray *mCakes;
    void finMoveI();
    void finMoveD();
    void finMove();
    void nada();
    Label* mLabelNumPesadas;
    int mNumeroPesadas;
    bool mFinPesadas;
    bool mFinEleccion;
    void mostrarPanelEleccionCake(bool visible);
    void mostrarPanelEleccionPeso(bool visible);
    Cake* mCakeSeleccionado;
    void getCakesBalanza(int& izquierda,int& derecha);
    void getPesoBalanza(int& izquierda,int& derecha);
    void cambiarDiferenteAbajo();
    int mPesoDiferente;
    int getCakesSinPesar();
    void cambiarDiferenteASinPesar();
    void cambiarDiferenteArriba();
    int getNumeroImposibles(int& izquierda,int& derecha);
    
     void onEnterTransitionDidFinish();
};


#endif // __BalanzasScene_SCENE_H__
