#ifndef __BotellasScene_SCENE_H__
#define __BotellasScene_SCENE_H__

#include "cocos2d.h"
#include "../BaseScene.h"
#include "../widgets/Boton.h"
#include "Bottle.fwd.h"
USING_NS_CC;


class BotellasScene : public BaseScene
{
public:
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init();
    virtual ~BotellasScene(void);
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static Scene* scene();
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(BotellasScene);
    static BotellasScene* getInstance();
    
    virtual void checkFinish(Ref* sender);
    virtual void Hint(Ref* sender);
    virtual void HintCallback(bool accepted);
     virtual void TutorialStart(Ref* sender){};
    Bottle* checkIfIstouching(Bottle* sender);
    void cargarAnimacionPesaje(Ref* sender);
   void onEnterTransitionDidFinish();  
    
protected:
    
    void Retry(Ref* sender);
private:
    Sprite* mFondoAlert;
    Label* mCounter;
    Bottle* mBotella3,*mBotella5,*mBotella8;
    Sprite    *mBackBoxBlue,*mBackBoxPink;
    void InfomoverBotellaBasket();
    CCArray* mListaPasos;
    
};


#endif // __BotellasScene_SCENE_H__
