//
//  Animal.cpp
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#include "Animal.h"
#include "SimpleAudioEngine.h"




Animal::Animal(Point mPointIzq,Point mPointDch,std::string name)
{
    mEstaIzquierda=true;
    mIsMoving=false;
    mPosicionIzquierda=mPointIzq;
    mPosicionDerecha=mPointDch;
    mEstado=AnimalState(kIslaIzquierda);
    mName=name;
    setPosition(mPointIzq);
    mSwallowTouches=true;
}

Animal::~Animal(void)
{
    
}


Animal* Animal::create(Node* parent,std::string name,Point mPointIzq,Point mPointDch,std::string imagen)
{
    Animal *devolver= new Animal(mPointIzq,mPointDch,name);
    devolver->autorelease();
    devolver->initBoton(devolver, menu_selector(Animal::click), imagen);
    devolver->setPosition(devolver->mPosicionIzquierda);
    return devolver;
}


void Animal::click(Ref* sender)
{
    if(mIsMoving){
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevator_badselected.mp3",false);
        return;
    }
    mIsMoving=true;
    
    if(mEstado==AnimalState(kMontado)){
        if(!GoatLettuceWolf::getInstance()->getBoat()->unMountAnimal(this)){
            CCLOG("SIN SENTIDO");
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevator_badselected.mp3",false);
            mIsMoving=false;
            return;
        }
        Point destino;
        if(GoatLettuceWolf::getInstance()->getBoat()->estaEnIzquierda()){
            destino=mPosicionIzquierda;
            this->mEstado=AnimalState(kIslaIzquierda);
        }
        else{
            destino=mPosicionDerecha;
            this->mEstado=AnimalState(kIslaDerecha);
        }
        CallFunc *finMove = CallFunc::create([this]() { this->finUnmount(); });
        this->runAction(Sequence::createWithTwoActions(JumpTo::create(0.5f, destino, Director::getInstance()->getWinSize().height*0.1f, 1),finMove));
        this->runAction(ScaleTo::create(0.5f, 1.f));
        this->mSoundEffect=CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/jump_animal.mp3",false);
        mIsMoving=true;
        return;
    }
    else{
        if(GoatLettuceWolf::getInstance()->getBoat()->estaEnIzquierda()){
            if(mEstado==AnimalState(kIslaDerecha)){
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevator_badselected.mp3",false);
                mIsMoving=false;
                return;
            }
        }
        else{
            if(mEstado==AnimalState(kIslaIzquierda)){
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevator_badselected.mp3",false);
                mIsMoving=false;
                return;
            }
            
        }
            
            
        
        bool montarPosible=GoatLettuceWolf::getInstance()->getBoat()->mountAnimal(this);
        if(!montarPosible){
            CCLOG("El barco esta lleno");
            mIsMoving=false;
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevator_badselected.mp3",false);
            return;
        }
        Point destino=GoatLettuceWolf::getInstance()->getBoat()->getLastSeat();
   
       
        CallFunc *finMove =CallFunc::create([this]() { this->finMove(); });
        this->runAction(Sequence::createWithTwoActions(JumpTo::create(0.5f, destino, Director::getInstance()->getWinSize().height*0.1f, 1),finMove));
        this->runAction(ScaleTo::create(0.5f, 0.9f));
        this->mEstado=AnimalState(kMontado);
        this->mSoundEffect=CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/jump_animal.mp3",false);
        mIsMoving=true;

    }
    
}

void Animal::finUnmount(){
    if(GoatLettuceWolf::getInstance()->getBoat()->estaEnIzquierda()){
        mEstado=AnimalState(kIslaIzquierda);
    }else{
          mEstado=AnimalState(kIslaDerecha);
    }
    mIsMoving=false;
     GoatLettuceWolf::getInstance()->checkFinish();
}
void Animal::finMove(){
    mEstado=AnimalState(kMontado);
    mIsMoving=false;
    GoatLettuceWolf::getInstance()->checkFinish();
   
   
}
                    
bool Animal::estaEnIzquierda(){
    return mEstaIzquierda;
}

void Animal::setMoving(bool moving){
    this->mIsMoving=moving;
}


std::string Animal::getName(){
    return mName;
}







