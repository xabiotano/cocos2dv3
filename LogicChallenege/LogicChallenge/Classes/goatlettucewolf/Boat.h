//
//  Boat.h
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#ifndef __logicmaster2__Boat__
#define __logicmaster2__Boat__

#include <iostream>
#include "../widgets/Boton.h"
#include "Boat.fwd.h"
#include "GoatLettuceWolfScene.h"
#include "Animal.h"

typedef enum boatState
{
    kBoatIzquierda,
    kBoatDerecha
    
} BoatState;

class Boat: public Boton
{
    
    public:
    
        Boat();
        virtual ~Boat(void);
        // there's no 'id' in cpp, so we recommend to return the class instance pointer
        static cocos2d::Scene* scene();
        // preprocessor macro for "static create()" constructor ( node() deprecated )
        static Boat* create();
        void move();
        bool estaEnIzquierda();
        bool mountAnimal(Animal* animal);
        bool unMountAnimal(Animal* animal);
        Point getLastSeat();
        Animal* mAnimalMounted;
        BoatState mState;


    
    private:
        bool mEstaIzquierda;
        Point mPosicionIzquierda,mPosicionDerecha;
        void finMove();
        bool mIsMoving;
        unsigned int mSoundEffect;
        ParticleSystemQuad* mParticle;
        void click(Ref* sender);
        Point seat1;
};


#endif /* defined(__logicmaster2__Boat__) */
