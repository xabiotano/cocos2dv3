//
//  Boat.cpp
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#include "Boat.h"
#include "SimpleAudioEngine.h"


Boat::Boat()
{
    initBoton(this, menu_selector(Boat::click),"lobocabralechuga/boat.png" );
    mEstaIzquierda=true;
    mIsMoving=false;
    mPosicionIzquierda=Point(290,240);
    mPosicionDerecha=Point(640,240);
    seat1=Point(250,250);
    
    mState=BoatState(kBoatIzquierda);
    mAnimalMounted=NULL;
}
Boat::~Boat(void){}


Boat* Boat::create(){
    Boat *devolver= new Boat();
    devolver->autorelease();
    devolver->setPosition(devolver->mPosicionIzquierda);
    return devolver;
}


void Boat::click(Ref* sender){
    move();
    
}

void Boat::move(){
    if(mIsMoving){
        return;
    }
    mParticle = ParticleSystemQuad::create("lobocabralechuga/humo_de_iz.plist");
    Point destino;
    if(estaEnIzquierda()){
        destino=this->mPosicionDerecha;
        mParticle->setScaleX(1);
    }
    else{
         destino=this->mPosicionIzquierda;
          mParticle->setScaleX(-1);
    }
    mParticle->setPosition(Point(0,50));
    this->addChild(mParticle,1);

    CallFunc *finMove = CallFunc::create([this]() { this->finMove(); });
    this->runAction(Sequence::createWithTwoActions(MoveTo::create(1.f,destino),finMove));
    
    this->mSoundEffect= CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/hardbridge/boat.mp3");
    if(mAnimalMounted){
        Point destinotemp;
        if(estaEnIzquierda()){
            destinotemp=mAnimalMounted->getPosition()+ Point(380,0);
        }else{
            destinotemp=mAnimalMounted->getPosition()- Point(380,0);
        }
        mAnimalMounted->runAction(MoveTo::create(1.f,destinotemp));
    }
    
    mEstaIzquierda=!mEstaIzquierda;
    mIsMoving=true;
}

void Boat::finMove(){
    
    //GoatLettuceWolf::getInstance()->swapBoatButton();
    GoatLettuceWolf::getInstance()->checkFinish();
    mIsMoving=false;
    if(!mEstaIzquierda){
         mState=BoatState(kBoatDerecha);
    }else{
         mState=BoatState(kBoatIzquierda);
        
        
    }
    
    if(mAnimalMounted){
        mAnimalMounted->click(NULL);
    }
    
    CocosDenshion::SimpleAudioEngine::getInstance()->stopEffect(this->mSoundEffect);
    GoatLettuceWolf::getInstance()->checkFinish();
    this->removeChild(mParticle, true);
}
                    
bool Boat::estaEnIzquierda(){
    return mEstaIzquierda;
}






bool Boat::mountAnimal(Animal* animal){
    if(mAnimalMounted){
        return false;
    }
    else{
        mAnimalMounted=animal;
        GoatLettuceWolf::getInstance()->reorderChild(animal,this->getLocalZOrder()-1);
        this->runAction(RepeatForever::create(Sequence::createWithTwoActions(ScaleTo::create(1.f, 1.05f), ScaleTo::create(1.f, 0.95f))));
        return true;
    }
}

bool Boat::unMountAnimal(Animal* animal){
    
    if(animal){
        this->stopAllActions();
        this->setScale(1.f);
        if(animal->mName=="wolf"){
             GoatLettuceWolf::getInstance()->reorderChild(animal,this->getLocalZOrder()+10);
        }else if(animal->mName=="lettuce"){
            GoatLettuceWolf::getInstance()->reorderChild(animal,this->getLocalZOrder()+8);
        }
        else{
             GoatLettuceWolf::getInstance()->reorderChild(animal,this->getLocalZOrder()+2);
        }
       
        
        mAnimalMounted=NULL;
        return true;
    }
    return false;
    
}

Point Boat::getLastSeat(){
    return Point(this->getPositionX()- this->getContentSize().width *0.2f,  this->getPositionY()     );
    
}










