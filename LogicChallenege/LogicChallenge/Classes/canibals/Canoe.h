
#ifndef __logicmaster2__Canoe__
#define __logicmaster2__Canoe__

#include <iostream>
#include "../widgets/Boton.h"
#include "Canoe.fwd.h"
#include "CanibalsScene.h"
#include "Human.fwd.h"

typedef enum canoaState
{
    kCanoaIzquierda,
    kCanoaDerecha
    
} CanoaState;



class Canoe: public Boton
{
    public:
        Canoe(bool esVerde);
        virtual ~Canoe(void);
        // there's no 'id' in cpp, so we recommend to return the class instance pointer
        static cocos2d::Scene* scene();
        // preprocessor macro for "static create()" constructor ( node() deprecated )
        static Canoe* create(Node* parent,std::string imagen,bool estaIzquierda);
        void move(Ref* sender);
        bool estaEnIzquierda();
        bool mountHuman(Human* human);
        void mountHumanLogic(Human* human);
        bool unMountHuman(Human* human);
        
       
        CanoaState mState;
        int getPassengers();
        Point getSeat();
        bool isMoving();
        Human* getHuman(int index);
    private:
        bool mEstaIzquierda;
        Point mPosicionIzquierda,mPosicionDerecha;
        void finMove();
        bool mIsMoving;
        unsigned int mSoundEffect;
        Point getSeatCanoe();
        Point mSeat1;
        cocos2d::Map<int,Sprite*> mHumanosMontados;
 
   
};


#endif 
