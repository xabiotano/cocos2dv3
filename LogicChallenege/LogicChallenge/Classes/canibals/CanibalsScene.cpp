#include "CanibalsScene.h"
#include "SimpleAudioEngine.h"
#include "../layers/LayerGameOver.h"
#include "../layers/LayerSuccess.h"
#include "../Variables.h"
#include "Levels.h"
#include "Human.h"
#include "Canoe.h"
#include "HumanTypes.h"

using namespace cocos2d;
using namespace CocosDenshion;


CanibalsScene* instancia_canibal;

CanibalsScene::~CanibalsScene(void){

    
}

Scene* CanibalsScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    // 'layer' is an autorelease object
    CanibalsScene *layer = CanibalsScene::create();
    // add layer as a child to scene
    scene->addChild(layer);
     // return the scene
    return scene;
}

bool CanibalsScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !BaseScene::init() )
    {
        return false;
    }
    instancia_canibal=this;
    mNombrePantalla="CanibalsScene";
   
    mTipoNivel=kNivelLogica;
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    Sprite* mFondo = Sprite::create("canibals/background.png");
    // position the sprite on the center of the screen
    mFondo->setPosition(Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    
    this->addChild(mFondo, 0);
    float scalex=Director::getInstance()->getVisibleSize().width/mFondo->getContentSize().width;
    float scaley=Director::getInstance()->getVisibleSize().height/mFondo->getContentSize().height;
    if(scalex>scaley){
        scaley=scalex;
    }
    mFondo->setScale(scaley);
    
    Director::getInstance()->setContentScaleFactor(960/visibleSize.width);
    
    crearHumanos();
    
    mTextoHelp=std::string(LanguageManager::getInstance()->getStringForKey("HELP_CANIBAL", "HELP_CANIBAL"));

 
    return true;
}



void CanibalsScene::crearHumanos(){
    

    mHumano1=Human::create(this, Point((65.f/960.f)*mVisibleSize.width,(160.f/640.f)*mVisibleSize.height),  Point((915.f/960.f)*mVisibleSize.width,(160.f/640.f)*mVisibleSize.height),HumanType(kExplorer),5);
    
    mHumano2=Human::create(this,  Point((145.f/960.f)*mVisibleSize.width,(140.f/640.f)*mVisibleSize.height),  Point((835.f/960.f)*mVisibleSize.width,(140.f/640.f)*mVisibleSize.height),HumanType(kExplorer),6);
    mHumano3=Human::create(this,  Point((200.f/960.f)*mVisibleSize.width,(170.f/640.f)*mVisibleSize.height),  Point((780.f/960.f)*mVisibleSize.width,(170.f/640.f)*mVisibleSize.height),HumanType(kExplorer),5);
    
    mCanibal1=Human::create(this, Point((65.f/960.f)*mVisibleSize.width,(390.f/640.f)*mVisibleSize.height),  Point((930/960.f)*mVisibleSize.width,(270.f/640.f)*mVisibleSize.height),HumanType(kCanibal),4);
    mCanibal2=Human::create(this, Point((165.f/960.f)*mVisibleSize.width,(380.f/640.f)*mVisibleSize.height),  Point((863/960.f)*mVisibleSize.width,(250.f/640.f)*mVisibleSize.height),HumanType(kCanibal),4);
    mCanibal3=Human::create(this,  Point((265.f/960.f)*mVisibleSize.width,(375.f/640.f)*mVisibleSize.height),  Point((800/960.f)*mVisibleSize.width,(240.f/640.f)*mVisibleSize.height),HumanType(kCanibal),4);
    
    this->addChild(mHumano1,4);
    this->addChild(mHumano2,6);
    this->addChild(mHumano3,4);
    this->addChild(mCanibal1,4);
    this->addChild(mCanibal2,4);
    this->addChild(mCanibal3,4);
    
    mCanoa=Canoe::create(this, std::string("canibals/boat.png"), true);
    this->addChild(mCanoa,7);
    
    /*CCLabelTTF* label=CCLabelTTF::create(LanguageManager::getInstance()->getStringForKey("CANIBALS_TOUCH_TO_MOVE", "CANIBALS_TOUCH_TO_MOVE"), "fonts/KGBlankSpaceSketch.ttf",15);*/
    Label* label=Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("CANIBALS_TOUCH_TO_MOVE", "CANIBALS_TOUCH_TO_MOVE"), "fonts/KGBlankSpaceSketch.ttf", 15);    
    label->setPosition(Point(mCanoa->getContentSize().width/2,mCanoa->getContentSize().height/2));
    label->enableOutline(Color4B(82,44,4,255),2);
    mCanoa->addChild(label);

}


CanibalsScene* CanibalsScene::getInstance(){
    return instancia_canibal;
};

void CanibalsScene::checkFinish(Ref* sender){
    mSmallGameOver=true;
    int num_canibales_i=0;
    int num_explorer_i=0;
    int num_canibales_d=0;
    int num_explorer_d=0;
    if(mCanoa->getPassengers()>0){
        if(mCanoa->estaEnIzquierda()){
            if(mCanoa->getHuman(0)!=NULL){
                if(mCanoa->getHuman(0)->esExplorer()){
                    num_explorer_i++;
                }else{
                    num_canibales_i++;
                }
            }
            if(mCanoa->getHuman(1)!=NULL){
                if(mCanoa->getHuman(1)->esExplorer()){
                    num_explorer_i++;
                }else{
                    num_canibales_i++;
                }
            }
        }else{
            if(mCanoa->getHuman(0)!=NULL){
                if(mCanoa->getHuman(0)->esExplorer()){
                    num_explorer_d++;
                }else{
                    num_canibales_d++;
                }
            }
            if(mCanoa->getHuman(1)!=NULL){
                if(mCanoa->getHuman(1)->esExplorer()){
                    num_explorer_d++;
                }else{
                    num_canibales_d++;
                }
            }
            
        }
    }
   
    CCLOG("CANOA I===> Canibal:%i Explorer:%i  |||| D===>Canibal:%i Explorer: %i",num_canibales_i,num_explorer_i,num_canibales_d,num_explorer_d);
  
    
    
    if(mCanibal1->mEstado!=HumanState(kEmbarcado) ){
        if(mCanibal1->mEstado==HumanState(kLadoIzquierdo)){
            num_canibales_i++;
        }else{
            num_canibales_d++;
        }
    }
    if(mCanibal2->mEstado!=HumanState(kEmbarcado)){
        if(mCanibal2->mEstado==HumanState(kLadoIzquierdo)){
            num_canibales_i++;
        }else{
            num_canibales_d++;
        }
    }
    if( mCanibal3->mEstado!=HumanState(kEmbarcado)){
        if(mCanibal3->mEstado==HumanState(kLadoIzquierdo)){
            num_canibales_i++;
        }else{
            num_canibales_d++;
        }
    }
    if( mHumano1->mEstado!=HumanState(kEmbarcado)){
        if(mHumano1->mEstado==HumanState(kLadoIzquierdo)){
            num_explorer_i++;
        }else{
            num_explorer_d++;
        }
    }
    if( mHumano2->mEstado!=HumanState(kEmbarcado)){
        if(mHumano2->mEstado==HumanState(kLadoIzquierdo)){
            num_explorer_i++;
        }else{
            num_explorer_d++;
        }
    }
    if( mHumano3->mEstado!=HumanState(kEmbarcado)){
        if(mHumano3->mEstado==HumanState(kLadoIzquierdo)){
            num_explorer_i++;
        }else{
            num_explorer_d++;
        }
    }
    
      CCLOG("TOTAL===> Canibal:%i Explorer:%i  |||| D===>Canibal:%i Explorer: %i ",num_canibales_i,num_explorer_i,num_canibales_d,num_explorer_d);
    
    
    if((num_canibales_i>num_explorer_i && num_explorer_i>0) ||
       (num_canibales_d>num_explorer_d && num_explorer_d>0) ){
        mTextoGameOver=LanguageManager::getInstance()->getStringForKey("CANIBAL_GAMEOVER_MORE_C_THAN_E");
        mGameover=true;
    }
    if(mHumano1->mEstado==HumanState(kLadoDerecho) &&
       mHumano2->mEstado==HumanState(kLadoDerecho)&&
       mHumano3->mEstado==HumanState(kLadoDerecho)&&
       mCanibal1->mEstado==HumanState(kLadoDerecho)&&
       mCanibal2->mEstado==HumanState(kLadoDerecho)&&
       mCanibal3->mEstado==HumanState(kLadoDerecho)){
        mStars=3;
        mSuccess=true;
        mTextoSuccess=std::string(LanguageManager::getInstance()->getStringForKey("CANIBAL_SUCCESS","You saved the missionaries!"));
    }
    BaseScene::checkFinish(NULL);

};


bool CanibalsScene::shouldCheckFinish(){
    if(mHumano1->mEstado==HumanState(kLadoDerecho) &&
       mHumano2->mEstado==HumanState(kLadoDerecho)&&
       mHumano3->mEstado==HumanState(kLadoDerecho)&&
       mCanibal1->mEstado==HumanState(kLadoDerecho)&&
       mCanibal2->mEstado==HumanState(kLadoDerecho)&&
       mCanibal3->mEstado==HumanState(kLadoDerecho)){
            CCLOG("shouldCheckFinish");
            return true;
    }
    return false;

}



Canoe* CanibalsScene::getCanoe(){
    return mCanoa;
}

void CanibalsScene::Retry(Ref* sender){
    Scene* scene=CanibalsScene::scene();
    Director *pDirector = Director::getInstance();
    pDirector->replaceScene(TransitionCrossFade::create(0.5f, scene));
    
}

void CanibalsScene::HintStartTemp(float dt){
    HintStart(NULL);
}


void CanibalsScene::HintStart(Ref* sender){
    if(mCanoa->isMoving())
    {
        CCLOG("Is moving");
        return;
    }
  

    switch(mHintStep){
        case 1:
            mHumano1->removeFromParent();
            mHumano2->removeFromParent();
            mHumano3->removeFromParent();
            mCanibal1->removeFromParent();
            mCanibal2->removeFromParent();
            mCanibal3->removeFromParent();
            mCanoa->removeFromParent();
            crearHumanos();
            break;
        case 2:
            mCanibal2->click(NULL);
            mCanibal1->click(NULL);
            break;
        case 3:
            mCanoa->move(NULL);
            break;
        case 4:
            mCanibal1->click(NULL);
            break;
        case 5:
            mCanoa->move(NULL);
            break;
        case 6:
            mCanibal1->click(NULL);
            mCanibal3->click(NULL);
            break;
        case 7:
            mCanoa->move(NULL);
            break;
        case 8:
            mCanibal1->click(NULL);
            break;
        case 9:
            mCanoa->move(NULL);
            break;
        case 10:
            mHumano1->click(NULL);
            mHumano2->click(NULL);
            break;
        case 11:
           mCanoa->move(NULL);
            break;
        case 12:
            mHumano1->click(NULL);
            mCanibal2->click(NULL);
            break;
        case 13:
            mCanoa->move(NULL);
            break;
        case 14:
            mHumano1->click(NULL);
            mHumano3->click(NULL);
            break;
        case 15:
            mCanoa->move(NULL);
            break;
        case 16:
            mCanibal3->click(NULL);
            break;
        case 17:
            mCanoa->move(NULL);
            break;
        case 18:
            mCanibal1->click(NULL);
            mCanibal3->click(NULL);
            break;
        case 19:
            mCanoa->move(NULL);
            break;
        case 20:
            mCanibal1->click(NULL);
            break;
        case 21:
            mCanoa->move(NULL);
            break;
        case 22:
            mCanibal1->click(NULL);
            mCanibal2->click(NULL);
            break;
        case 23:
            mCanoa->move(NULL);
            break;
       
    }
    mHintStep++;
}

void CanibalsScene::Hint(Ref* sender){
    BaseScene::Hint(NULL);
}

void CanibalsScene::HintCallback(bool accepted){
    if(accepted){
        BaseScene::HintCallback(accepted);
        //crearPersonajes();
        mHintStep=1;
        mSituacionHint=true;
        LayerHintOk::mostrar(true);
        mScheudleHint=schedule_selector(CanibalsScene::HintStartTemp);
        //mScheudleHint=schedule_selector(CanibalsScene::HintStart);
        this->schedule(mScheudleHint,2.f);
       
    }
}


void CanibalsScene::onEnterTransitionDidFinish(){
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sounds/fivefrogs/jungle.mp3", true);
    BaseScene::onEnterTransitionDidFinish();
}
