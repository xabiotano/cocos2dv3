//
//  TweetSender.m
//  GetSocial
//
//  Created by Mikel Eizagirre on 24/03/13.
//
//

#import "FacebookSender.h"
//#import "../cocos2dx/platform/ios/EAGLView.h"
//#import "../cocos2dx/platform/ios"
#import <string>
#import "cocos2d.h"
#import "RootViewController.h"
#import "WASWhatsAppUtil.h"



@implementation FacebookSender

/**
 * Try to send a tweet using iOS 5 automatically
 */
+(void) tryToPostOnFacebook:(const char *)texto
{
    /*
    NSLog(@"Try to post on facebook");
    SLComposeViewController  *mySocialComposer;
    UIViewController *myViewController;

    myViewController = [UIViewController alloc];
    EAGLView *view = [EAGLView sharedEGLView];
    [view addSubview:(myViewController.view)];
    
    mySocialComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    
    
  
    
    // Twitter on iOS 5, already is connected to user
    NSString* customMessage= [NSString stringWithFormat:@"%@",[NSString stringWithCString: texto encoding: NSUTF8StringEncoding]];

    
    
    
    std::string pathimagen = "../Documents/trivialparty.jpg";
    CCLOG("%s",pathimagen.c_str());
    NSString* pathimagen_ios=[NSString stringWithFormat:@"%@",[NSString stringWithCString: pathimagen.c_str() encoding: NSUTF8StringEncoding]];
    
    //  Adds an image to the Tweet.  For demo purposes, assume we have an
    //  image named 'larry.png' that we wish to attach
    if (![mySocialComposer addImage:[UIImage imageNamed:pathimagen_ios]]) {
        NSLog(@"Unable to add the image!");
    }
    
    [mySocialComposer setInitialText:customMessage];
    
    [myViewController presentViewController:mySocialComposer animated:YES completion:nil];
    
    mySocialComposer.completionHandler = ^(SLComposeViewControllerResult result)
    {
        const char *message = "";
        int status=-1;
        switch (result)
        {
            case SLComposeViewControllerResultCancelled:
            {
                message = "Post Canceled";
                status=0;
                break;
            }
            case SLComposeViewControllerResultDone:
            {
                message = "Posted on Facebook";
                status=1;
            }
            default:
                break;
        }
        [myViewController dismissViewControllerAnimated:true completion:^{}];
    };*/
    
    
  

    
    
    NSString* file = @"//screenshot.png";    //get the path to the Documents directory
    NSArray* paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    NSString* screenshotPath = [documentsDirectory stringByAppendingPathComponent:file];
    
    NSLog(@"copy from: %@",screenshotPath);
    
    
    
    
    
    
    //get the screenshot as raw data
    NSData *data = [NSData dataWithContentsOfFile:screenshotPath];
    //create an image from the raw data
    UIImage *img = [UIImage imageWithData:data];
    //save the image to the users Photo Library
    UIImageWriteToSavedPhotosAlbum(img, nil, nil, nil);

    
    if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"whatsapp://app"]]){
        NSString* file = @"//screenshot.png";    //get the path to the Documents directory
        NSArray* paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        NSString* documentsDirectory = [paths objectAtIndex:0];
        NSString* screenshotPath = [documentsDirectory stringByAppendingPathComponent:file];
        
        NSLog(@"copy from: %@",screenshotPath);
        
        //get the screenshot as raw data
        NSData *data = [NSData dataWithContentsOfFile:screenshotPath];
        //create an image from the raw data
        UIImage *img = [UIImage imageWithData:data];
        //save the image to the users Photo Library
        UIImageWriteToSavedPhotosAlbum(img, nil, nil, nil);
        
        
        NSString* texttoshare= [NSString stringWithFormat:@"%@",[NSString stringWithCString: texto encoding: NSUTF8StringEncoding]];
        NSArray *activityItems = @[img];
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
        
        //activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint, UIActivityTypePostToTwitter, UIActivityTypePostToWeibo];
        UIViewController *myViewController = [UIViewController alloc];
        //EAGLView *view = [EAGLView sharedEGLView];
        cocos2d::GLView *view =   cocos2d::Director::getInstance()->getOpenGLView();
        [view addSubview:(myViewController.view)];
       
        
        [myViewController presentViewController:activityVC animated:TRUE completion:nil];
        
    }else{
        NSString* file = @"//screenshot.png";    //get the path to the Documents directory
        NSArray* paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        NSString* documentsDirectory = [paths objectAtIndex:0];
        NSString* screenshotPath = [documentsDirectory stringByAppendingPathComponent:file];
    
        NSLog(@"copy from: %@",screenshotPath);
    
        //get the screenshot as raw data
        NSData *data = [NSData dataWithContentsOfFile:screenshotPath];
        //create an image from the raw data
        UIImage *img = [UIImage imageWithData:data];
        //save the image to the users Photo Library
        UIImageWriteToSavedPhotosAlbum(img, nil, nil, nil);

    
        NSString* texttoshare= [NSString stringWithFormat:@"%@",[NSString stringWithCString: texto encoding: NSUTF8StringEncoding]];
        NSArray *activityItems = @[texttoshare, img];
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    
        //activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint, UIActivityTypePostToTwitter, UIActivityTypePostToWeibo];
        UIViewController *myViewController = [UIViewController alloc];
      //  EAGLView *view = [EAGLView sharedEGLView];
        cocos2d::GLView *view =   cocos2d::Director::getInstance()->getOpenGLView();
        [view addSubview:(myViewController.view)];
    
        [myViewController presentViewController:activityVC animated:TRUE completion:nil];
    
    }
}




@end
