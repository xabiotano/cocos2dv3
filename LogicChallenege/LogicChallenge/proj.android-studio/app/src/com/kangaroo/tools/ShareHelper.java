package com.kangaroo.tools;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URLConnection;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.view.View;

public class ShareHelper {

	public static void takeScreenshot(Activity activity,String path) {
	   
	   

	    try {
	        // image naming and path  to include sd card  appending name you choose for file
	      

	        // create bitmap screen capture
	        View v1 =activity.getWindow().getDecorView().getRootView();
	        v1.setDrawingCacheEnabled(true);
	        Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
	        v1.setDrawingCacheEnabled(false);

	        File imageFile = new File(path);
	        shareImage(activity,imageFile);
	        
	    } catch (Throwable e) {
	        // Several error may come out with file handling or OOM
	        e.printStackTrace();
	    }
	}
	
	
	private static void shareImage(Context context,File f){
	
        Uri screenshotUri =Uri.parse("file://"+f.getAbsolutePath());


        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);


        intent.putExtra(Intent.EXTRA_SUBJECT,"Logic Safari (Google Play e iOS");
        intent.putExtra(Intent.EXTRA_TEXT,"Echáme una mano, ¿Sabes la respuesta?");
        intent.putExtra(Intent.EXTRA_TITLE,"");
        intent.putExtra(Intent.EXTRA_STREAM,screenshotUri);

        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

        //grant permisions for all apps that can handle given intent


        intent.setDataAndType(screenshotUri, URLConnection.guessContentTypeFromName(screenshotUri.toString()));
		context.startActivity(Intent.createChooser(intent, "Friends"));

       

	}
	
}
