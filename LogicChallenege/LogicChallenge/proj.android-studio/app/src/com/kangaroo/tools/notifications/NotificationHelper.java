package com.kangaroo.tools.notifications;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;
import com.kangaroo.logicchallenge.R;

import org.cocos2dx.lib.Cocos2dxActivity;

import java.lang.ref.WeakReference;
import java.util.Calendar;
import static android.content.Context.ALARM_SERVICE;


public class NotificationHelper implements INotificationHelper{
    private final static String TAG="NotificationHelper";
    private WeakReference<Activity> mActivity;
    private static NotificationHelper mInstance;



    public static NotificationHelper getInstance()
    {
        if(mInstance==null)
        {
            mInstance=new NotificationHelper((Cocos2dxActivity)Cocos2dxActivity.getContext());
        }
        return mInstance;
    }

    public NotificationHelper(Activity activity){
        mActivity=new WeakReference<Activity>(activity);
    }




    @Override
    public  void showLocalNotification(String message, int interval, int tag) {
        Log.v(TAG, "showLocalNotification" + message + interval + tag);
        PendingIntent sender = getPendingIntent(message, tag);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.SECOND, interval);

        AlarmManager am = (AlarmManager)mActivity.get().getSystemService(ALARM_SERVICE);
        am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), sender);
    }

    @Override
    public void cancelLocalNotification(int tag) {
        Log.v(TAG, "cancelLocalNotification");
		PendingIntent sender = getPendingIntent(null, tag);
        if(sender != null) {
            AlarmManager am = (AlarmManager)mActivity.get().getSystemService(ALARM_SERVICE);
            am.cancel(sender);
            sender.cancel();
        }



    }

    @Override
    public void showGiftNotificationI(boolean mostrado) {

        showGiftNotification(true);
    }

    private native void showGiftNotification(boolean usada) ;


    private  PendingIntent getPendingIntent(String message, int tag) {


        Intent i = new Intent(mActivity.get().getApplicationContext(), LocalNotificationReceiver.class);
        i.putExtra("notification_id", tag);
        String str =String.valueOf(message); // works.
        i.putExtra("message",str);
        Log.e(TAG,"LogicChallenge NOTIFICATION"+tag+"   "+message);
        PendingIntent sender = PendingIntent.getBroadcast(mActivity.get(), 0, i, 0);
        return sender;
    }





}
