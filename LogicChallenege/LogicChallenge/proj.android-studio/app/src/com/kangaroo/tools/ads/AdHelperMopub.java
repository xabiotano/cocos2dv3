package com.kangaroo.tools.ads;

import android.app.Activity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdView;
import com.kangaroo.tools.AdsActvity;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubInterstitial;
import com.mopub.mobileads.MoPubView;

import org.cocos2dx.lib.Cocos2dxActivity;

/**
 * Created by xabiotano on 4/6/17.
 */

public class AdHelperMopub  implements  IAdHelper, MoPubInterstitial.InterstitialAdListener, MoPubView.BannerAdListener {
    IAdNative mAdNative;
    private MoPubInterstitial mInterstitial;
    private MoPubView mMoPubView;
    private final String INTERISTIAL_AD_UNIT="d44dbc7af6c44cfebc595305832a9f65";
    private final String BANNER_AD_UNIT="d94e21a634ca4b7c84b45451eaf85c8c";
    private boolean mShowWhenLoad=false;

    private static AdHelperMopub mAdHelper;

    public static  IAdHelper getInstance(IAdNative adNative){
        if(mAdHelper==null)
        {
            mAdHelper=new AdHelperMopub();
            mAdHelper.setAdNative(adNative);
        }
        return mAdHelper;
    }


    public static  IAdHelper getInstance(){
        return mAdHelper;
    }



    private void setAdNative(IAdNative adNative) {
        mAdNative=adNative;
        mInterstitial = new MoPubInterstitial(mAdNative.getActivity(), INTERISTIAL_AD_UNIT);
        mInterstitial.setInterstitialAdListener(this);
        mInterstitial.load();
    }

    @Override
    public void showIntersitial() {
        Log.i("IAdHelperMopub","showIntersitial");
        if(mInterstitial.isReady())
        {
            mAdNative.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mInterstitial.show();

                }
            });
        }else{
            mShowWhenLoad=true;
            cacheIntersitial();
        }
    }

    @Override
    public void cacheIntersitial() {
        if(mInterstitial==null)
        {
            Log.i("IAdHelperMopub","cacheIntersitial -- creado");
            mInterstitial = new MoPubInterstitial(mAdNative.getActivity(), INTERISTIAL_AD_UNIT);
            mInterstitial.setInterstitialAdListener(this);
        }
        if(mInterstitial.isReady())
        {
            Log.i("IAdHelperMopub","cacheIntersitial -- ready");
            return;
        }
        mInterstitial.load();
        Log.i("IAdHelperMopub","cacheIntersitial -- load");
    }

    @Override
    public void showBanner() {

        if(mMoPubView==null) {
            Log.i("IAdHelperMopub", "showBanner");
            final FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.MATCH_PARENT,
                    FrameLayout.LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;


            mMoPubView = new MoPubView(mAdNative.getActivity());
            mMoPubView.setAdUnitId(BANNER_AD_UNIT); // Enter your Ad Unit ID from www.mopub.com
            mMoPubView.loadAd();
            mMoPubView.setBannerAdListener(this);


            final FrameLayout adViewLayout = new FrameLayout(mAdNative.getActivity());
            adViewLayout.setLayoutParams(params);
            mAdNative.getActivity().addContentView(adViewLayout,params);

            mAdNative.getActivity().runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    adViewLayout.addView(mMoPubView);

                    mMoPubView.setEnabled(true);
                    mMoPubView.setVisibility(View.VISIBLE);

                }
            });
        }
    }

    @Override
    public void hideBanner() {

    }

    @Override
    public void setIntersitialCachedI() {
        //llamo al metodo C++ AdHelper::setIntersitialCached();
        Log.i("IAdHelperMopub","setIntersitialCached");
        mAdNative.setIntersitialCached(true);
        if(mShowWhenLoad)
        {
            mShowWhenLoad=false;
            showIntersitial();
        }

    }

    @Override
    public boolean getIntersitialCachedI() {
        Log.i("IAdHelperMopub","getIntersitialCached");
        return mInterstitial.isReady();
    }

    @Override
    public void destroy() {
        if (mInterstitial != null) {
            mInterstitial.destroy();
            mInterstitial = null;
        }
        if(mMoPubView!=null)
        {
            mMoPubView.destroy();
            mMoPubView=null;
        }
        mAdHelper=null;

    }


    /* InterstitialAdListener*/
    @Override
    public void onInterstitialLoaded(MoPubInterstitial interstitial) {
        if(interstitial.isReady())
        {
            setIntersitialCachedI();
        }
    }

    @Override
    public void onInterstitialFailed(MoPubInterstitial interstitial, MoPubErrorCode errorCode) {}

    @Override
    public void onInterstitialShown(MoPubInterstitial interstitial) {
        mAdNative.interstitialDidAppear("");
    }

    @Override
    public void onInterstitialClicked(MoPubInterstitial interstitial) {}

    @Override
    public void onInterstitialDismissed(MoPubInterstitial interstitial) {}



    @Override
    public void onBannerLoaded(MoPubView banner) {}
    @Override
    public void onBannerFailed(MoPubView banner, MoPubErrorCode errorCode) {
        Log.e("AdHelperMopub","onBannerFailed: "+ errorCode);
        if(mMoPubView!=null){
            mMoPubView.loadAd();
        }
    }
    @Override
    public void onBannerClicked(MoPubView banner){}
    @Override
    public void onBannerExpanded(MoPubView banner) {}
    @Override
    public void onBannerCollapsed(MoPubView banner) {}







    /*InterstitialAdListener*/
}
