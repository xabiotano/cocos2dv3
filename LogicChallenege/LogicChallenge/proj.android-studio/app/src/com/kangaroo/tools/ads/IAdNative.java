package com.kangaroo.tools.ads;

import android.app.Activity;

import org.cocos2dx.lib.Cocos2dxActivity;

/**
 * Created by xabiotano on 5/6/17.
 */

public interface IAdNative {
    void interstitialDidAppear(String device_token);
    void setIntersitialCached(boolean cacheado);
    boolean getRemoveAdsPurchased();
    Cocos2dxActivity getActivity();
}
