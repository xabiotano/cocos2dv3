package com.kangaroo.tools;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.util.EventLogTags.Description;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.kangaroo.logicchallenge.R;

public class IConnection implements Runnable {

	ConnectivityManager conMgr;
	Context mContext;
	Handler mHandler;
	boolean mHayInternet;
	AlertDialog mDialog;
	
	public IConnection(Context context){
		mContext=context;
		conMgr=  (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		mHandler=new Handler();
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try{
			hasActiveInternetConnection();
			mHandler.postDelayed(IConnection.this, 10000);
			Log.i("IConnection", "Internet check");
		}catch(Exception e){
			
		}
		
		
	}
	
	
	
	public  void hasActiveInternetConnection() {
	   try {
	       	new InternetCheckTask(this).execute();
	           
	     } catch (Exception e) {
	            Log.e("IConnection", "Error checking internet connection", e);
	     }
	}
	
	public void resultadoPost(boolean resultado) {
		Log.i("IConnection", "Resultado "+ resultado);
		mHayInternet=resultado;
		if(resultado &&mDialog!=null && mDialog.isShowing()){
			mDialog.dismiss();
			return;
		}
		if(!mHayInternet){
			if(mDialog!=null && mDialog.isShowing()){
				hasActiveInternetConnection();
				return;
			}
			 mDialog = new AlertDialog.Builder(mContext)
	        .setTitle(mContext.getResources().getString(R.string.internet_error))
	        .setPositiveButton(android.R.string.ok, null) //Set to null. We override the onclick
	        .setNegativeButton(android.R.string.cancel, null)
	        .create();
			
			
			 mDialog.setOnShowListener(new DialogInterface.OnShowListener() {

			    @Override
			    public void onShow(final DialogInterface dialog) {

			        Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
			        button.setOnClickListener(new View.OnClickListener() {

			            @Override
			            public void onClick(View view) {
			                // TODO Do something
			            	hasActiveInternetConnection();
			            	mDialog.dismiss();
			            }
			        });
			    }
			    
			});
			 mDialog.show();
		}
	}
}
