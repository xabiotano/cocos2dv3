/****************************************************************************
Copyright (c) 2015 Chukong Technologies Inc.
 
http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package org.cocos2dx.cpp;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.kangaroo.tools.AdsActvity;
import com.kangaroo.tools.IConnection;
import com.kangaroo.tools.notifications.INotificationHelper;
import com.kangaroo.tools.notifications.NotificationHelper;

import org.cocos2dx.lib.Cocos2dxActivity;

public class AppActivity extends Activity {

    private InterstitialAd mInterstitialAd;
    IConnection mRunnable=null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRunnable = new IConnection(this);
        try{
            mRunnable.run();

        }catch(Exception e){

        }
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3264621525340778/7309910444");

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int var1) {
                Log.e("AppActivity","onAdFailedToLoad"+var1);
            }
            @Override
            public void onAdLoaded() {
                mInterstitialAd.show();
            }
            @Override
            public void onAdClosed() {
                // Load the next interstitial.
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }

        });

        mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice("06E7B5E4A8151F226F0D84803E4B72CD").build());

    }
    @Override
    protected void onResume() {
        super.onResume();
        String startedFrom = getIntent().getStringExtra("started_from");
        if(startedFrom!=null && startedFrom.equalsIgnoreCase("notification"))
        {
            NotificationHelper.getInstance().showGiftNotificationI(true);
        }
        //mInterstitialAd.show();
    }



    public static void showLocalNotification(String message, int interval, int tag) {
        NotificationHelper.getInstance().showLocalNotification(message,interval,tag);
    }

    public static void cancelLocalNotification(int tag) {
        NotificationHelper.getInstance().cancelLocalNotification(tag);
    }


}
